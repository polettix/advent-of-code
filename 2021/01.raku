#!/usr/bin/env raku
use v6;

sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.sample')) {
   my $inputs = get-inputs($filename);
   my ($part1, $part2) = solve($inputs);

   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";
   put "part1 $highlight$part1$reset";
   put "part2 $highlight$part2$reset";
}

sub get-inputs ($filename) {
   $filename.IO.basename.IO.lines.Array;
} ## end sub get_inputs ($filename = undef)

sub solve ($inputs) {
   return (part1($inputs), part2($inputs));
}

sub count-increases (@inputs) {
   my $count = (1 .. @inputs.end)
      .map({@inputs[$_] > @inputs[$_ - 1] ?? 1 !! 0 })
      .sum;
}

sub part1 ($inputs) { return count-increases($inputs) }

sub part2 ($inputs) {
   return count-increases(
      (1 ..^ $inputs.end).map({$inputs[($_-1)..($_+1)].sum})
   );
}
