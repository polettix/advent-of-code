#!/usr/bin/env raku
use v6;

sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.sample')) {
   return solve($filename);
}

sub get-inputs ($filename) {
   my ($map, $image) = $filename.IO.slurp.split: /\n (\n+ | $)/;
   my @map = $map.comb(/ \S /).map: { $_ eq '.' ?? 0 !! 1 };
   my @image = $image.lines».comb(/ \S /)».map({$_ eq '.' ?? 0 !! 1 }).Array;
   return {
      mapping => @map,
      image   => expand(expand(@image)),
      around  => 0,
   };
}

sub solve ($filename) {
   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";

   my $inputs = get-inputs($filename);
   my ($start, $elapsed);

   $start = now;
   my $part1 = part1($inputs);
   $elapsed = now - $start;
   put "part1 ($elapsed) $highlight$part1$reset";

   $start = now;
   my $part2 = part2($inputs);
   $elapsed = now - $start;
   put "part2 ($elapsed) $highlight$part2$reset";

   return 0;
}

sub printout ($image) {
   for @$image -> $line {
      put TR/01/.X/ given $line.join: '';
   }
}

sub expand ($image, $around = 0) {
   my $retval = $image.map({ [ $around, |@$_, $around ] }).Array;
   $retval.unshift: [ $around xx $retval[0].elems ];
   $retval.push: [ $around xx $retval[0].elems ];
   return $retval;
}

sub value-at ($input, $r, $c) {
   my $bstring = (-1 .. 1).map({ $input<image>[$r + $_][$c - 1 .. $c + 1].join('')}).join('');
   return $input<mapping>[$bstring.parse-base(2)];
}

sub calculate-lit ($image) { $image».sum.sum }

sub enhance ($input) {
   my @trimmed;
   for 1 ..^ $input<image>.end -> $ri {
      my $row = $input<image>[$ri];
      @trimmed.push: (1 ..^ $row.end).map({value-at($input, $ri, $_)}).Array;
   }
   my $around = $input<mapping>[0] - $input<around>;
   return {
      mapping => $input<mapping>,
      image => expand(expand(@trimmed, $around), $around),
      around => $around;
   };
}

sub part1 ($inputs is copy) {
   $inputs = enhance($inputs) for 1 .. 2;
   '2021-20-1.pgm'.IO.spurt(pgm($inputs<image>)) if %*ENV<DEBUG>;
   return calculate-lit($inputs<image>);
}

sub part2 ($inputs is copy) {
   for 1 .. 50 {
      $inputs = enhance($inputs);
      print '.' if %*ENV<DEBUG>;
   }
   put '' if %*ENV<DEBUG>;
   '2021-20-2.pgm'.IO.spurt(pgm($inputs<image>)) if %*ENV<DEBUG>;
   return calculate-lit($inputs<image>);
}

sub pgm ($image) {
   my @lines = 'P2';

   my $width  = $image[0].elems;
   my $height = $image.elems;
   @lines.push: "$width $height";

   @lines.push: 1;

   @lines.push: $_ for @$image;

   @lines.push: ''; # ensure an end-of-line

   return @lines.join("\n");
}
