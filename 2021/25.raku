#!/usr/bin/env raku
use v6;

sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.sample')) {
   return solve($filename);
}

sub solve ($filename) {
   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";

   my $inputs = get-inputs($filename);
   my ($start, $elapsed);

   $start = now;
   my $part1 = part1($inputs);
   $elapsed = now - $start;
   put "part1 ($elapsed) $highlight$part1$reset";

   $start = now;
   my $part2 = part2($inputs);
   $elapsed = now - $start;
   put "part2 ($elapsed) $highlight$part2$reset";

   return 0;
}

sub get-inputs ($filename) {
   $filename.IO.lines.map({.comb(/\S/).Array}).Array
}

sub part1 ($inputs) {
   my $steps = 0;
   my $pre = printable($inputs);
   loop {
      step($inputs);
      my $post = printable($inputs);
      last if $pre eq $post;
      $pre = $post;
      ++$steps;
      $steps.note if %*ENV<DEBUG>;
   }
   return $steps + 1;
}

sub part2 ($inputs) {
   return 'there is no part2...'
}

sub tick ($field, Bool:D :$east) {
   my @limits = $field.elems, $field[0].elems;
   @limits = @limits.reverse unless $east;
   my $moving = $east ?? '>' !! 'v';
   my $empty  = '.';
   for ^@limits[0] -> $o {    # "o"uter
      my $just-moved = False;
      my $first-moved = False;
      for ^@limits[1] -> $i { # "i"nner
         my $I = ($i + 1) % @limits[1];
         last if $I < $i && $first-moved;
         if $just-moved { # skip if already moved
            $just-moved = False;
            next;
         }
         $just-moved = False;
         my ($R, $C) = $east ?? ($o, $I) !! ($I, $o);
         next unless $field[$R][$C] eq $empty;
         my ($r, $c) = $east ?? ($o, $i) !! ($i, $o);
         next unless $field[$r][$c] eq $moving;
         $field[$R][$C] = $moving;
         $field[$r][$c] = $empty;
         $just-moved = True;
         $first-moved = True if $i == 0;
      }
   }
   return $field;
}

sub step ($field) { tick(tick($field, east => True), east => False) }

sub printable ($field) { $field».join('').join("\n") }
