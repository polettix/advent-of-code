#!/usr/bin/env raku
use v6;

sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.sample')) {
   my $inputs = get-inputs($filename);
   my ($part1, $part2) = solve($inputs);

   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";
   put "part1 $highlight$part1$reset";
   put "part2 $highlight$part2$reset";
}

sub get-inputs ($filename) {
   my @grid = $filename.IO.words».comb(/\d/)».Array;
   my (%info-about, @dumbos-in);
   my ($mx, $my) = @grid[0].end, @grid.end;
   for 0 .. $my -> $y {
      for 0 .. $mx -> $x {
         my $key = "$x,$y";
         my %h = value => @grid[$y][$x];
         %info-about{$key} = @dumbos-in[%h<value>]{$key} = %h;
         %h<neighbors> = set gather {
            for [-1 .. 1] X [-1 .. 1] -> ($dx, $dy) {
               next unless $dx || $dy;
               my ($X, $Y) = ($x + $dx, $y + $dy);
               next unless 0 <= $X <= $mx && 0 <= $Y <= $my;
               take "$X,$Y";
            }
         };
      }
   }
   return [%info-about, @dumbos-in, @grid];
}

sub printable (%ia, $mx, $my) {
   return sub ($step, $msg is copy = Nil) {
      return unless %*ENV<DEBUG>;
      $msg //= "#$step";
      put "- $msg";
      (0 .. $mx).map(-> $x { (%ia{"$x,$_"}<value> + $step) % 10 }).join('').put
         for 0 .. $my;
      put '';
   };
}

sub solve ($inputs) {
   my ($info-about, $dumbos-in, $grid) = @$inputs;
   my $mx = $grid[0].end;
   my $my = $grid.end;
   my $number-of-cells = ($mx + 1) * ($my + 1);
   my &printout = printable($info-about, $mx, $my);
   &printout(0, 'start');
   my $overall-count = 0;
   my $sync-step;
   for 1 .. * -> $step {
      my $fire-value = (0 - $step) % 10;
      my @firing = $dumbos-in[$fire-value].keys;
      my $this-count = 0;
      while (@firing.elems) {
         my $cell = @firing.shift;
         ++$this-count;
         for $info-about{$cell}<neighbors>.keys -> $n-key {
            my $neighbor = $info-about{$n-key};
            my $n-value = $neighbor<value>;
            next if $n-value == $fire-value; # also firing in this step
            $neighbor<value> = my $next-n-value = ($n-value + 1) % 10;
            $dumbos-in[$next-n-value]{$n-key} = $neighbor;
            $dumbos-in[$n-value]{$n-key}:delete;
            @firing.push: $n-key if $next-n-value == $fire-value;
         }
      }
      $sync-step = $step if $this-count == $number-of-cells;
      $overall-count += $this-count if $step <= 100;
      &printout($step) if $step == 100;
      last if $step >= 100 && $sync-step;
   }
   &printout($sync-step);
   return ($overall-count, $sync-step);
}
