#!/usr/bin/env raku
use v6;

sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.sample')) {
   return solve($filename);
}

sub solve ($filename) {
   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";

   my $inputs = get-inputs($filename);
   my ($start, $elapsed);

   $start = now;
   my $part1 = part1($inputs);
   $elapsed = now - $start;
   put "part1 ($elapsed) $highlight$part1$reset";

   $start = now;
   my $part2 = part2($inputs);
   $elapsed = now - $start;
   put "part2 ($elapsed) $highlight$part2$reset";

   if %*ENV<ALTERNATIVE> {
      $start = now;
      $part2 = part2-play($inputs);
      $elapsed = now - $start;
      put "part2 [play recursively] ($elapsed) $highlight$part2$reset";
   }

   return 0;
}

sub get-inputs ($filename) {
   $filename.IO.lines».comb(/\d+/)»[1];
}

class Player {
   has $!position is required is built;
   has $!score                is built = 0;

   method TWEAK (:$!position) { --$!position }

   method advance ($amount) {
      $!position = ($!position + $amount) % 10;
      $!score    += 1 + $!position;
   }

   method score () { $!score }
}

class DeterministicDie {
   has $!current is built = 99;
   has $!count            = 0;
   method roll () {
      ++$!count;
      $!current = ($!current + 1) % 100;
      return $!current + 1;
   }
   method roll3 () { (1..3).map({self.roll}).sum }
   method count () { $!count }
}

sub part1 ($inputs) {
   my @players = $inputs.map: { Player.new(position => $_) };
   my $die = DeterministicDie.new();
   my $target = 1000;
   my $current = 0;
   loop {
      @players[$current].advance($die.roll3);
      last if @players[$current].score >= $target;
      $current = 1 - $current;
   }
   return @players[1 - $current].score * $die.count;
}

sub moves-to-win ($start) {
   my %factor-for;
   for (1..3) X (1..3) X (1..3) -> $tuple {
      %factor-for{$tuple.sum}++;
   }

   my %mtw;
   my $target = 21;
   my @stack = {position => $start - 1, score => 0, factor => 1, rolls => [3 .. 9]},;
   while (@stack) {
      my $top = @stack[* - 1];
      if ($top<rolls>.elems == 0) {
         @stack.pop;
         next;
      }

      my $roll = $top<rolls>.pop;
      my $position = ($top<position> + $roll) % 10;
      my $score = $top<score> + 1 + $position;
      my $factor = $top<factor> * %factor-for{$roll};
      if $score >= $target {
         %mtw{@stack.elems}<wins> += $factor;
         next; # no "recursion"
      }

      push @stack, {
         position => $position,
         score    => $score,
         factor   => $factor,
         rolls    => [3 .. 9];
      };
   }

   my $n = 1;
   my $residuals = 1;
   my $max-moves = %mtw.keys».Int.max;
   while $residuals > 0 {
      die if $n > $max-moves;
      my $wins = %mtw{$n}<wins> //= 0;
      $residuals = %mtw{$n}<go-on> = $residuals * 27 - $wins;
      ++$n;
   }

   return %mtw;
}

sub part2 ($inputs) {
   my @mtws = $inputs.map: {moves-to-win($_)};

   my @wins;
   for 0, 1 -> $pid {
      my $player = @mtws[$pid];
      my $other  = @mtws[1 - $pid];
      my $n-wins = 0;
      for $player.kv -> $n-moves, $outcome {
         my $wins = $outcome<wins> or next;
         my $other-go-on = $other{$n-moves - 1 + $pid}<go-on>;
         $n-wins += $wins * $other-go-on;
      }
      push @wins, $n-wins;
   }

   return @wins.max;
}

sub play2 (*@args) {
   state %cache;
   state @die = (3,1), (4,3), (5,6), (6,7), (7,6), (8,3), (9,1);
   state &calc = sub ($p1, $p2, $s1, $s2) {
      return 0, 1 if $s2 >= 21;
      my ($w1, $w2) X= 0;
      for @die -> ($d, $n) {
         my $np1 = ($p1 + $d) % 10 || 10;
         my ($v2, $v1) = play2($p2, $np1, $s2, $s1 + $np1);
         ($w1, $w2) = $w1 + $v1 * $n, $w2 + $v2 * $n;
      }
      return $w1, $w2;
   };
   my $key = @args.join(',');
   return %cache{$key} //= &calc(|@args);
}

sub part2-play ($inputs) { return play2($inputs[0], $inputs[1], 0, 0).max }
