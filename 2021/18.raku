#!/usr/bin/env raku
use v6;
use Test;

class SnailFish { ... }

sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.sample')) {
   return solve($filename) if $filename.IO.e;

   my $L = SnailFish.new('[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]');
   ['left', $L.Str, $L.magnitude].note;
   my $R = SnailFish.new('[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]');
   ['right', $R.Str, $R.magnitude].note;
   my $sum = $L + $R;
   ['sum', $sum.Str, $sum.magnitude].note;

   my $ls = SnailFish.new('[[[[4,3],4],4],[7,[[8,4],9]]]');
   my $rs = SnailFish.new('[1,1]');
   ($ls + $rs).Str.say;
   ([+] ($ls, $rs)).Str.say;
   "\n\n\n".say;
   my $result = [+] $ls, $rs;
   $result.Str.say;
   $result.magnitude.say;

   my @magnitude-tests = (
      '[2,3]' => 12,
      '[1,[2,3]]' => 27,
      '[[1,2],[[3,4],5]]' => 143,
      '[[[[0,7],4],[[7,8],[6,0]]],[8,1]]' => 1384,
      '[[[[1,1],[2,2]],[3,3]],[4,4]]' => 445,
      '[[[[3,0],[5,3]],[4,4]],[5,5]]' => 791,
      '[[[[5,0],[7,4]],[5,5]],[6,6]]' => 1137,
      '[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]' => 3488,
   );
   for @magnitude-tests -> $pair {
      my ($sftxt, $magnitude) = $pair.kv;
      my $sf = SnailFish.new($sftxt);
      is $sf.magnitude, $magnitude, "magnitude $sftxt";
      is $sf.Str, $sftxt, "freezing $sftxt";
   }

   my @sum-tests =
      [
         '[[[[0,7],4],[[7,8],[6,0]]],[8,1]]',
         '[[[[4,3],4],4],[7,[[8,4],9]]]',
         '[1,1]',
      ],
      [
         '[[[[1,1],[2,2]],[3,3]],[4,4]]',
         '[1,1]',
         '[2,2]',
         '[3,3]',
         '[4,4]',
      ],
      [
         '[[[[3,0],[5,3]],[4,4]],[5,5]]',
         '[1,1]',
         '[2,2]',
         '[3,3]',
         '[4,4]',
         '[5,5]',
      ],
      [
         '[[[[5,0],[7,4]],[5,5]],[6,6]]',
         '[1,1]',
         '[2,2]',
         '[3,3]',
         '[4,4]',
         '[5,5]',
         '[6,6]',
      ],
      [
         '[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]',
         '[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]',
         '[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]',
         '[[2,[[0,8],[3,4]]],[[[6,7],1],[7,[1,6]]]]',
         '[[[[2,4],7],[6,[0,5]]],[[[6,8],[2,8]],[[2,1],[4,5]]]]',
         '[7,[5,[[3,8],[1,4]]]]',
         '[[2,[2,2]],[8,[8,1]]]',
         '[2,9]',
         '[1,[[[9,3],9],[[9,0],[0,7]]]]',
         '[[[5,[7,4]],7],1]',
         '[[[[4,2],2],6],[8,7]]',
      ],
      ;
   for @sum-tests -> ($exp, $first, *@addends) {
      my $got = [+] (SnailFish.new($first), @addends.Slip);
      is $got, $exp, "sum leading to $exp";
   }

   done-testing();
}

sub get-inputs ($filename) { $filename.IO.lines.Array }

sub solve ($filename) {
   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";

   my $inputs = get-inputs($filename);
   my ($start, $elapsed);

   $start = now;
   my $part1 = part1($inputs);
   $elapsed = now - $start;
   put "part1 ($elapsed) $highlight$part1$reset";

   $start = now;
   my $part2 = part2($inputs);
   $elapsed = now - $start;
   put "part2 ($elapsed) $highlight$part2$reset";

   return 0;
}

sub part1 ($inputs) {
   my @ins = @$inputs;
   @ins[0] = SnailFish.new(@ins[0]);
   ([+] @ins).Int;
}
sub part2 ($is) {
   my @sfs = $is.map: {SnailFish.new($_)};
   (@sfs X @sfs).map({ ([+] @$_).Int }).max;
}

class SnailFish {
   has $!string;
   has @!array;
   has $!magnitude = Nil;

   multi method new ($string) { self.bless(:$string, array => []) }
   submethod TWEAK (:$!string, :@!array) { self!_reduce() }
   method Str () { return $!string //= @!array.join('') }
   method !_reduce () {
      @!array = $!string.comb: / ( '[' | \d+ | ',' | ']' ) / unless @!array.elems;
      while self!explode || self!split {}
      $!string = Nil;
   }

   method !array () { @!array.List.Array }
   method generate ($l, $r) {
      my $L = ($l ~~ SnailFish ?? $l !! SnailFish.new($l))!array;
      my $R = ($r ~~ SnailFish ?? $r !! SnailFish.new($r))!array;
      my @array = ('[', @$L, ',', @$R, ']').flat;
      self.bless(string => Nil, array => @array);
   }

   method !split() {
      for ^@!array -> $i {
         next unless @!array[$i] ~~ /\d+/;
         my $v = @!array[$i].Int;
         next unless $v >= 10;
         my $rest  = $v % 2;
         my $left  = (($v - $rest) / 2).Int;
         my $right = $left + $rest;
         @!array.splice($i, 1, '[', $left, ',', $right, ']');
         return True;
      }
      return False;
   }

   method !explode () {
      my $depth = 0;
      my $last-number-index;
      my $candidate-index;
      for ^@!array -> $i {
         given @!array[$i] {
            when '[' {
               ++$depth;
               if $depth >= 5 {
                  $candidate-index = $i;
                  last;
               }
            }
            when ']' { --$depth }
            when ',' { }
            default  { $last-number-index = $i }
         }
      }
      return False unless defined $candidate-index;
      my ($left, $comma, $right) = @!array.splice($candidate-index + 1, 4);
      @!array[$candidate-index] = 0;
      @!array[$last-number-index] += $left if defined $last-number-index;
      for $candidate-index ^..^ @!array -> $i {
         next unless @!array[$i] ~~ /\d+/;# [ 'sumeq', @!array[$i], $right ].note;
         @!array[$i] += $right;
         last;
      }
      return True;
   }

   grammar Grammar {
      rule  TOP      { ^ <compound> $ }
      rule  compound { '[' <left> ',' <right> ']' }
      rule  left     { <elem> }
      rule  right    { <elem> }
      rule  elem     { <compound> | <value> }
      token value    { \d+ }
   }

   class Actions {
      method TOP ($/) { $/.make: $<compound>.made }
      method compound ($/) {
         $/.make: 3 * $<left>.made + 2 * $<right>.made;
      }
      method left ($/) { $/.make: $<elem>.made }
      method right ($/) { $/.make: $<elem>.made }
      method elem ($/) {
         $/.make: $<value> ?? $<value>.made !! $<compound>.made;
      }
      method value ($/) { $/.make: $/.Int }
   }

   method !calc-magnitude () {
      return Grammar.parse(self.Str, actions => Actions).made;
   }

   method magnitude () { $!magnitude //= self!calc-magnitude() }

   method !calc-magnitude-direct () {
      my $s = self.Str;
      with $s { s{\[ (\d+) ',' (\d+) \]} = $0 * 3 + $1 * 2 while /\D/ }
      return 0+$s;
   }

   method Int () { self.magnitude }
}

multi sub infix:<+> (SnailFish $l, $r) { SnailFish.generate($l, $r) }
