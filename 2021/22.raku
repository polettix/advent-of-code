#!/usr/bin/env raku
use v6;

sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.sample')) {
   return solve($filename);
}

sub solve ($filename) {
   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";

   my $inputs = get-inputs($filename);
   my ($start, $elapsed);

   $start = now;
   my $part1 = part1($inputs);
   $elapsed = now - $start;
   put "part1 ($elapsed) $highlight$part1$reset";

   $start = now;
   my $part2 = part2($inputs);
   $elapsed = now - $start;
   put "part2 ($elapsed) $highlight$part2$reset";

   return 0;
}

sub get-inputs ($filename) {
   $filename.IO.lines.map(
      {
         my $on-off = .substr(1,1) eq 'n';
         my @ranges = .comb(/ \-? \d+ /).map: -> $f, $t { (+$f, +$t) }
         ($on-off, @ranges);
      }
   ).List;
}

sub part1 ($inputs) {
   state $bounding-box = (-50, 50) xx 3;
   my @chunks = $inputs.map: { [$_[0], intersection($_[1], $bounding-box)] };
   measure(@chunks.grep({defined $_[1]}));
}

sub part2 ($inputs) { measure($inputs) }

# returns Nil if no intersection, $para of intersection otherwise
sub intersection ($para1, $para2) {
   my @para;
   for (@$para1 Z @$para2) -> ($ur, $vr) {
      my (\begin, \end) = max($ur[0], $vr[0]), min($ur[1], $vr[1]);
      return Nil unless begin <= end;
      @para.push: (begin, end);
   }
   return @para;
}

sub measure (@inputs) {
   return 0 unless @inputs.elems; # M(empty) = 0
   my ($head, @tail) = @inputs;
   my $tail-measure = measure(@tail);
   return $tail-measure unless $head[0];
   my @isects = @tail.map: { [True, intersection($_[1], $head[1])] };
   my $isects-measure = measure(@isects.grep({defined $_[1]}));
   my $volume = [*] $head[1].map({$_[1] - $_[0] + 1});
   return $volume + $tail-measure - $isects-measure;
}
