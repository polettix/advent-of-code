#!/usr/bin/env raku
use v6;

sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.sample')) {
   my $inputs = get-inputs($filename);
   my ($part1, $part2) = solve($inputs);

   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";
   put "part1 $highlight$part1$reset";
   put "part2 $highlight$part2$reset";
}

sub get-inputs ($filename) { $filename.IO.lines.Array }

sub solve ($inputs) {
   return (part1($inputs), part2($inputs));
}

class BitsParser { ... }

sub part1 ($inputs) {
   my @lengths;
   for @$inputs -> $input {
      my $p = BitsParser.new(hex => $input);
      my $P = $p.parse-packet();
      @lengths.push: $P.version-sum;
   }
   return @lengths.join(', ');
}

sub part2 ($inputs) {
   my @values;
   for @$inputs -> $input {
      my $p = BitsParser.new(hex => $input);
      my $P = $p.parse-packet();
      @values.push: $P.evaluate();
   }
   return @values.join(', ');
}

class BitsNode {
   has $.version is built is required;
   has $.type    is built is required;
   has $.value   is built is required;
   has $.bits    is built is required;

   method version-sum () {
      my $retval = $.version;
      if $.type != 4 {
         $retval += .version-sum for $.value.List;
      }
      return $retval;
   }

   method evaluate () { self.evaluate-by(self.type) }

   multi method evaluate-by (4) { self.value }
   multi method evaluate-by (0) { [+] self.value».evaluate }
   multi method evaluate-by (1) { [*] self.value».evaluate }
   multi method evaluate-by (2) { self.value».evaluate.min }
   multi method evaluate-by (3) { self.value».evaluate.max }
   multi method evaluate-by (5) { ([>] self.value».evaluate) ?? 1 !! 0 }
   multi method evaluate-by (6) { ([<] self.value».evaluate) ?? 1 !! 0 }
   multi method evaluate-by (7) { ([==] self.value».evaluate) ?? 1 !! 0 }

}

class BitsParser {
   has $!hex is built is required;
   has $!bits;
   has $!pos;

   submethod TWEAK () {
      $!hex ~~ /^ (0*) (.*) $/;
      my ($zeros, $hex) = @$/;
      $!bits = '';
      if $hex.chars {
         $!bits = $hex.Str.parse-base(16).base(2);
         $!bits = '0' ~ $!bits while $!bits.chars % 4;
      }
      if $zeros.chars {
         $!bits = ($zeros.Str x 4) ~ $!bits;
      }
      $!pos  = 0;
   }

   method !get-b (Int:D $n) {
      my $retval = $!bits.substr($!pos, $n);
      $!pos += $n;
      return +$retval;
   }
   method !get (Int:D $n) {
      my $bits = $!bits.substr($!pos, $n);
      $!pos += $n;
      return ($bits.parse-base(2), $bits);
   }

   method !version () { self!get(3) }
   method !type ()    { self!get(3) }
   method !literal () {
      my $go-on = 1;
      my $value = 0;
      my $bits = '';
      while $go-on {
         ($go-on, my $gb) = self!get(1);
         my ($ls, $lsb) = self!get(4);
         $bits ~= $gb ~ $lsb;
         $value = ($value +< 4) +| $ls;
      }
      return ($value, $bits);
   }

   method parse-packet () {
      my ($v, $vb) = self!version();
      my ($t, $tb) = self!type();
      my ($V, $Vb);
      if $t == 4 {
         ($V, $Vb) = self!literal();
      }
      else {
         my ($length-type, $lb) = self!get(1);
         ($V, $Vb) = $length-type ?? self!sequence-by-count()
                                  !! self!sequence-by-length();
         $Vb = $lb ~ $Vb;
      }
      return BitsNode.new(version => $v, type => $t, value => $V, bits => ($vb, $tb, $Vb).join(''));
   }

   method !sequence-by-length() {
      my ($n-bits, $bits) = self!get(15);
      $n-bits += 15;
      #"expecting overall $n-bits".say;
      my $children = [];
      while $bits.chars < $n-bits {
         my $child = self.parse-packet();
         $children.push: $child;
         $bits ~= $child.bits;
         #[ 'got child', $child ].say;
      }
      die 'wtf?!?' if $bits.chars != $n-bits;
      return ($children, $bits);
   }
   method !sequence-by-count() {
      my ($n-packets, $bits) = self!get(11);
      my $children = [];
      for 1 .. $n-packets {
         my $child = self.parse-packet();
         $children.push: $child;
         $bits ~= $child.bits;
      }
      return ($children, $bits);
   }
}
