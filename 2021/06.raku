#!/usr/bin/env raku
use v6;

sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.sample')) {
   my ($part1, $part2) = solve($filename);

   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";
   put "part1 $highlight$part1$reset";
   put "part2 $highlight$part2$reset";
}

sub solve ($filename) {
   my @n = 0 xx 9;
   @n[$_]++ for $filename.IO.lines.comb(/\d+/);
   my $part1;
   for 1 .. 256 -> $day {
      @n[($day + 6) % 9] += @n[($day + 8) % 9];
      $part1 = @n.sum if $day == 80;
   }
   return ($part1, @n.sum);
}
