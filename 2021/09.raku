#!/usr/bin/env raku
use v6;

sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.sample')) {
   my $inputs = get-inputs($filename);
   my ($part1, $part2) = solve($inputs);

   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";
   put "part1 $highlight$part1$reset";
   put "part2 $highlight$part2$reset";
}

sub get-inputs ($filename) {
   return { data => $filename.IO.lines».comb(/\d/)».Array };
}

sub solve ($inputs) {
   return (part1($inputs), part2($inputs));
}

sub part1 ($in) {
   my $inputs = $in<data>;
   my $lr = $inputs.end;      # last row
   my $lc = $inputs[0].end;   # last column
   my $sum = 0;               # result
   $in<low> = my @low;        # find out low points
   for 0 .. $lr -> $br {
      ITEM:
      for 0 .. $lc -> $bc {
         for [1, 0], [0, 1], [-1, 0], [0, -1] -> ($dr, $dc) {
            my ($r, $c) = $br + $dr, $bc + $dc;
            next unless 0 <= $r <= $lr && 0 <= $c <= $lc;
            next ITEM if $inputs[$br][$bc] >= $inputs[$r][$c];
         }
         $sum += 1 + $inputs[$br][$bc];
         @low.push: [$br, $bc];
      }
   }
   return $sum;
}

sub part2 ($inputs) {
   my $lr = $inputs<data>.end;     # last row
   my $lc = $inputs<data>[0].end;  # last column
   my %size-of;                    # size of each basin
   for $inputs<low>.List -> ($br, $bc) {  # iterate on basins' low pts
      my $key = "$br-$bc";         # to index %size-of
      my @queue = $($br, $bc,);    # starting point for flood fill
      while @queue.elems {
         my ($r, $c) = @queue.shift.List;
         next unless 0 <= $r <= $lr && 0 <= $c <= $lc;
         next if $inputs<data>[$r][$c] == 9; # edge or marked
         $inputs<data>[$r][$c] = 9;  # mark as done - DIRTY!
         ++%size-of{$key};

         # just add all candidates, will check later
         for [1, 0], [0, 1], [-1, 0], [0, -1] -> ($dr, $dc) {
            @queue.push: [$r + $dr, $c + $dc];
         }
      }
   }
   return [*] %size-of.values.sort.reverse.Array.splice(0, 3);
}
