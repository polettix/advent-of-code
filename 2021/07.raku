#!/usr/bin/env raku
use v6;

sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.sample')) {
   my $inputs = get-inputs($filename);
   my ($part1, $part2) = solve($inputs);

   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";
   put "part1 $highlight$part1$reset";
   put "part2 $highlight$part2$reset";
}

sub get-inputs ($file) { $file.IO.lines.comb(/\d+/)».Int.Array }

sub solve ($x) {
   return ($x.min .. $x.max).map( { ($x «-» $_)».abs.sum }).min,
      ($x.min .. $x.max)
         .map( { ($x «-» $_)».abs.map({($_ + 1) * $_ / 2}).sum }).min;
}
