#!/usr/bin/env raku
use v6;

sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.sample')) {
   my $inputs = get-inputs($filename);
   my ($part1, $part2) = solve($inputs);

   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";
   put "part1 $highlight$part1$reset";
   put "part2 $highlight$part2$reset";
}

sub get-inputs ($filename) {
   $filename.IO.basename.IO.lines».comb(/<[0 1]>/)».Array;
} ## end sub get_inputs ($filename = undef)

sub solve ($inputs) {
   return (part1($inputs), part2($inputs));
}

sub part1 ($inputs) {
   my @benchmarks = $inputs.elems / 2 xx $inputs[0].elems;
   my @sums = [Z+] @$inputs;
   my $epsilon = ((@sums Z< @benchmarks)».Int).join('');
   my $gamma = TR/01/10/ given $epsilon;
   return $epsilon.parse-base(2) * $gamma.parse-base(2);
}

sub part2 ($inputs) {
   my $result = 1;
   TARGET:
   for 0, 1 -> $t {
      my @candidates = @$inputs;
      for 0 .. @candidates[0].end -> $bit {
         my @s; # array of arrays of arrays, top indexed by 0, 1
         @s[$_[$bit]].push: $_ for @candidates;
         @candidates = @(@s[0] <= @s[1] ?? @s[$t] !! @s[1 - $t]);
         if (@candidates.elems == 1) {
            $result *= @candidates[0].join('').parse-base(2);
            next TARGET;
         }
      }
   }
   return $result;
}
