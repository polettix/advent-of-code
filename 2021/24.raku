#!/usr/bin/env raku
use v6;

sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.input')) {
   return solve($filename);
}

sub solve ($filename) {
   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";

   my $inputs = get-inputs($filename);
   my ($start, $elapsed);

   $start = now;
   my $part1 = part1($inputs<short>);
   $elapsed = now - $start;
   put "part1 ($elapsed) $highlight$part1$reset";

   $start = now;
   my $part2 = part2($inputs<short>);
   $elapsed = now - $start;
   put "part2 ($elapsed) $highlight$part2$reset";

   return 0;
}

sub part1 ($inputs) {
   my $sequence = eval-rec($inputs, True);
   die "WTF no sequence?!?" if $sequence.chars == 0;
   return $sequence;
}

sub part2 ($inputs) {
   my $sequence = eval-rec($inputs, False);
   die "WTF no sequence?!?" if $sequence.chars == 0;
   return $sequence;
}

sub get-inputs ($input) {
   my (@short, @short-cur);
   my (@full,  @full-cur);
   for $input.IO.lines -> $line {
      with $line {
         when /^ div \s+ z \s+ <( \-? \d+ )> / {
            @short-cur[0] = +$/;
         }
         when /^ add \s+ x \s+ <( \-? \d+ )> / {
            @short-cur[1] = +$/;
         }
         when /^ add \s+ y \s+ <( \-? \d+ )> / {
            @short-cur[2] = +$/;
         }
         when 'add z y' { @short.push: [@short-cur.List] }
      }

      my $op = $line.comb(/\S+/);
      if ($op[0] eq 'inp') {
         @full.push: [@full-cur.List] if @full-cur.elems;
         @full-cur = ();
      }
      else {
         @full-cur.push: $op;
      }
   }
   @full.push: [@full-cur.List] if @full-cur.elems;
   return {short => @short, full => @full};
}

sub eval-short ($func, $z, $w) {
   my ($i, $j, $k) = @$func;
   my $x = (($z % 26) + $j == $w) ?? 0 !! 1;
   #('eval-short', $z, $w, $x).note;
   ($z / $i).Int * (25 * $x + 1) + ($w + $k) * $x;
}

sub eval-rec ($funcs, $greatest = True, $depth = 0, $z = 0, @w = []) {
   return $z == 0 ?? @w.join('') !! '' if $depth == $funcs.elems;
   my $func = $funcs[$depth];
   my @candidates;
   if $func[0] == 26 { # might going backwards, yay!
      my $w = $z % 26 + $func[1];
      @candidates.push: $w if 1 <= $w <= 9;
   }
   else { # try 'em all...
      @candidates = 1 .. 9;
      @candidates = @candidates.reverse if $greatest;
   }
   for @candidates -> $w {
      my $outcome = eval-rec($funcs, $greatest, $depth + 1,
         eval-short($func, $z, $w), [@w.Slip, $w]);
      return $outcome if $outcome.chars;
   }
   return '';
}

# This is here just for historical reasons, can be used to double check stuff
sub eval-full ($func, $z, $w) {
   my %regs = z => $z, w => $w;
   #('start', %regs).note;
   for @$func -> ($op, $arg1, $arg2) {
      my $v2 = $arg2 ~~ /\d+/ ?? +$arg2 !! %regs{$arg2};
      given $op {
         when 'add' { %regs{$arg1} += $v2 }
         when 'mul' { %regs{$arg1} *= $v2 }
         when 'mod' {
            die 'mod' if %regs{$arg1} < 0 || $v2 == 0;
            %regs{$arg1} %= $v2 }
         when 'div' {
            die 'div' if $v2 == 0;
            %regs{$arg1} = (%regs{$arg1} / $v2).Int }
         when 'eql' { %regs{$arg1} = %regs{$arg1} == $v2 ?? 1 !! 0 }
         default { die "<$op> wtf?!?" }
      }
      #('     ', %regs, $op, $arg1, $arg2, $v2).note;
   }
   return %regs<z>;
}
