#!/usr/bin/env raku
use v6;

sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.sample')) {
   my ($part1, $part2) = solve($filename);

   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";
   put "part1 $highlight$part1$reset";
   put "part2 $highlight$part2$reset";

}

sub solve ($filename where *.IO.e) {
   my (%counts);;
   for $filename.IO.lines {
      my ($p1, $p2) = .comb(/ \-? \d+ /).map: {[$^x, $^y]};
      my $deltas = $p2 «-» $p1;
      $deltas = $deltas «/» $deltas».abs.max;
      my $is-hv = ([*] @$deltas) == 0; # is it horizontal/vertical?
      $p2 «+=» $deltas; # move one step ahead to include it too
      while $p1 !~~ $p2 {
         %counts<over-1>{$p1}++ if $is-hv && %counts<1>{$p1}++;
         %counts<over-2>{$p1}++ if           %counts<2>{$p1}++;
         $p1 «+=» $deltas;
      }
   }
   return %counts<over-1 over-2>».keys».elems;
}
