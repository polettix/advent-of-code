#!/usr/bin/env raku
use v6;

sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.sample')) {
   my $inputs = get-inputs($filename);
   $inputs = [$inputs, []];
   my ($part1, $part2) = solve($inputs);

   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";
   put "part1 $highlight$part1$reset";
   put "part2 $highlight$part2$reset";
}

sub get-inputs ($filename) {
   $filename.IO.lines».chomp».comb(/./)».Array;
}

sub solve ($inputs) {
   state %data-for = ')' => ['(', 3], ']' => ['[', 57],
      '}' => ['{', 1197], '>' => ['<', 25137];
   my $sum = 0;
   my @incomplete;
   SEQ:
   for $inputs[0].List -> @seq {
      my @stack;
      for @seq -> $item {
         if %data-for{$item}:exists {
            my $top = @stack.elems ?? @stack.pop !! '';
            if %data-for{$item}[0] ne $top {
               $sum += %data-for{$item}[1];
               next SEQ;
            }
         }
         else {
            @stack.push: $item;
         }
      }

      state %score-for = '(' => 1, '[' => 2, '{' => 3, '<' => 4;
      my $score = 0;
      $score = 5 * $score + %score-for{$_} for @stack.reverse;
      @incomplete.push: $score;
   }
   my $mid = (@incomplete.elems - 1) / 2;
   return $sum, @incomplete.sort[$mid];
}
