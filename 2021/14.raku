#!/usr/bin/env raku
use v6;

sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.sample')) {
   my $inputs = get-inputs($filename);
   my ($part1, $part2) = solve($inputs);

   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";
   put "part1 $highlight$part1$reset";
   put "part2 $highlight$part2$reset";
}

sub get-inputs ($filename) {
   my @words = $filename.IO.lines.comb: / \w+ /;
   my $start = @words.shift;
   my %new-letter-for = @words;
   return {
      start => $start,
      nlfor => %new-letter-for,
   };
}

sub solve ($inputs) {
   my $nlfor = $inputs<nlfor>;
   my @s = $inputs<start>.comb: / \w /;
   my $bag = (@s Z @s[1 .. *])».join('').Bag.Hash;
   my %count = @s.Bag.Hash;
   my $part1;
   for 1 .. 40 -> $step {
      my %new;
      for $bag.kv -> $key, $factor {
         my @items;
         if $nlfor{$key}:exists {
            my $m = $nlfor{$key};
            %count{$m} += $factor;
            my ($l, $r) = $key.comb: / \w /;
            @items.push: $l ~ $m, $m ~ $r;
         }
         else { @items.push: $key };
         for @items -> $item {
            %new{$item} //= 0;
            %new{$item} += $factor;
         }
      }
      $bag = %new;
      $part1 = %count.values.max - %count.values.min if $step == 10;
   }
   return $part1, %count.values.max - %count.values.min;
}
