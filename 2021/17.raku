#!/usr/bin/env raku
use v6;

sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.sample')) {
   my $inputs = get-inputs($filename);
   my ($part1, $part2) = solve($inputs);

   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";
   put "part1 $highlight$part1$reset";
   put "part2 $highlight$part2$reset";
}

sub get-inputs ($filename) {
   my @retval = $filename.IO.lines[0].comb(/\-?\d+/)
      .map(-> $a, $b {[(+$a, +$b).sort.List]});
   $_ = .abs for @retval[0][0], @retval[0][1];
   return @retval;
}

sub solve ($inputs) {
   return (part1($inputs), part2($inputs));
}

sub part1 ($inputs) {
   my $m = $inputs[1]».abs.max;
   return $m * ($m - 1) / 2;
}

sub part2 ($inputs) {
   my $count = 0;
   my @all = $inputs».List.flat;
   my $m = $inputs[1]».abs.max;
   for 0 .. $inputs[0][1] -> $vx {
      for -$m .. $m -> $vy {
         ++$count if hits($vx, $vy, @all);
      }
   }
   return $count;
}

sub hits ($vx is copy, $vy is copy, @limits) {
   my ($min-x, $max-x, $min-y, $max-y) = @limits;
   my ($x, $y) X= 0;
   while $vy >= 0 || $y >= $min-y {
      $x += $vx;
      $y += $vy;
      return 1 if $min-x <= $x <=  $max-x
               && $min-y <= $y <=  $max-y;
      --$vx if $vx > 0;
      --$vy;
   }
   return 0;
}
