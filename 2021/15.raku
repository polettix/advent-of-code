#!/usr/bin/env raku
use v6;

sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.sample')) {
   my $inputs = get-inputs($filename);
   my ($part1, $part2) = solve($inputs);

   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";
   put "part1 $highlight$part1$reset";
   put "part2 $highlight$part2$reset";
}


sub get-inputs ($filename) { $filename.IO.lines.map({[.comb: /\d/]}).Array }

sub solve ($inputs) {
   return solver($inputs, kr => 1, kc => 1),
          solver($inputs, kr => 5, kc => 5);
}

class Astar { ... }

sub solver ($b, :$kr!, :$kc!) {
   my $size = [$b.elems, $b[0].elems];
   my $max = ($size «*» ($kr, $kc)) «-» (1, 1);
   my $row-size = $max[1] + 1;
   my &cost = -> $p {
      my $v = $p «%» $size;
      my $base = $b[$v[0]][$v[1]];
      my $shift = ($p «/» $size)».Int.sum;
      1 + ($base + $shift - 1) % 9;
   };
   my $astar = Astar.new(
      heuristic => -> $v, $w { ($v «-» $w)».abs.sum },
      distance => -> $v, $w { &cost($w) },
      identifier => -> $v { $v[0] * $row-size + $v[1] },
      successors => -> $v {
         ((1, 0), (0, 1), (-1, 0), (0, -1)).map({ $v «+» $_ })
            .grep({.min >= 0 && ($max «-» $_).min >= 0});
      },
   );
   my $path = $astar.best-path([0, 0], $max);
   return $path.map(&cost).sum - $b[0][0];
}

class Astar {
   has (&!distance, &!successors, &!heuristic, &!identifier);

   method best-path ($start!, $goal!) {
      my ($id, $gid) = ($start, $goal).map: {&!identifier($^a)};
      my %node-for = $id => {pos => $start, g => 0};
      class PriorityQueue { ... }
      my $queue = PriorityQueue.new;
      $queue.enqueue($id, 0);
      while ! $queue.is-empty {
         my $cid = $queue.dequeue;
         my $cx = %node-for{$cid};
         next if $cx<visited>++;

         return self!unroll($cx, %node-for) if $cid eq $gid;

         my $cv = $cx<pos>;
         for &!successors($cv) -> $sv {
            my $sid = &!identifier($sv);
            my $sx = %node-for{$sid} ||= {pos => $sv};
            next if $sx<visited>;;
            my $g = $cx<g> + &!distance($cv, $sv);
            next if $sx<g>:exists && $g >= $sx<g>;
            $sx<p> = $cid; # p is the id of "best previous"
            $sx<g> = $g;   # with this cost
            $queue.enqueue($sid, $g + &!heuristic($sv, $goal));
         }
      }
      return ();
   }

   submethod BUILD (:&!distance!, :&!successors!,
      :&!heuristic = &!distance, :&!identifier = {~$^a}) {}

   method !unroll ($node is copy, %node-for) {
      my @path = $node<pos>;
      while $node<p>:exists {
         $node = %node-for{$node<p>};
         @path.unshift: $node<pos>;
      }
      return @path;
   }

   class PriorityQueue {
      has @!items  = ('-');
      method is-empty { @!items.end < 1 }
      method dequeue () { # includes "sink"
         return if @!items.end < 1;
         my $r = @!items.end > 1 ?? @!items.splice(1, 1, @!items.pop)[0] !! @!items.pop;
         my $k = 1;
         while (my $j = $k * 2) <= @!items.end {
            ++$j if $j < @!items.end && @!items[$j + 1]<w> < @!items[$j]<w>;
            last if @!items[$k]<w> < @!items[$j]<w>;
            (@!items[$j, $k], $k) = (|@!items[$k, $j], $j);
         }
         return $r<id>;
      }
      method enqueue ($id, $weight) { # includes "swim"
         @!items.push: {id => $id, w => $weight};
         my $k = @!items.end;
         (@!items[$k/2, $k], $k) = (|@!items[$k, $k/2], ($k/2).Int)
            while $k > 1 && @!items[$k]<w> < @!items[$k/2]<w>;
         return self;
      }
   }
}
