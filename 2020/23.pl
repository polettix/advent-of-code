#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
use List::Util qw< min max >;
$|++;

my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.input}rmxs;
open my $fh, '<', $filename;
chomp(my $input = <$fh>);
close $fh;

my @original_input = split m{}mxs, $input;

my $n = shift || -1;
my $i = shift || 100;
mixup2([@original_input], $n, $i);
#mixup2([@original_input], 1_000_000, 10_000_000);
exit 0;



{
   my @input = @original_input;
   mixup(\@input, 100);
   push @input, shift @input while $input[0] != 1;
   shift @input;
   say @input;
}

{
   my @input = @original_input;
   my $max = max @input;
   push @input, ($max + 1) .. 1_000_000;
   mixup(\@input, 100_000);
   my $i = -1;
   --$i while $input[$i] != 1;
   say "$i @input[$i, $i + 1, $i + 2]"; # (@input[0 .. 10]) (@input[$i .. 0])";
}

sub mixup ($input, $iterations) {
   my $max = max $input->@*;
   my $min = min $input->@*;
   push $input->@*, shift $input->@*;
   my $iteration = 1;
   while ($iteration <= $iterations) {
      my %is_removed = map {$_ => 1} (my @removed = splice $input->@*, 0, 3);
      my $target = $input->[-1];
      while ('necessary') {
         --$target;
         $target = $max if $target < $min;
         last unless $is_removed{$target};
      }
      my $i = $#$input;
      while ($i >= 0) {
         next if $input->[$i--] != $target;
         splice $input->@*, $i + 2, 0, @removed;
         push $input->@*, shift $input->@*;
         last;
      }
      print '.' if $iteration++ % 100_000 == 0;
   }
   return $input;
}

sub mixup2 ($input, $total = -1, $iterations = 100) {
   my @boxes = $input->@*;
   my $min_input = min @boxes;
   my $max_input = max @boxes;
   my @pred = my @succ = (@boxes, ($max_input + 1) .. $total);
   push @succ, shift @succ;
   unshift @pred, pop @pred;
   my $current = $boxes[0];

   # align label with position
   unshift @boxes, '0' for 1 .. $min_input;
   unshift @pred, '0' for 1 .. $min_input;
   unshift @succ, '0' for 1 .. $min_input;

   # Update $total
   $max_input = $total = $#pred;

   if ($total < 30) {
      push @boxes, ($max_input + 1) .. $total;
      say '(', join(' ', map {sprintf '%2d', $_} @boxes), ')';
   }

   # match label with (label - $min_input) position
   for my $i ($min_input .. $#boxes - 1) {
      next if $boxes[$i] == $i;
      for my $j ($i + 1 .. $#boxes) {
         next unless $boxes[$j] == $i;
         $_->@[$i, $j] = $_->@[$j, $i] for (\@boxes, \@pred, \@succ);
         last;
      }
   }

   if ($total < 30) {
      say '(', join(' ', map {sprintf '%2d', $_} @pred), ')';
      say '(', join(' ', map {sprintf '%2d', $_} @boxes), ')';
      say '(', join(' ', map {sprintf '%2d', $_} @succ), ')';
   }

   my $i = 1;
   while ($i <= $iterations) {
      printf "\r%lf     ", 100 * $i / $iterations if $i % 1000 == 0;
      my $chunk_start = $succ[$current];
      my $chunk_mid   = $succ[$chunk_start];
      my $chunk_end   = $succ[$chunk_mid];
      my $next        = $succ[$chunk_end];
      ($succ[$current], $pred[$next]) = ($next, $current);
      my $target = $current;
      while ('necessary') {
         $target--;
         $target = $max_input if $target < $min_input;
         last if ($target != $chunk_start)
            && ($target != $chunk_mid)
            && ($target != $chunk_end);
      }
      my $post_target = $succ[$target];
      ($succ[$target], $pred[$chunk_start]) = ($chunk_start, $target);
      ($succ[$chunk_end], $pred[$post_target]) = ($post_target, $chunk_end);
      ++$i;
      $current = $next;
   }

   my @chain = (0) x $max_input;

   for my $j (1 .. $max_input) {
      die "$j\n" if $succ[$pred[$j]] != $j;
   }

   my $p = 1;
   my @sequence;
   for (1 .. 8) {
      $p = $succ[$p];
      push @sequence, $p;
   }
   say "[@sequence]";
   say $sequence[0] * $sequence[1];
}
