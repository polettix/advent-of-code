#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;

my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.input}rmxs;
open my $fh, '<', $filename;
chomp(my @jolts = <$fh>);

my @sorted = (0, sort {$a <=> $b} @jolts);
push @sorted, $sorted[-1] + 3;
my %count_for;
$count_for{$sorted[$_] - $sorted[$_ - 1]}++ for 1 .. $#sorted;

say 'result1 -> ', $count_for{1} * $count_for{3};

my $prod = 1;
my $count = 0;
my ($x, $y, $z) = @sorted[0, 1, 2];
for my $i (2 .. $#sorted, undef) {
   my $z = defined $i ? $sorted[$i] : $sorted[-1] + 3;
   if ($x + 1 == $y && $y + 1 == $z) { # "internal"
      ++$count;
   }
   else {
      $prod *= fibolike($count);
      $count = 0;
   }
   ($x, $y) = ($y, $z); # advance window
}
say "prod<$prod>";

sub fibolike ($n) {
   state $c = [1, 2, 4];
   push $c->@*, $c->[-1] + $c->[-2] + $c->[-3] while $#$c < $n;
   return $c->[$n];
}
