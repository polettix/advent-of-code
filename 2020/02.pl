#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;

my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.input}rmxs;
open my $fh, '<', $filename;
my ($count1, $count2) = (0, 0);
while (<$fh>) {
   my ($min, $max, $char, $password) = split m{[^0-9a-zA-Z]+}mxs;
   my ($c1, $c2) = map { substr($password, $_ - 1, 1) || '' } $min, $max;
   $count2++ if ($char eq $c1 && $char ne $c2) || ($char eq $c2 && $char ne $c1);
   my $n = $password =~ s{$char}{}gmxs;
   $count1++ if $min <= $n && $n <= $max;
}
say "part1<$count1> part2<$count2>";
