#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;

my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.input}rmxs;
open my $fh, '<', $filename;
my @ids;
while (<$fh>) {
   chomp;
   push @ids, oct('0b' . tr{BFLR}{1001}r);
}
@ids = sort {$a <=> $b } @ids;
say "higher id<$ids[-1]>";

for my $i (0 .. $#ids - 1) {
   if ($ids[$i] == $ids[$i + 1] - 2) {
      my $seat = $ids[$i] + 1;
      say "empty seat<$seat>";
      last;
   }
}
