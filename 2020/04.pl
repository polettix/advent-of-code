#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;

my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.input}rmxs;
open my $fh, '<', $filename;
my @passports;

{
   local $/ = '';
   push @passports, {map { split m{:}mxs, $_, 2 } split m{\s+}mxs}
      for <$fh>;
}

my @mandatory_fields = qw< byr iyr eyr hgt hcl ecl pid >; # cid optional
my ($pre_valid, $valid) = (0, 0);
for my $passport (@passports) {
   my $count = grep {exists $passport->{$_}} @mandatory_fields;
   next if $count != @mandatory_fields;
   $pre_valid++;
   my ($byr, $iyr, $eyr, $hgt, $hcl, $ecl, $pid) =
      $passport->@{@mandatory_fields};
   next unless 1920 <= $byr && $byr <= 2002
      && 2010 <= $iyr && $iyr <= 2020
      && 2020 <= $eyr && $eyr <= 2030
      && $hcl =~ m{\A \# [0-9a-f]{6} \z}mxs
      && $ecl =~ m{\A (?:amb|blu|brn|gry|grn|hzl|oth) \z}mxs
      && $pid =~ m{\A \d{9} \z}mxs;
   my ($h, $u) = $hgt =~ m{\A (0|[1-9]\d*) (in|cm) \z}mxs or next;
   next if $u eq 'in' && !(59 <= $h && $h <= 76);
   next if $u eq 'cm' && !(150 <= $h && $h <= 193);
   ++$valid;
}

say "1. valid<$pre_valid>\n2. valid<$valid>";
