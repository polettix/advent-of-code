#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
$|++;

{
   my @field;
   my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.input}rmxs;
   open my $fh, '<', $filename;
   while (<$fh>) {
      s{\s+}{}gmxs;
      push @field, ['.', split(m{}mxs), '.']; # add edges
   }
   unshift @field, [('.') x $field[0]->@*];
   push @field, $field[0];
   my $save = dclone(\@field);

   my ($n_changed, $n_occupied) = (-1, 0);
   ($n_changed, $n_occupied) = one_round(\@field) while $n_changed != 0;
   print_field(\@field);
   say "part1<$n_occupied>";

   @field = dclone($save)->@*;
   my $adjacents = two_find_adjacents(\@field);
   $n_changed = -1;
   ($n_changed, $n_occupied) = two_round(\@field, $adjacents)
      while $n_changed != 0;
   say "part2<$n_occupied>";
}

sub print_field ($field) {
   my $n = $field->[0]->@* - 2;
   say join '', $field->[$_]->@[1 .. $n] for 1 .. $#$field - 1;
   say '';
}

sub one_round ($field) {
   my $n_changes = 0;
   my $n_occupied = 0;
   my $n = $field->[0]->@* - 2;
   my @new_field = $field->[0];
   for my $i (1 .. $#$field - 1) {
      my $oa = grep { $field->[$_][1] eq '#' } ($i - 1 .. $i + 1);
      push @new_field, ['.'];
      for my $j (1 .. $n) {
         my $cell = $field->[$i][$j];

         # update number of occupied seats around
         $oa-- if $cell eq '#'; # remove that very seat if occupied
         $oa += ($field->[$_][$j + 1] eq '#' ? 1 : 0)
              - ($field->[$_][$j - 2] eq '#' ? 1 : 0) for ($i - 1 .. $i + 1);
         $oa++ if $field->[$i][$j - 1] eq '#'; # previous one

         if ($cell eq 'L' && $oa == 0) {
            $cell = '#';
            ++$n_changes;
         }
         elsif ($cell eq '#' && $oa >= 4) {
            $cell = 'L';
            ++$n_changes;
         }
         $n_occupied++ if $cell eq '#';
         push $new_field[-1]->@*, $cell;
      }
      push $new_field[-1]->@*, '.';
   }
   push @new_field, $field->[-1];
   $field->@* = @new_field;
   return ($n_changes, $n_occupied)
}

sub two_find_adjacents ($field) {
   my $n = $field->[0]->@* - 2;
   my %adj;
   for my $i (1 .. $#$field - 1) {
      for my $j (1 .. $n) {
         next if $field->[$i][$j] eq '.';
         my $k = 1;
         my @done = (0) x 4;
         while ('necessary') {
            my $checked = 0;
            for my $candidate (
                  [0, $i + $k, $j],
                  [1, $i + $k, $j + $k],
                  [2, $i, $j + $k],
                  [3, $i - $k, $j + $k]) {
               my ($dir, $I, $J) = $candidate->@*;
               next if $done[$dir];
               if (($I < 1) || ($I > $#$field - 1) || ($J > $n)) {
                  $done[$dir] = 1;
                  next;
               }
               ++$checked;
               next if $field->[$I][$J] eq '.';
               push $adj{$i}{$j}->@*, [$I, $J];
               push $adj{$I}{$J}->@*, [$i, $j];
               $done[$dir] = 1;
            }
            last unless $checked;
            ++$k;
         }
      }
   }
   return \%adj;
}

sub two_round ($field, $adjs) {
   my $n_changes = 0;
   my $n_occupied = 0;
   my $n = $field->[0]->@* - 2;
   my @new_field = $field->[0];
   for my $i (1 .. $#$field - 1) {
      push @new_field, ['.'];
      for my $j (1 .. $n) {
         my $cell = $field->[$i][$j];

         # update number of occupied seats around
         my $oa = two_occupied_visible($field, $adjs, $i, $j);

         if ($cell eq 'L' && $oa == 0) {
            $cell = '#';
            ++$n_changes;
         }
         elsif ($cell eq '#' && $oa >= 5) {
            $cell = 'L';
            ++$n_changes;
         }
         $n_occupied++ if $cell eq '#';
         push $new_field[-1]->@*, $cell;
      }
      push $new_field[-1]->@*, '.';
   }
   push @new_field, $field->[-1];
   $field->@* = @new_field;
   return ($n_changes, $n_occupied)
}

sub two_occupied_visible ($field, $adjs, $i, $j) {
   return 0 if $field->[$i][$j] eq '.';
   my $count = 0;
   for my $adjacent ($adjs->{$i}{$j}->@*) {
      my ($I, $J) = $adjacent->@*;
      ++$count if $field->[$I][$J] eq '#';
   }
   return $count;
}
