#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
$|++;

my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.input}rmxs;
open my $fh, '<', $filename;

my %values_for;
my %overall;
while (<$fh>) {
   my ($field, $r11, $r12, $r21, $r22)
      = m{\A (.*?):.*?(\d+)-(\d+).*?(\d+)-(\d+)}mxs
      or last;
   $values_for{$field} = {map {$_ => 1} ($r11 .. $r12, $r21 .. $r22)};
   %overall = (%overall, $values_for{$field}->%*);
}

scalar <$fh>; # your ticket:
my @ticket = split m{[,\s]+}mxs, scalar <$fh>;

scalar<$fh> for 1 .. 2;
my @other_tickets = map { [split m{[,\s]+}mxs] } <$fh>;

my $sum1 = 0;
my @valid_tickets;
for my $other (@other_tickets) {
   my $valid = 1;
   for my $item ($other->@*) {
      next if $overall{$item};
      $sum1 += $item;
      $valid = 0;
   }
   push @valid_tickets, $other if $valid;
}
say $sum1;

my $n = keys %values_for;
my $outcome = solve_by_constraints(
   start => {
      values_for => \%values_for,
      valid_tickets => \@valid_tickets,
      meaning_of => [
         map { { map { $_ => 1 } keys %values_for } } 1 .. $n
      ],
      index_of => {},
   },
   is_done => sub ($state) { scalar(keys $state->{index_of}->%*) == scalar(keys $state->{values_for}->%*) },
   search_factory => sub ($state) {
die "wtf?!?";
   },
   constraints => [
      sub ($state) { # remove candidates out of range
         my $mo = $state->{meaning_of};
         my $index_of = $state->{index_of};
         my $changes = 0;
         for my $i (0 .. $#$mo) {
            my $moi = $mo->[$i];
            for my $meaning (keys $moi->%*) {
               my $values_for = $state->{values_for}{$meaning};
               for my $ticket ($state->{valid_tickets}->@*) {
                  if (! $values_for->{$ticket->[$i]}) {
                     $changes++;
                     delete $moi->{$meaning};
                     last;
                  }
               }
            }
            my $n_candidates = $moi->%*;
            die "no more candidates for <$i>" unless $n_candidates;
            if ($n_candidates == 1) {
               my ($meaning) = keys $moi->%*;
               die "double assignment for <$meaning>"
                  if exists($index_of->{$meaning}) && $index_of->{$meaning} != $i;
               $index_of->{$meaning} = $i;
            }
         }
         return $changes;
      },
      sub ($state) {
         my $mo = $state->{meaning_of};
         my $index_of = $state->{index_of};
         my $changes = 0;
         for my $i (0 .. $#$mo) {
            my $moi = $mo->[$i];
            for my $meaning (keys $moi->%*) {
               next unless exists $index_of->{$meaning};
               next if $index_of->{$meaning} == $i;
               $changes++;
               delete $moi->{$meaning};
            }
            my $n_candidates = $moi->%*;
            die "no more candidates for <$i>" unless $n_candidates;
            if ($n_candidates == 1) {
               my ($meaning) = keys $moi->%*;
               $index_of->{$meaning} = $i if $n_candidates == 1;
            }
         }
         return $changes;
      },
   ],
);

say Dumper $outcome->{index_of};
my $mult2 = 1;
while (my ($name, $index) = each $outcome->{index_of}->%*) {
   next unless $name =~ m{\A departure}mxs;
   $mult2 *= $ticket[$index];
}
say $mult2;



sub solve_by_constraints {
   my %args = (@_ && ref($_[0])) ? %{$_[0]} : @_;
   my @reqs = qw< constraints is_done search_factory start >;
   exists($args{$_}) || die "missing parameter '$_'" for @reqs;
   my ($constraints, $done, $factory, $state, @stack) = @args{@reqs};
   my $logger = $args{logger} // undef;
   while ('necessary') {
      last if eval {    # eval - constraints might complain loudly...
         $logger->(validating => $state) if $logger;
         my $changed = -1;
         while ($changed != 0) {
            $changed = 0;
            $changed += $_->($state) for @$constraints;
            $logger->(pruned => $state) if $logger;
         } ## end while ($changed != 0)
         $done->($state) || (push(@stack, $factory->($state)) && undef);
      };
      $logger->(backtrack => $state, $@) if $logger;
      while (@stack) {
         last if $stack[-1]->($state);
         pop @stack;
      }
      return unless @stack;
   } ## end while ('necessary')
   return $state;
} ## end sub solve_by_constraints
