#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
use List::Util 'sum';
$|++;

my @code;
my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.input}rmxs;
open my $fh, '<', $filename;
while (<$fh>) {
   if (my ($mask) = m{\A mask \s*=\s* (\S+) \s*\z}mxs) {
      my $fullmask = $mask;
      (my $baseline = $mask) =~ s{X}{0}gmxs;
      $baseline = oct('0b' . $baseline);
      $mask =~ s{[01]}{0}gmxs;
      $mask =~ s{X}{1}gmxs;
      $mask = oct('0b' . $mask);
      push @code, {op => 'mask', mask => $mask, baseline => $baseline, fullmask => $fullmask};
   }
   elsif (my ($address, $value) = m{\A mem\[(\d+)\] .*? (\d+)}mxs) {
      push @code, {op => 'mem', address => $address, value => $value};
   }
   else { die 'whatever' }
}

my %mem;
my ($mask, $baseline) = (0, 0xFFFFFFFF);
for my $line (@code) {
   if ($line->{op} eq 'mask') {
      ($mask, $baseline) = $line->@{qw< mask baseline >};
   }
   elsif ($line->{op} eq 'mem') {
      my ($address, $value) = $line->@{qw< address value >};
      $value = $value & $mask;
      $value = $baseline | ($value & $mask);
      $mem{$address} = $value;
   }
   else { die "wtf?!? <$line->{op}>" }
}

say Dumper \%mem;
say sum values %mem;

%mem = ();
my (@floaters, @ones);
for my $line (@code) {
   if ($line->{op} eq 'mask') {
      my $mask = $line->{fullmask};
      (@floaters, @ones) = ();
      for my $i (0 .. length($mask) - 1) {
         my $c = substr($mask, $i, 1);
         if ($c eq 'X') { push @floaters, $i }
         elsif ($c eq '1') { push @ones, $i }
      }
   }
   else {
      my ($address, $value) = $line->@{qw< address value >};
      my $addresses = addresses_for($address, \@ones, \@floaters);
      @mem{$addresses->@*} = ($value) x $addresses->@*;
   }
}
say sum values %mem;

sub addresses_for ($address, $ones, $floaters) {
   my @address = split m{}mxs, sprintf '%b', $address;
   unshift @address, (0) x (36 - @address);
   @address[$ones->@*] = (1) x ($ones->@*);
   my @addresses;
   for my $counter (0 .. 2 ** scalar($floaters->@*) - 1) {
      my @flags = reverse split m{}mxs, sprintf '%b', $counter;
      for my $i (0 .. $#$floaters) {
         my $position = $floaters->[$i];
         $address[$position] = $flags[$i] ? 1 : 0;
      }
      push @addresses, join '', @address;
   }
   return \@addresses;
}
