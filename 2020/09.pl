#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;

my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.input}rmxs;
open my $fh, '<', $filename;
chomp(my @numbers = <$fh>);

my $target;
for my $i (25 .. $#numbers) {
   if (! is_compliant($i, \@numbers)) {
      say "@{[$i+1]}. $numbers[$i]";
      $target = $numbers[$i];
      last;
   }
}

my $sum = $numbers[0];
my ($lo, $hi) = (0, 0);
while ($sum != $target) {
   if ($sum < $target) { $sum += $numbers[++$hi] }
   else { $sum -= $numbers[$lo++] }
}
my @range = sort {$a <=> $b} @numbers[$lo .. $hi];
my $code = $range[0] + $range[-1];
say "pass2<$code>";

sub is_compliant ($i, $numbers) {
   my $target = $numbers->[$i];
   for my $j ($i - 25 .. $i - 2) {
      my $J = $numbers->[$j];
      for my $k ($j + 1 .. $i - 1) {
         my $K = $numbers->[$k];
         next if $J == $K;
         return 1 if $J + $K == $target;
      }
   }
   return 0;
}
