#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
$|++;

{
   my @commands;
   my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.input}rmxs;
   open my $fh, '<', $filename;
   while (<$fh>) {
      my ($op, $arg) = m{\A (\S) (\S*) \s*\z}mxs;
      push @commands, [uc($op), $arg];
   }

   my ($e, $n, $d) = part_1(\@commands);
   my $m = abs($e) + abs($n);
   say "e<$e> n<$n> d<$d> manhattan<$m>";

   ($e, $n) = part_2(\@commands);
   $m = abs($e) + abs($n);
   say "e<$e> n<$n> manhattan<$m>";
}

sub part_1 ($commands) {
   my ($e, $n, $d) = (0, 0, 0);
   my @off = ([1, 0], [0, -1], [-1, 0], [0, 1]);
   my @of = $off[$d]->@*;
   for my $command ($commands->@*) {
      my ($op, $arg) = $command->@*;
      if ($op eq 'N')    { $n += $arg }
      elsif ($op eq 'S') { $n -= $arg }
      elsif ($op eq 'E') { $e += $arg }
      elsif ($op eq 'W') { $e -= $arg }
      elsif ($op eq 'F') { $e += $of[0] * $arg; $n += $of[1] * $arg }
      elsif ($op eq 'R') { $d = ($d + ($arg / 90)) % 4; @of = $off[$d]->@* }
      elsif ($op eq 'L') {
         $d = ($d + 4 - ($arg / 90)) % 4; 
         @of = $off[$d]->@*;
      }
      else { die "unknown operation <$op>" }
   }
   return ($e, $n, $d);
}

sub part_2 ($commands) {
   my ($e, $n, $we, $wn) = (0, 0, 10, 1);
   for my $command ($commands->@*) {
      my ($op, $arg) = $command->@*;
      if ($op eq 'N')    { $wn += $arg }
      elsif ($op eq 'S') { $wn -= $arg }
      elsif ($op eq 'E') { $we += $arg }
      elsif ($op eq 'W') { $we -= $arg }
      elsif ($op eq 'F') { $e += $we * $arg; $n += $wn * $arg }
      elsif ($op eq 'R' || $op eq 'L') {
         $arg = 360 - $arg if $op eq 'R';
         $arg += 360 while $arg < 0;
         $arg %= 360;
         ($we, $wn) = (-$wn, $we) for 1 .. ($arg / 90);
      }
      else { die "unknown operation <$op>" }
   }
   return ($e, $n);
}
