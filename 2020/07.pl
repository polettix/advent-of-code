#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;

my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.input}rmxs;
open my $fh, '<', $filename;
my %content_for;
while (<$fh>) {
   chomp;
   my ($color, $content) = m{\A (.*?) \s+ bags\ contain\s+(.*)}mxs;
   $content_for{$color} = \my %count_for;
   while (length $content) {
      if ($content =~ s{\A \s* (\d+) \s+ (.*?) \s+bags?[,.]}{}mxs) {
         $count_for{$2} = $1;
      }
      elsif ($content eq 'no other bags.') {
         last;
      }
      else {
         die "content<$content> yields errors\n";
      }
   }
}

my $sum1 = 0;
my $cache1 = {};
$sum1 += _count_containers_r(\%content_for, $_, 'shiny gold', $cache1)
   for keys %content_for;
say "sum1<$sum1>";

my $sum2 = count_bags(\%content_for, 'shiny gold') - 1; # remove root bag
say "sum2<$sum2>";

sub count_bags ($cf, $root, $cache = {}) {
   my $count = 1; # the root itself
   if (! defined $cache->{$root}) {
      while (my ($child, $n) = each $cf->{$root}->%*) {
         $count += $n * count_bags($cf, $child, $cache);
      }
   }
   return $cache->{$root} //= $count;
}

sub _count_containers_r ($cf, $root, $target, $cache) {
   return $cache->{$root} if exists $cache->{$root};
   $cache->{$root} = 0; # avoid infinite recursion
   my $cfr = $cf->{$root};
   return $cache->{$root} = 1 if $cfr->{$target};
   for my $child (keys $cfr->%*) {
      return $cache->{$root} = 1
         if _count_containers_r($cf, $child, $target, $cache);
   }
   return 0;
}
