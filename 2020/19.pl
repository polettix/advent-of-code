#!/usr/bin/env perl
use 5.024;
use warnings;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
$|++;

use Marpa::R2;

my @rules;
my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.input}rmxs;
open my $fh, '<', $filename;
while (<$fh>) {
   last unless m{\S}mxs;
   chomp;
   my ($id) = m{\A (\d+):}mxs;

   s{(\d+)}{Rule$1}gmxs;
   s{:}{ ::=}gmxs;
   s{"}{'}gmxs;

   $rules[$id] = $_;
}

my $dsl = join "\n",
   ':default ::= action => [name,values]',
   'lexeme default = latm => 1',
   @rules;

my $grammar = Marpa::R2::Scanless::G->new( { source => \$dsl } );
say scalar grep { eval { s{\s+}{}gmxs; $grammar->parse(\$_) } } <$fh>;
