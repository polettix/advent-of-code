#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;

my @code;

{
   my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.input}rmxs;
   open my $fh, '<', $filename;
   push @code, [ split m{\s+}mxs ] for <$fh>;
}

my (undef, $acc) = check_code(\@code);
say "part1<$acc>";

for my $i (0 .. $#code) {
   my $previous = $code[$i][0];
   next if $previous eq 'acc';
   $code[$i][0] = $previous eq 'nop' ? 'jmp' : 'nop';
   my ($outcome, $acc) = check_code(\@code);
   if ($outcome eq 'fine') {
      say "part2<$acc>";
      last;
   }
   $code[$i][0] = $previous;
}

sub check_code ($code) {
   my ($acc, $pc) = (0, 0);
   my %visit_flag;
   while ('necessary') {
      return (fine => $acc) if $pc > $#code;
      die "segmentation fault ;)\n" unless 0 <= $pc;
      return (loop => $acc) if $visit_flag{$pc}++;

      my ($op, $arg) = $code->[$pc]->@*;
      if ($op eq 'nop')    { ++$pc }
      elsif ($op eq 'acc') { $acc += $arg; ++$pc }
      elsif ($op eq 'jmp') { $pc  += $arg }
      else                 { die "unhandled op<$op>\n" }
   }
}
