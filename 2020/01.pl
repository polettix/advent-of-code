#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie qw< open close >;

use constant TARGET => 2020;

my %cache;
open my $fh, '<', '01-1.input';
while (<$fh>) {
   my $n = $_ + 0;
   my $complement = TARGET - $n;
   if ($cache{$complement}) {
      say "$n * $complement = ", $n * $complement;
   }
   $cache{$n} = 1;
}

my @candidates = keys %cache;
my %is_target = map { (TARGET - $_) => 1 } @candidates;

for my $x (@candidates) {
   for my $y (@candidates) {
      next if $y == $x;
      my $sum = $x + $y;
      if ($is_target{$sum}) {
         my $z = TARGET - $sum;
         if ($x != $z && $y != $z) {
            say "$x * $y * $z = ", $x * $y * $z;
         }
      }
   }
}
