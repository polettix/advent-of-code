#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
$|++;

my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.input}rmxs;
open my $fh, '<', $filename;
chomp(my $earliest = <$fh>);
my @buses = split m{[,\s]+}mxs, scalar <$fh>;

my @values;
my $N = 1;
my ($closer, $closer_wait);
for my $i (0 .. $#buses) {
   my $bus = $buses[$i];
   next unless $bus =~ m{\A \d+ \z}mxs;
   push @values, [$bus, ($bus - $i) % $bus];
   $N *= $bus;
   my $wait = $bus - ($earliest % $bus);
   ($closer, $closer_wait) = ($bus, $wait)
      if (! defined $closer) || ($wait < $closer_wait);
}
my $product = $closer * $closer_wait;
say "part1<$product>";

use List::Util 'reduce';
my $overall = reduce {crt($a->@*, $b->@*)}
map { say "($_->@*)"; $_ }
   map  { [$buses[$_], ($buses[$_] - $_) % $buses[$_]] }
   grep { $buses[$_] ne 'x' } 0 .. $#buses;
say "part2<$overall->[1]>";


sub crt ($n1, $r1, $n2, $r2) {
   my ($gcd, $x, $y) = egcd($n1, $n2);
   die "not coprime! <$n1> <$n2>" if $gcd != 1;
   my $N = $n1 * $n2;
   my $r = ($r2 * $x * $n1 + $r1 * $y * $n2) % $N;
   return [$N, $r];
}

sub egcd {    # https://en.wikipedia.org/wiki/Extended_Euclidean_algorithm
   use bigint;
   my ($X, $x, $Y, $y, $A, $B, $q) = (1, 0, 0, 1, @_);
   while ($A) {
      ($A, $B, $q) = ($B % $A, $A, int($B / $A));
      ($x, $X, $y, $Y) = ($X, $x - $q * $X, $Y, $y - $q * $Y);
   }
   return ($B, $x, $y);
} ## end sub egcd
