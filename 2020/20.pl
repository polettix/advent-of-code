#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper;
$Data::Dumper::Indent = 1;
use Storable 'dclone';
use List::Util 'max';
$|++;

my @tiles;
my %tiles_for;
{
   local $/ = '';
   my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.input}rmxs;
   open my $fh, '<', $filename;
   while (<$fh>) {
      chomp;
      my ($id, $data) = m{\ATile \s+ (\d+) :\s+ (.*)}mxs;
      my $sides = sides($data);
      push @tiles, {id => $id, data => $data, sides => $sides};
      push $tiles_for{$_}->@*, $tiles[-1] for $sides->@*;
   }
}

say scalar @tiles, ' tiles';
say Dumper $tiles[0];

while (my ($side, $tiles) = each %tiles_for) {
   next if $tiles->@* <= 2;
   say "$side -> $tiles->@*";
}

my $product = 1;
for my $tile (@tiles) {
   my $solitary_sides = 0;
   for my $side ($tile->{sides}->@*) {
      ++$solitary_sides if $tiles_for{$side}->@* == 1;
   }
   next if $solitary_sides < 2;
   say $tile->{id};
   $product *= $tile->{id};
}

say "product 1 = $product";

debug_tile($_) for @tiles;

my $tm = assemble(\@tiles, \%tiles_for);
my $full_image = merge_tiles($tm);

say 'roughness = ', check_monster($full_image);

sub debug_tile ($tile) {
   my @sides = $tile->{sides}->@*;
   say sprintf '+-%4s--%4s--%4s-+', ('----') x 3;
   say sprintf '| %4s  %4d  %4s |', '', $sides[1], '';
   say sprintf '| %4s  %4s  %4s |', ('') x 3;
   say sprintf '| %4d  %4d  %4d |', $sides[0], $tile->{id}, $sides[2];
   say sprintf '| %4s  %4s  %4s |', ('') x 3;
   say sprintf '| %4s  %4d  %4s |', '', $sides[3], '';
   say sprintf '+-%4s--%4s--%4s-+', ('----') x 3;
}

sub assemble ($tiles, $tiles_for) {
   my %unassigned = map { $_->{id} => $_ } $tiles->@*;

   my @tiles_matrix;
   for my $tile ($tiles->@*) {
      my @solitaries;
      for my $side ($tile->{sides}->@*) {
         push @solitaries, $side if $tiles_for->{$side}->@* == 1;
      }
      next if @solitaries < 2;
      say "solitaries for $tile->{id}: @solitaries";
      my $t = arrange($tile, @solitaries);
      push @tiles_matrix, [$t];
      delete $unassigned{$tile->{id}};
      last;
   }

   say "got corner $tiles_matrix[0][0]{id}";
   debug_tile($tiles_matrix[0][0]);

   while (scalar keys %unassigned) {
      my $current = $tiles_matrix[-1][-1];
      say "current $current->{id}";
      my $placement = 2; # go right if possible
      if ($tiles_for->{$current->{sides}[2]}->@* == 1) { # go down
         $current = $tiles_matrix[-1][0];
         $placement = 3; # go down from first of previous row
         push @tiles_matrix, [];
      }
      my $side = $current->{sides}[$placement];
      my $tiles = $tiles_for->{$side};
      my $neighbor = $tiles->[0]{id} == $current->{id} ? $tiles->[1] : $tiles->[0];
      say "neighbor ($placement) $neighbor->{id}";
      debug_tile($neighbor);
      delete $unassigned{$neighbor->{id}};
      my ($l, $u);
      if ($placement == 3) { # first of the row
         $u = $side;
         for my $candidate (adjacent_sides($neighbor, $side)) {
            next unless $tiles_for->{$candidate}->@* == 1; # is it border?
            $l = $candidate;
         }
      }
      elsif (@tiles_matrix == 1) { # on the first row, put border on top
         $l = $side;
         for my $candidate (adjacent_sides($neighbor, $side)) {
            next unless $tiles_for->{$candidate}->@* == 1; # is it border?
            $u = $candidate;
         }
      }
      else {
         $l = $side;
         $u = $tiles_matrix[-2][$tiles_matrix[-1]->@*]{sides}[3];
      }
      say "tile -> ($l, $u)";
      my $t = arrange($neighbor, $l, $u);
      debug_tile($t);
      push $tiles_matrix[-1]->@*, $t;
   }

   return \@tiles_matrix;
}

sub merge_tiles ($tiles) {
   my @all_lines;
   for my $row ($tiles->@*) {
      my @lines;
      for my $tile ($row->@*) {
         my @tile_lines = map {
            substr $_, 1, length($_) - 2
         } split m{\n}mxs, $tile->{data};
         shift @tile_lines;
         pop @tile_lines;
         ($lines[$_] //= '') .= $tile_lines[$_] for 0 .. $#tile_lines;
      }
      push @all_lines, @lines;
   }
   return join "\n", @all_lines;
}

sub adjacent_sides ($tile, $side) {
   for my $id (-1 .. 2) {
      return $tile->{sides}->@[$id - 1, $id + 1] if $tile->{sides}[$id] == $side;
   }
   die "whatever";
}

sub arrange ($tile, $l, $u) {
   my $t = dclone($tile);
   _rotate($t) while $t->{sides}[0] != $l; # put l in place
   _hflip($t) if $t->{sides}[1] != $u;
   return $t;
}

sub _rotate ($tile) { # one rotation
   _hflip(_transpose($tile));
}

sub _hflip ($tile) {
   $tile->{data} = join "\n", reverse split m{\n}mxs, $tile->{data};
   $tile->{sides}->@[1, 3] = $tile->{sides}->@[3, 1];
   return $tile;
}

sub _transpose ($tile) {
   my @data = split m{\n}, $tile->{data};
   my @new_rows = ('') x @data;
   for my $col (@data) {
      my @chars = split m{}mxs, $col;
      $new_rows[$_] .= $chars[$_] for 0 .. $#chars;
   }
   $tile->{data} = join "\n", @new_rows;
   $tile->{sides}->@* = $tile->{sides}->@[1, 0, 3, 2];
   return $tile;
}

sub sides ($tile_data) {
   for ($tile_data) {
      my ($u) = m{\A(.*?)$}mxs;
      my ($d) = m{^([^\n]*)\z}mxs;
      my $l   = join '', m{^(.)}gmxs;
      my $r   = join '', m{([^\n])$}gmxs;
      return [
         map {
            tr{#.}{10};
            my $n = oct('0b' . $_);
            my $m = oct('0b' . scalar(reverse $_));
            $n < $m ? $n : $m;
         } ($l, $u, $r, $d)
      ];
   }
}

sub check_monster ($image) {

   my $tile = {data => $image, sides => [0 .. 3]};
   my $monster = <<'END';
                  # 
#    ##    ##    ###
 #  #  #  #  #  #   
END

   my @monster = split m{\n}mxs, $monster;;
   my @mpos;
   my ($maxi, $maxj);
   for my $i (0 .. $#monster) {
      my @line = split m{}mxs, $monster[$i];
      for my $j (0 .. $#line) {
         next if $line[$j] ne '#';
         push @mpos, [$i, $j];
         $maxi = max($maxi // $i, $i);
         $maxj = max($maxj // $j, $j);
      }
   }

   for (1 .. 2) {
      for (1 .. 4) {
         my @lines = split m{\n}mxs, $tile->{data};
         my ($n, $m) = (scalar(@lines), length($lines[0]));
         for my $i (0 .. $n - $maxi - 1) {
            for my $j (0 .. $m - $maxj - 1) {
               attempt_overlay_monster(\@lines, \@mpos, $i, $j);
            }
         }
         $tile->{data} = join "\n", @lines;
         _rotate($tile);
      }
      _hflip($tile);
   }

   return scalar $tile->{data} =~ tr{#}{X};
}

sub attempt_overlay_monster ($lines, $mpos, $i, $j) {
   for my $pos ($mpos->@*) {
      my ($I, $J) = $pos->@*;
      return if substr($lines->[$i + $I], ($j + $J), 1) eq '.';
   }
   for my $pos ($mpos->@*) {
      my ($I, $J) = $pos->@*;
      substr($lines->[$i + $I], ($j + $J), 1, 'O');
   }
}
