#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;

my @groups;

{
   my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.input}rmxs;
   open my $fh, '<', $filename;
   local $/ = '';
   while (<$fh>) {
      chomp;
      my @group = map { { map {$_ => 1} split m{}mxs } } split m{\n+}mxs;
      push @groups, \@group;
   }
}

my $n = 0;
my $l = 0;
for my $group (@groups) {
   my %collective = map { $_->%* } $group->@*;
   $n += scalar keys %collective;
   my ($first, @others) = $group->@*;
   %collective = $first->%*;
   for my $element (@others) {
      my @missing = grep { ! exists $element->{$_} } keys %collective;
      delete @collective{@missing};
   }
   $l += scalar keys %collective;
}

say "any<$n> all<$l>";
