#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;

my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.input}rmxs;
open my $fh, '<', $filename;
my $col = 0;
my $n_cols;
my @grid = map { chomp; $_ } <$fh>;
my $count = check_slope(1, 3, \@grid);
say "count1<$count>";
$count *= check_slope($_->@*, \@grid) for (
   [1, 1],
   # [1, 3], # already done
   [1, 5],
   [1, 7],
   [2, 1],
);
say "mult2<$count>";

sub check_slope ($down, $right, $grid) {
   my ($i, $j) = (0, 0);
   my $n_cols = length $grid->[0];
   my $count = 0;
   while ('necessary') {
      $i += $down;
      last if $i > $#$grid;
      $j = ($j + $right) % $n_cols;
      $count++ if substr($grid->[$i], $j, 1) eq '#';
   }
   return $count;
}
