#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
$|++;

my @recipes;
my %is_black;
my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.input}rmxs;
open my $fh, '<', $filename;
while (<$fh>) {
   s{\s+}{}gmxs;
   my @recipe = m{\G(se|sw|ne|nw|e|w)}gmxs;
   push @recipes, \@recipe;
   my $landing_tile = join ',', landing_tile(\@recipe)->@*;
   delete $is_black{$landing_tile} if $is_black{$landing_tile}++;
}

say scalar keys %is_black;

my $state = \%is_black;
$state = conway_hex($state) for 1 .. 100;
say scalar keys $state->%*;

sub landing_tile ($recipe) {
   state $delta_for = {
      e  => [ 1,  0],
      se => [ 0,  1],
      sw => [-1,  1],
      w  => [-1,  0],
      nw => [ 0, -1],
      ne => [ 1, -1],
   };
   my ($x, $y) = (0, 0);
   for my $step ($recipe->@*) {
      my ($dx, $dy) = $delta_for->{$step}->@*;
      $x += $dx;
      $y += $dy;
   }
   return [$x, $y];
}

sub conway_hex ($previously_active) {
   state $delta_for = {
      e  => [ 1,  0],
      se => [ 0,  1],
      sw => [-1,  1],
      w  => [-1,  0],
      nw => [ 0, -1],
      ne => [ 1, -1],
   };
   my %count_for;
   for my $cell (keys $previously_active->%*) {
      my ($x, $y) = split m{,}mxs, $cell;
      for my $delta (values $delta_for->%*) {
         my ($X, $Y) = ($x + $delta->[0], $y + $delta->[1]);
         $count_for{"$X,$Y"}++;
      }
   }
   my %next_active;
   while (my ($key, $count) = each %count_for) {
      $next_active{$key} = 1
         if ($count == 2) || ($count == 1 && $previously_active->{$key});
   }
   return \%next_active;
}
