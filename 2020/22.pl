#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper;
$Data::Dumper::Indent = 1;
use Storable 'dclone';
$|++;
use List::Util 'sum';

{
   my @players;

   {
      my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.input}rmxs;
      open my $fh, '<', $filename;
      while (<$fh>) {
         if (m{^Player}mxs) { push @players, [] }
         elsif (m{\d}mxs) { push $players[-1]->@*, 0 + $_ }
      }
   }

   say Dumper \@players;

   say play1(dclone(\@players));

   say scalar play2(dclone(\@players));
}

sub play1 ($players) {
   my $n_cards = sum map { scalar $_->@* } $players->@*;
   while (! grep { $_->@* == $n_cards } $players->@*) {
      my @round;
      my $winner = 0;
      for my $i (0 .. $#$players) {
         say "Player $i: $players->[$i]->@*";
         next unless $players->[$i]->@*;
         push @round, shift $players->[$i]->@*;
         $winner = $i if $round[-1] > $round[$winner];
      }
      say "round @round";
      say "Player $winner wins";
      push $players->[$winner]->@*, reverse sort { $a <=> $b } @round;
   }
   for my $player ($players->@*) {
      next unless $player->@*;
      say join ', ', $player->@*;
      my $score = 0;
      my $weight = $n_cards;
      $score += $weight-- * $_ for $player->@*;
      return $score;
   }
   die 'wtf?!?';
}

sub play2 ($players, $game = 0) {
   my $n_cards = sum map { scalar $_->@* } $players->@*;
   my %ilf;
   while (! grep { $_->@* == $n_cards } $players->@*) {
      my $key = join '|', map { join ',', $_->@* } $players->@*;
      return 0 if $ilf{$key}++;
      my @round;
      my $winner = 0;
      my $have_enough = 1;
      for my $i (0 .. $#$players) {
         next unless $players->[$i]->@*;
         push @round, shift $players->[$i]->@*;
         $have_enough = 0 unless $round[-1] <= $players->[$i]->@*;
         $winner = $i if $round[-1] > $round[$winner];
      }
      if ($have_enough) { # recurse!
         my $j = 0;
         my @sub_deck = map {
            next unless $_->@*;
            [$_->@[0 .. $round[$j++] - 1]];
         } $players->@*;
         ($winner) = play2(\@sub_deck, $game + 1);
      }
      @round = reverse @round if $winner;
      push $players->[$winner]->@*, @round;
   }
   my $winner = -1;
   for my $player ($players->@*) {
      ++$winner;
      next unless $player->@*;
      my $score = 0;
      my $weight = $n_cards;
      $score += $weight-- * $_ for $player->@*;
      return ($winner, $score) if wantarray;
      return $score;
   }
   die 'wtf?!?';
}
