#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
$|++;

my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.input}rmxs;
open my $fh, '<', $filename;
my @input = split m{[,\s]+}mxs, scalar <$fh>;

my %turn_of;
my $this_spoken = 0;
my $next_spoken = 0;
my $turn = 0;
while ($turn < 2020) {
   ++$turn;
   $this_spoken = @input ? shift(@input) : $next_spoken;
   $next_spoken = exists $turn_of{$this_spoken}
      ? $turn - $turn_of{$this_spoken} : 0;
   $turn_of{$this_spoken} = $turn;
}
say "turn<$turn> last<$this_spoken>";

while ($turn < 30000000) {
   ++$turn;
   $this_spoken = @input ? shift(@input) : $next_spoken;
   $next_spoken = exists $turn_of{$this_spoken}
      ? $turn - $turn_of{$this_spoken} : 0;
   $turn_of{$this_spoken} = $turn;
}
say "turn<$turn> last<$this_spoken>";
