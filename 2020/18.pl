#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
$|++;

my $sum1 = 0;
my $sum2 = 0;
my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.input}rmxs;
open my $fh, '<', $filename;
while (<$fh>) {
   s{\s+}{}gmxs;
   $sum1 += parse1($_)->[0];
   $sum2 += parse2($_)->[0];
}
say $sum1;
say $sum2;

sub parse1 ($text) { return pf_PARSE(expression1())->($text) }
sub parse2 ($text) { return pf_PARSE(expression2())->($text) }

=begin grammar

expression = operand ( operation operand )*
operand = value | parenthesized_expression
parenthesized_expression = '(' expression ')'
value = /\d+/
operation = /[+*]/

=end grammar

=cut

sub expression1 {
   state $wrapped_expression;
   state $expression = sub { $wrapped_expression->(@_) };
   if (! $wrapped_expression) {
      my $parenthesized_expression = pf_match_and_filter(
         pf_sequence('(', $expression, ')'),
         sub ($match) { return $match->[1] } # "remove" parentheses
      );
      my $value = pf_regexp(qr{(\d+)}mxs);
      my $operation = pf_regexp(qr{([+*])}mxs);
      my $operand = pf_alternatives($value, $parenthesized_expression);
      $wrapped_expression = pf_match_and_filter(
         pf_sequence(
            $operand,
            pf_repeated(pf_sequence($operation, $operand)),
         ),
         sub ($match) {
            my $value = $match->[0][0];
            for my $opop ($match->[1]->@*) {
               my $operation = $opop->[0][0];
               my $operand   = $opop->[1][0];
               if ($operation eq '+') { $value += $operand }
               elsif ($operation eq '*') { $value *= $operand }
               else { die "wtf?!?" }
            }
            return [$value];
         }
      );
   }
   return $expression;
}

=begin grammar

expression = factor ( '*' factor )*
factor = addendum ( '+' addendum )*
addendum = value | parenthesized_expression
parenthesized_expression = '(' expression ')'
value = /\d+/

=end grammar

=cut

sub expression2 {
   state $real_expression;
   state $expression = sub { $real_expression->(@_) };
   if (! $real_expression) {
      my $value = pf_regexp(qr{(\d+)}mxs);
      my $parenthesized_expression = pf_match_and_filter(
         pf_sequence('(', $expression, ')'),
         sub ($match) { return $match->[1] } # "remove" parentheses
      );
      my $addendum = pf_alternatives($value, $parenthesized_expression);
      my $filter = sub ($match) {
         my $value = $match->[0][0];
         for my $opop ($match->[1]->@*) {
            my $operation = $opop->[0][0];
            my $operand   = $opop->[1][0];
            if ($operation eq '+') { $value += $operand }
            elsif ($operation eq '*') { $value *= $operand }
            else { die "wtf?!?" }
         }
         return [$value];
      };
      my $factor = pf_match_and_filter(
         pf_sequence($addendum, pf_repeated(pf_sequence(pf_exact('+'), $addendum))),
         $filter,
      );
      $real_expression = pf_match_and_filter(
         pf_sequence($factor, pf_repeated(pf_sequence(pf_exact('*'), $factor))),
         $filter,
      );
   }
   return $real_expression;
}


########################################################################

sub pf_alternatives {
   my (@A, $r) = @_;
   return sub { (defined($r = $_->($_[0])) && return $r) for @A; return };
}

sub pf_exact {
   my ($wlen, $what, @retval) = (length($_[0]), @_);
   unshift @retval, $what unless scalar @retval;
   return sub {
      my ($rtext, $pos) = ($_[0], pos ${$_[0]});
      $pos ||= 0;
      return if length($$rtext) - $pos < $wlen;
      return if substr($$rtext, $pos, $wlen) ne $what;
      pos($$rtext) = $pos + $wlen;
      return [@retval];
   };
}

sub pf_list {
   my ($w, $s, $sep_as_last) = @_; # (what, separator, sep_as_last)
   $s = pf_exact($s) if defined($s) && !ref($s);
   return sub {
      defined(my $base = $w->($_[0])) or return;
      my $rp = sub { return ($s && !($s->($_[0])) ? () : $w->($_[0])) };
      my $rest = pf_repeated($rp)->($_[0]);
      $s->($_[0]) if $s && $sep_as_last; # attempt last separator?
      unshift $rest->@*, $base;
      return $rest;
   };
}

sub pf_match_and_filter {
   my ($matcher, $filter) = @_;
   return sub {
      my $match = $matcher->($_[0]) or return;
      return $filter->($match);
   };
}

sub pf_PARSE {
   my ($expression) = @_;
   return sub {
      my $rtext = ref $_[0] ? $_[0] : \$_[0]; # avoid copying
      my $ast = $expression->($rtext) or die "nothing parsed\n";
      my $pos = pos($$rtext) || 0;
      my $delta = length($$rtext) - $pos;
      return $ast if $delta == 0;
      my $offending = substr $$rtext, $pos, 72;
      substr $offending, -3, 3, '...' if $delta > 72;
      die "unknown sequence starting at $pos <$offending>\n";
   };
}

sub pf_regexp {
   my ($rx, @forced_retval) = @_;
   return sub {
      my $rtext = $_[0];
      scalar(${$_[0]} =~ m{\G()$rx}cgmxs) or return;
      return scalar(@forced_retval) ? [@forced_retval] : [$2];
   };
}

sub pf_repeated { # *(0,-1) ?(0,1) +(1,-1) {n,m}(n,m)
   my ($w, $m, $M) = ($_[0], $_[1] || 0, (defined($_[2]) ? $_[2] : -1));
   return sub {
      my ($rtext, $pos, $lm, $lM, @retval) = ($_[0], pos ${$_[0]}, $m, $M);
      while ($lM != 0) { # lm = local minimum, lM = local maximum
         defined(my $piece = $w->($rtext)) or last;
         $lM--;
         push @retval, $piece;
         if ($lm > 0) { --$lm } # no success yet
         else         { $pos = pos $$rtext } # ok, advance
      }
      pos($$rtext) = $pos if $lM != 0;  # maybe "undo" last attempt
      return if $lm > 0;    # failed to match at least $min
      return \@retval;
   };
}

sub pf_sequence {
   my @items = map { ref $_ ? $_ : pf_exact($_) } @_;
   return sub {
      my ($rtext, $pos, @rval) = ($_[0], pos ${$_[0]});
      for my $item (@items) {
         if (defined(my $piece = $item->($rtext))) { push @rval, $piece }
         else { pos($$rtext) = $pos; return } # failure, revert back
      }
      return \@rval;
   };
}

{ my $r; sub pf_ws  { $r ||= pf_regexp(qr{(\s+)}) } }
{ my $r; sub pf_wso { $r ||= pf_regexp(qr{(\s*)}) } }
