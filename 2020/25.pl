#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
$|++;

use constant SUBJECT => 7;
use constant MODULUS => 20201227;

my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.input}rmxs;
open my $fh, '<', $filename;
chomp(my $key_public = <$fh>);
chomp(my $door_public = <$fh>);

my $key_secret = crack_public_key($key_public);

my $encryption_key = 1;
$encryption_key = one_iteration($encryption_key, $door_public)
   for 1 .. $key_secret;

say $encryption_key;

sub one_iteration ($v, $subject_number = SUBJECT, $modulus = MODULUS) {
   return (($v * $subject_number) % $modulus)
}

sub crack_public_key ($public, $sn = SUBJECT, $mod = MODULUS) {
   my $v = 1;
   my $loop_size = 0;
   while ($v != $public) {
      $v = one_iteration($v, $sn, $mod);
      ++$loop_size;
   }
   say "public<$public> loop_size<$loop_size>";
   return $loop_size;
}
