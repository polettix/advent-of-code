#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
$|++;

my %candidates_for;
my %count_for;

my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.input}rmxs;
open my $fh, '<', $filename;
while (<$fh>) {
   my ($ingredients, $allergens) = m{\A(.*?) \s+\(contains \s+ (.*)\)}mxs;
   my %candidates = map {$count_for{$_}++; $_ => 1} split m{\s+}, $ingredients;
   for my $allergen (split m{\s*,\s*}mxs, $allergens) {
      $candidates_for{$allergen} = intersect($candidates_for{$allergen}, \%candidates);
   }
}

say Dumper \%candidates_for;
my %food_for;
my %allergen_in;
my $moved = -1;
while ($moved != 0) {
   $moved = 0;
   for my $allergen (keys %candidates_for) {
      my @foods = keys $candidates_for{$allergen}->%*;
      next if @foods > 1;
      delete $candidates_for{$allergen};
      $food_for{$allergen} = $foods[0];
      $allergen_in{$foods[0]} = $allergen;
      delete $_->{$foods[0]} for values %candidates_for;
      ++$moved;
   }
}

say Dumper \%food_for;
say Dumper \%candidates_for;

my $count1 = 0;
while (my ($food, $occurrences) = each %count_for) {
   next if exists $allergen_in{$food};
   $count1 += $occurrences;
}
say "count 1 = $count1";

say join ',', map {$food_for{$_}} sort {$a cmp $b} keys %food_for;

sub intersect ($X, $Y) {
   return {$Y->%*} unless $X;
   return {map {$_ => 1} grep {$X->{$_}} keys $Y->%*};
}
