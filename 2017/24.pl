#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
$|++;

my $inputs = get_inputs(@ARGV); say Dumper($inputs);
my ($part1, $part2) = solve($inputs);

my $highlight = "\e[1;97;45m";
my $reset     = "\e[0m";
say "part1 $highlight$part1$reset";
say "part2 $highlight$part2$reset";

sub get_inputs ($filename = undef) {
   my %pairs_for;
   $filename //= basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
   open my $fh, '<', $filename;
   while (<$fh>) {
      my ($A, $B) = m{(\d+)}gmxs;
      die if exists $pairs_for{$A}{$B};
      $pairs_for{$A}{$B} = 1;
      $pairs_for{$B}{$A} = 1;
   }
   close $fh;
   return \%pairs_for;
} ## end sub get_inputs ($filename = undef)

sub solve ($inputs) {
   return (part1($inputs), part2($inputs));
}

sub part1 ($inputs) {
   my $ns = dclone($inputs);
   my @stack;
   my ($tip) = keys($ns->{0}->%*);
   my @exploration = ([0, 0, $tip, $tip, $tip]); # stack length, n1, n2, to
   my $max = 0;
   while (@exploration) {
      my $arc = pop @exploration;
      my ($sl, $n1, $n2, $to, $sum) = $arc->@*;
      while (@stack > $sl) {
         my ($N1, $N2) = pop(@stack)->@*;
         $ns->{$N1}{$N2} = $ns->{$N2}{$N1} = 1; # restore
      }
      push @stack, [$n1, $n2];
      $ns->{$n1}{$n2} = $ns->{$n2}{$n1} = 0; # remove
      my @nhps = grep { $ns->{$to}{$_} } keys(($ns->{$to} //= {})->%*);
      if (@nhps) { # can go on
         push @exploration, map { [$sl + 1, $to, $_, $_, $sum + $to + $_] } @nhps;
      }
      else { # bridge end reached
         $max = $sum if $max < $sum;
      }
   }
   return $max;
}

sub part2 ($inputs) {
   my $ns = dclone($inputs);
   my @stack;
   my ($tip) = keys($ns->{0}->%*);
   my @exploration = ([0, 0, $tip, $tip, $tip]); # stack length, n1, n2, to
   my $max = 0;
   my $maxl = 0;
   while (@exploration) {
      my $arc = pop @exploration;
      my ($sl, $n1, $n2, $to, $sum) = $arc->@*;
      while (@stack > $sl) {
         my ($N1, $N2) = pop(@stack)->@*;
         $ns->{$N1}{$N2} = $ns->{$N2}{$N1} = 1; # restore
      }
      push @stack, [$n1, $n2];
      $ns->{$n1}{$n2} = $ns->{$n2}{$n1} = 0; # remove
      my @nhps = grep { $ns->{$to}{$_} } keys(($ns->{$to} //= {})->%*);
      if (@nhps) { # can go on
         push @exploration, map { [$sl + 1, $to, $_, $_, $sum + $to + $_] } @nhps;
      }
      else { # bridge end reached
         if (@stack > $maxl) {
            $maxl = @stack;
            $max = $sum;
         }
         elsif (@stack == $maxl) {
            $max = $sum if $max < $sum;
         }
      }
   }
   return $max;
}
