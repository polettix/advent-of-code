#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
$|++;

my $inputs = get_inputs(@ARGV);
my ($part1, $part2) = solve($inputs);

my $highlight = "\e[41m"; my $reset = "\e[49m";
say "part1 $highlight$part1$reset";
say "part2 $highlight$part2$reset";

sub get_inputs ($filename = undef) {
   my $inputs = {};
   $filename //= basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
   open my $fh, '<', $filename;
   while (<$fh>) {
      chomp;
      push $inputs->{rows}->@*, $_;
   }
   close $fh;
   return $inputs;
}

sub solve ($inputs) {
   my $route = $inputs->{rows};
   my @accumulator;
   my ($x, $y) = (index($route->[0], '|'), 0);
   my ($dx, $dy) = (0, 1);
   my $n_steps = 0;
   while ((my $c = substr($route->[$y], $x, 1)) ne ' ') {
      if ($c =~ m{[a-z]}imxs) { push @accumulator, $c }
      elsif ($c eq '+') {
         ($dx, $dy) = $dx
            ? (0, (substr($route->[$y - 1], $x, 1) ne ' ' ? -1 : 1))
            : ((substr($route->[$y], $x - 1, 1) ne ' ' ? -1 : 1), 0);
      }
      $x += $dx;
      $y += $dy;
      $n_steps++;
   }
   return (join('', @accumulator), $n_steps);
}
