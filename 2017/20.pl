#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper;
$Data::Dumper::Indent = 1;
use Storable 'dclone';
use List::Util 'sum';
$|++;

my $inputs = get_inputs(@ARGV);
my ($part1, $part2) = solve($inputs);

my $highlight = "\e[1;97;45m";
my $reset     = "\e[0m";
say "part1 $highlight$part1$reset";
say "part2 $highlight$part2$reset";

sub get_inputs ($filename = undef) {
   my @particles;
   $filename //= basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
   open my $fh, '<', $filename;
   while (<$fh>) {
      s{\s+}{}gmxs;
      my @particle = m{(-?\d+)}gmxs;
      push @particles, \@particle;
   }
   close $fh;
   return \@particles;
} ## end sub get_inputs ($filename = undef)

sub solve ($inputs) {
   return (part1($inputs), part2($inputs));
}

sub part1 ($inputs) {
   my ($lid, $lsum);
   for my $id (0 .. $inputs->$#*) {
      my $sum = sum map { abs($_) } $inputs->[$id]->@[-3, -2, -1];
      ($lid, $lsum) = ($id, $sum) if (!defined $lid) || ($sum < $lsum);
   }
   return $lid;
} ## end sub part1 ($inputs)

sub part2 ($inputs) {
   my @inputs = $inputs->@*;

   my %collisions_at;
   for my $i (0 .. $#inputs - 1) {
      my $I = $inputs[$i];
      for my $j ($i + 1 .. $#inputs) {
         my $ct = collision_time($I, $inputs[$j]) or next;
         push $collisions_at{$ct}->@*, [$i, $j];
      }
   }

   for my $T (sort {$a <=> $b} keys %collisions_at) {
      my %to_be_deleted;
      for my $pair ($collisions_at{$T}->@*) {
         my ($i, $j) = $pair->@*;
         next unless $inputs[$i] && $inputs[$j];
         $to_be_deleted{$i} = $to_be_deleted{$j} = 1;
      }
      $inputs[$_] = undef for keys %to_be_deleted;
   }

   my $count = grep { defined $_ } @inputs;
   return $count;
} ## end sub part2 ($inputs)

sub collision_time ($p1, $p2) {
   my @T;
   for my $dim (0, 1, 2) {
      my $A = $p1->[6 + $dim] - $p2->[6 + $dim];
      my $B = 2 * ($p1->[3 + $dim] - $p2->[3 + $dim]) + $A;
      my $C = 2 * ($p1->[0 + $dim] - $p2->[0 + $dim]);
      if (@T) {
         @T = grep { ($C + $B * $_ + $A * $_ * $_) == 0 } @T or return;
         next;
      }
      if ($A) {
         my $det = $B * $B - 4 * $A * $C;
         return if $det < 0; # imaginary stuff... no crossing
         my $iroot = isqrt($det);
         return unless $iroot * $iroot == $det;
         for my $num (-$B - $iroot, -$B + $iroot) {
            next if $num % (2 * $A);
            my $T = $num / (2 * $A);
            next if $T < 0;
            push @T, $T;
         }
         return unless @T;
      }
      elsif ($B) {
         return if $C % $B; # $T must be integer
         @T = (- $C / $B);
      }
      else {
         return if $C; # constant... must be 0
      }
   }
   return @T == 1 || $T[0] < $T[1] ? $T[0] : $T[1];
}

sub isqrt ($N) {
   return $N if $N <= 1;
   my ($lo, $hi) = (0, $N);
   while ($lo < $hi - 1) {
      my $sum = $lo + $hi;
      my $k = ($sum % 2)? ($sum + 1) / 2 : $sum / 2;
      my $k2 = $k * $k;
      return $k if $k2 == $N;
      $lo = $k if $k2 < $N;
      $hi = $k if $k2 > $N;
   }
   return $lo;
}
