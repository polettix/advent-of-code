#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
$|++;

my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
open my $fh, '<', $filename;
while (<$fh>) {
   chomp;
   my $previous = substr $_, -1, 1;
   my $sum = 0;
   for my $i (0 .. length($_) - 1) {
      my $c = substr $_, $i, 1;
      $sum += $c if $c == $previous;
      $previous = $c;
   }
   say "part1 -> $sum";

   $sum = 0;
   my $l = length($_);
   next if $l % 2;
   my $hl = $l / 2;
   for my $i (-$hl .. - 1) {
      my $c = substr $_, $i, 1;
      my $d = substr $_, $i + $hl, 1;
      $sum += $c if $c == $d;
   }
   $sum *= 2;
   say "part2 -> $sum";
}
close $fh;
