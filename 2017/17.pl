#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
$|++;

use constant PART2 => 50_000_000;

my $inputs = get_inputs(@ARGV);
my ($part1, $part2) = solve($inputs);

my $highlight = "\e[41m"; my $reset = "\e[49m";
say "part1 $highlight$part1$reset";
say "part2 $highlight$part2$reset";

sub get_inputs ($filename = undef) {
   $filename //= basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
   open my $fh, '<', $filename;
   chomp(my $inputs = <$fh>);
   close $fh;
   return $inputs;
}

sub solve ($inputs) {
   return (part1($inputs), part2($inputs));
}

sub part1 ($advance) {
   my @buffer = (0);
   my $i = 0;
   my $current = 0;
   while (++$i < 2018) {
      my $insertion = ($current + $advance) % @buffer + 1;
      splice @buffer, $insertion, 0, $i;
      $current = $insertion;
   }
   return $buffer[($current + 1) % @buffer];
}

sub part2 ($advance) {
   my $pos0 = 0;
   my $after0 = undef;
   my $i = 0;
   my $current = 0;
   my $size = 1;
   while (++$i <= PART2) {
      my $insertion = ($current + $advance) % $size + 1;
      if ($insertion <= $pos0) { ++$pos0 }
      elsif ($insertion == $pos0 + 1) { $after0 = $i }
      $current = $insertion;
      ++$size;
   }
   return $after0;
}
sub part2_bf ($advance) {
   my @buffer = (0);
   my $i = 0;
   my $current = 0;
   while (++$i <= PART2) {
      my $insertion = ($current + $advance) % @buffer + 1;
      splice @buffer, $insertion, 0, $i;
      $current = $insertion;
   }
   $i = 0;
   while ($i <= $#buffer) {
      return $buffer[$i] unless $buffer[$i - 1];
      ++$i;
   }
   die "wtf?";
}
