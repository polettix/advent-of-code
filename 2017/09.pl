#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
$|++;

my @groups;

my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
open my $fh, '<', $filename;
chomp(@groups = <$fh>);
close $fh;

for my $group (@groups) {
   say 'part 1 => ', part1($group);
   say 'part 2 => ', part2($group);
}

sub part1 ($item) {
   for ($item) {
      s{!.}{}gmxs;
      s{<.*?>}{}gmxs;
      s{[^{}]}{}gmxs;
      my $level = 0;
      my $score = 0;
      for my $i (0 .. length() - 1) {
         my $c = substr $_, $i, 1;
         if ($c eq '{')    { $score += ++$level }
         elsif ($c eq '}') { --$level }
      }
      return $score;
   }
}

sub part2 ($item) {
   for ($item) {
      s{!.}{}gmxs;
      my $score = 0;
      while (s{<(.*?)>}{}mxs) { $score += length($1) };
      return $score;
   }
}
