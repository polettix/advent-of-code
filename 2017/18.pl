#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
$|++;

my $inputs = get_inputs(@ARGV);
my ($part1, $part2) = solve($inputs);

my $highlight = "\e[41m"; my $reset = "\e[49m";
say "part1 $highlight$part1$reset";
say "part2 $highlight$part2$reset";

sub get_inputs ($filename = undef) {
   my $inputs = {};
   $filename //= basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
   open my $fh, '<', $filename;
   while (<$fh>) {
      push $inputs->{code}->@*, [split m{\s+}mxs];
   }
   close $fh;
   return $inputs;
}

sub solve ($inputs) {
   return (part1($inputs), part2($inputs));
}

sub resolve ($state, $operand) {
   return $operand if $operand =~ m{\d}mxs;
   return $state->{value_for}{$operand};
}

sub step1 ($state, $code) {
   my $pc = $state->{pc} // die "invalid pc\n";
   my ($op, @args) = $code->[$pc]->@*;
   $pc++;
   if ($op eq 'set') { $state->{value_for}{$args[0]} = resolve($state, $args[1]) }
   elsif ($op eq 'mul') { $state->{value_for}{$args[0]} *= resolve($state, $args[1]) }
   elsif ($op eq 'mod') { $state->{value_for}{$args[0]} %= resolve($state, $args[1]) }
   elsif ($op eq 'add') { $state->{value_for}{$args[0]} += resolve($state, $args[1]) }
   elsif ($op eq 'snd') { $state->{tune} = resolve($state, $args[0]) }
   elsif ($op eq 'rcv') { $state->{recv} = $state->{tune} if resolve($state, $args[0]) }
   elsif ($op eq 'jgz') { $pc += resolve($state, $args[1]) - 1 if resolve($state, $args[0]) > 0 }
   else                 { die "wtf?!? '$op' -> (@args)\n" }
   $state->{pc} = (0 <= $pc && $pc <= $code->$#*) ? $pc : undef;
   return $state;
}

sub part1 ($inputs) {
   my $state = { recv => undef, pc => 0 }; eval {
   step1($state, $inputs->{code}) while ! defined $state->{recv}; };
   return $state->{recv};
}

sub dumpable ($process) {
   my $regs = join ', ', map { "$_<$process->{value_for}{$_}>" } keys $process->{value_for}->%*;
   return "pc<$process->{pc}> ($regs) [$process->{inqueue}->@*]";
}

sub step2 ($state, $code) {
   my $pc = $state->{pc} // die "invalid pc\n";
   my ($op, @args) = $code->[$pc]->@*;
   $pc++;
   if ($op eq 'set') { $state->{value_for}{$args[0]} = resolve($state, $args[1]) }
   elsif ($op eq 'mul') { $state->{value_for}{$args[0]} *= resolve($state, $args[1]) }
   elsif ($op eq 'mod') { $state->{value_for}{$args[0]} %= resolve($state, $args[1]) }
   elsif ($op eq 'add') { $state->{value_for}{$args[0]} += resolve($state, $args[1]) }
   elsif ($op eq 'snd') { push $state->{outqueue}->@*, resolve($state, $args[0]); $state->{n_sent}++ }
   elsif ($op eq 'rcv') {
      if ($state->{inqueue}->@*) {
         $state->{value_for}{$args[0]} = shift $state->{inqueue}->@*;
      }
      else { $state->{waiting} = 1; $pc-- }
   }
   elsif ($op eq 'jgz') { $pc += resolve($state, $args[1]) - 1 if resolve($state, $args[0]) > 0 }
   else                 { die "wtf?!? '$op' -> (@args)\n" }
   $state->{pc} = (0 <= $pc && $pc <= $code->$#*) ? $pc : undef;
   return 1;
}
sub part2 ($inputs) {
   my @processes = map {;
      {
         pc => 0,
         value_for => {p => $_},
         waiting => 0,
         n_sent => 0,
      }
   } (0, 1);
   $processes[$_]{inqueue} = $processes[0+!$_]{outqueue} = [] for (0, 1);
   while ('necessary') {
      my $worked = 0;
      for my $process (@processes) {
         $process->{waiting} = 0 if $process->{inqueue}->@*;
         $worked += step2($process, $inputs->{code})
            while defined($process->{pc}) && ! $process->{waiting};
      }
      last unless $worked;
   }
   return $processes[1]{n_sent};
}
