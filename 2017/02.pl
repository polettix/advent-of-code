#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
use List::Util qw< min max sum >;
$|++;

my $part1 = 0;
my $part2 = 0;

my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
open my $fh, '<', $filename;
while (<$fh>) {
   my @items = split m{\s+}mxs;
   $part1 += max(@items) - min(@items);
   for my $i (0 .. $#items - 1) {
      for my $j ($i + 1 .. $#items) {
         my ($x, $y) = @items[$i, $j];
         ($x, $y) = ($y, $x) if $y < $x;
         next if $y % $x;
         $part2 += $y / $x;
      }
   }
}
close $fh;

say "part1 => $part1";
say "part2 => $part2";
