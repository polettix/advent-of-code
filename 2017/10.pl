#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
use List::Util 'reduce';
$|++;

my $n_items;
my @lengths;
my @lengths2;

my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
open my $fh, '<', $filename;
chomp($n_items = <$fh>);
chomp(my $lengths = <$fh>);
@lengths = split m{[,\s]+}mxs, $lengths;
@lengths2 = map { ord($_) } split m{}mxs, $lengths =~ s{\A\s+|\s+\z}{}grmxs;
my @suffix = (17, 31, 73, 47, 23);
push @lengths2, @suffix;
close $fh;

say 'part1 => ', part1($n_items, \@lengths);
say 'part2 => ', join ' ', part2($n_items, \@lengths2);

sub _iterations ($n_items, $lengths, $n) {
   my @buffer = 0 .. $n_items - 1;
   my $current = -$n_items;
   my $skip = 0;
   my $i = 0;
   while ($n-- > 0) {
      for my $length ($lengths->@*) {
         next if $length > $n_items;
         my $last = $current + $length;
         my @range = $current .. $last - 1;
         @buffer[@range] = @buffer[reverse @range];
         $current = $last + $skip++;
         $skip %= $n_items;
         $current -= $n_items while $current >= 0;
      }
   }
   return @buffer;
}

sub part1 ($n_items, $lengths) {
   my @buffer = _iterations($n_items, $lengths, 1);
   return $buffer[0] * $buffer[1];
}

sub part2 ($n_items, $lengths) {
   my @sparse = _iterations($n_items, $lengths, 64);
   die unless @sparse == $n_items;
   my @dense;
   push @dense, reduce { $a ^ $b } splice @sparse, 0, 16 while @sparse > 0;
   die unless @dense == $n_items / 16;
   join '', map {sprintf '%02x', $_} @dense;
}
