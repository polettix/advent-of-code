#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
$|++;

my @moves;

my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
open my $fh, '<', $filename;
chomp(my $mvs = <$fh>);
@moves = split m{,}mxs, $mvs;
close $fh;

say 'part1 => ', part1(@moves);
say 'part2 => ', part2(@moves);

sub move_to ($x, $y, $d) {
   state $move_for = {
      'n'  => [ 0,  1],
      's'  => [ 0, -1],
      'nw' => [-1,  0],
      'se' => [ 1,  0],
      'ne' => [ 1,  1],
      'sw' => [-1, -1],
   };
   my ($dx, $dy) = $move_for->{$d}->@*;
   return ($x + $dx, $y + $dy);
}

sub part1 (@moves) {
   my ($x, $y) = (0, 0);
   ($x, $y) = move_to($x, $y, $_) for @moves;
   return distance_from_origin($x, $y);
}

sub part2 (@moves) {
   my ($x, $y) = (0, 0);
   my $max_distance = 0;
   for my $move (@moves) {
      ($x, $y) = move_to($x, $y, $move);
      my $d = distance_from_origin($x, $y);
      $max_distance = $d if $d > $max_distance;
   }
   return $max_distance;
}

sub distance_from_origin ($x, $y) {
   if ($x * $y > 0) { # same sign... optimize!
      ($x, $y) = (-$x, -$y) if $x < 0;
      ($x, $y) = ($y, $x) if $y < $x;
      $y -= $x;
   }
   return abs($x) + abs($y);
}
