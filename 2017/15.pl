#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
$|++;

my $inputs = get_inputs(@ARGV);
my ($part1, $part2) = solve($inputs);

my $highlight = "\e[41m"; my $reset = "\e[49m";
say "part1 $highlight$part1$reset";
say "part2 $highlight$part2$reset";

sub get_inputs ($filename = undef) {
   my $inputs = {};
   $filename //= basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
   open my $fh, '<', $filename;
   while (<$fh>) {
      my ($name, $start) = m{\A Generator\s+ (\S+) .*? (\d+) \s*\z}mxs;
      $inputs->{$name} = $start;
   }
   close $fh;
   return $inputs;
}

sub solve ($inputs) {
   return (part1($inputs), part2($inputs));
}

sub generator ($start, $factor, $modulus = 2147483647) {
   return sub { $start = ($start * $factor) % $modulus };
}

sub filter_by ($generator, $n) {
   return sub {
      my $x = $generator->();
      $x = $generator->() while $x % $n;
      return $x;
   };
}

sub part1 ($inputs) {
   my $A = generator($inputs->{A}, 16807);
   my $B = generator($inputs->{B}, 48271);
   my $count = 0;
   my $i = 0;
   while ($i++ < 40_000_000) {
      my $av = $A->() & 0xFFFF;
      my $bv = $B->() & 0xFFFF;
      $count++ if $av == $bv;
   }
   return $count;
}
sub part2 ($inputs) {
   my $A = filter_by(generator($inputs->{A}, 16807), 4);
   my $B = filter_by(generator($inputs->{B}, 48271), 8);
   my $count = 0;
   my $i = 0;
   while ($i++ < 5_000_000) {
      my $av = $A->() & 0xFFFF;
      my $bv = $B->() & 0xFFFF;
      $count++ if $av == $bv;
   }
   return $count;
}
