#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
$|++;

my $part1 = 0;
my $part2 = 0;

my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
open my $fh, '<', $filename;
while (<$fh>) {
   chomp;
   ++$part1 if is_valid_1($_);
   ++$part2 if is_valid_2($_);
}
close $fh;

say "part1 => $part1";
say "part2 => $part2";

sub is_valid_1 ($passphrase) {
   my @words = split m{\s+}, $passphrase;
   die '1 word' unless @words > 1;
   my %present;
   for (@words) { return if $present{$_}++ }
   return 1;
}

sub is_valid_2 ($passphrase) {
   my @words = split m{\s+}, $passphrase;
   die '1 word' unless @words > 1;
   my %present;
   for (@words) {
      $_ = join '', sort {$a cmp $b} split m{}mxs;
      return if $present{$_}++;
   }
   return 1;
}
