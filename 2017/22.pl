#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
$|++;

my $inputs = get_inputs(@ARGV);
my ($part1, $part2) = solve($inputs);

my $highlight = "\e[1;97;45m";
my $reset     = "\e[0m";
say "part1 $highlight$part1$reset";
say "part2 $highlight$part2$reset";

sub get_inputs ($filename = undef) {
   $filename //= basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
   open my $fh, '<', $filename;
   my @lines = map { s{\s+}{}gmxs; [split m{}mxs] } <$fh>;
   close $fh;
   my %infected;
   my $n = @lines;
   my $d = ($n - 1) / 2;
   for my $i (0 .. $n - 1) {
      for my $j (0 .. $n - 1) {
         next unless $lines[$i][$j] eq '#';
         my $key = join ',', ($i - $d), ($j - $d);
         $infected{$key} = 1;
      }
   }
   return \%infected;
} ## end sub get_inputs ($filename = undef)

sub solve ($inputs) {
   return (part1($inputs), part2($inputs));
}

sub part1 ($inputs, $bursts = 10000) {
   my %infected = $inputs->%*;
   my $count = 0;
   my ($x, $y, $dx, $dy) = (0, 0, 0, -1);
   for (1 .. $bursts) {
      my $key = "$y,$x";
      #say "burst<$_> key<$key> infected<", $infected{$key} ? 1 : 0, '>';
      #say Dumper \%infected;
      if ($infected{$key}) {
         state $turn = {
            '0,-1' => [1, 0],
            '1,0'  => [0, 1],
            '0,1'  => [-1, 0],
            '-1,0' => [0, -1],
         };
         ($dx, $dy) = $turn->{"$dx,$dy"}->@*;
         delete $infected{$key}; # clean node
      }
      else {
         state $turn = {
            '0,-1' => [-1, 0],
            '-1,0' => [0, 1],
            '0,1'  => [1, 0],
            '1,0'  => [0, -1],
         };
         ($dx, $dy) = $turn->{"$dx,$dy"}->@*;
         $infected{$key} = 1;
         ++$count;
      }
      ($x, $y) = ($x + $dx, $y + $dy);
   }
   return $count;
}

sub part2 ($inputs, $bursts = 10_000_000) {
   my %infected = $inputs->%*;
   my (%weakened, %flagged);
   my $count = 0;
   my ($x, $y, $dx, $dy) = (0, 0, 0, -1);
   for (1 .. $bursts) {
      my $key = "$y,$x";
      #say "burst<$_> key<$key> infected<", $infected{$key} ? 1 : 0, '>';
      #say Dumper \%infected;
      if ($infected{$key}) {
         state $turn = {
            '0,-1' => [1, 0],
            '1,0'  => [0, 1],
            '0,1'  => [-1, 0],
            '-1,0' => [0, -1],
         };
         ($dx, $dy) = $turn->{"$dx,$dy"}->@*;
         $flagged{$key} = delete $infected{$key};
      }
      elsif ($flagged{$key}) {
         ($dx, $dy) = (-$dx, -$dy);
         delete $flagged{$key};
      }
      elsif ($weakened{$key}) {
         # no change in direction
         $infected{$key} = delete $weakened{$key};
         ++$count;
      }
      else {
         state $turn = {
            '0,-1' => [-1, 0],
            '-1,0' => [0, 1],
            '0,1'  => [1, 0],
            '1,0'  => [0, -1],
         };
         ($dx, $dy) = $turn->{"$dx,$dy"}->@*;
         $weakened{$key} = 1;
      }
      ($x, $y) = ($x + $dx, $y + $dy);
   }
   return $count;
}
