#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
$|++;

my @jumps;

my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
open my $fh, '<', $filename;
chomp(@jumps = <$fh>);
close $fh;

say 'part1 => ', part1(@jumps);
say 'part2 => ', part2(@jumps);

sub part1 (@jumps) {
   my $pc = 0;
   my $n = 0;
   while (0 <= $pc && $pc <= $#jumps) {
      ++$n;
      $pc += $jumps[$pc]++;
   }
   return $n;
}

sub part2 (@jumps) {
   my $pc = 0;
   my $n = 0;
   while (0 <= $pc && $pc <= $#jumps) {
      ++$n;
      my $offset = $jumps[$pc];
      $jumps[$pc] += $offset >= 3 ? -1 : 1;
      $pc += $offset;
   }
   return $n;
}
