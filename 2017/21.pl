#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
$|++;

my $inputs = get_inputs(@ARGV);
my ($part1, $part2) = solve($inputs);

my $highlight = "\e[1;97;45m";
my $reset     = "\e[0m";
say "part1 $highlight$part1$reset";
say "part2 $highlight$part2$reset";

sub get_inputs ($filename = undef) {
   my %next_for;
   $filename //= basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
   open my $fh, '<', $filename;
   while (<$fh>) {
      s{\s+}{}gmxs;
      my ($input, $output) = split m{=>}mxs;
      $output = [map {[split m{}mxs]} split m{/}mxs, $output];
      $next_for{$_} = $output for equivalent_arrangements($input);
   }
   close $fh;
   return \%next_for;
} ## end sub get_inputs ($filename = undef)

sub solve ($inputs) {
   return (part1($inputs), part2($inputs));
}

sub part1 ($inputs, $iterations = 5) {
   no warnings 'qw';
   my $pattern = [
      [qw< . # . >],
      [qw< . . # >],
      [qw< # # # >],
   ];
   $pattern = part1_iteration($pattern, $inputs) for 1 .. $iterations;
   return scalar grep { $_ eq '#' } map {$_->@*} $pattern->@*;
}
sub part2 ($inputs) { return part1($inputs, 18) }

sub part1_iteration ($pattern, $input) {
   my $n = $pattern->@*;
   my $by = $n % 2 ? 3 : 2;
   my @output;
   for my $I (0 .. $n / $by - 1) {
      for my $J (0 .. $n / $by - 1) {
         my $sp = join '/', map {
            $pattern->[$_]->@[$J * $by .. ($J + 1) * $by - 1]
         } $I * $by .. ($I + 1) * $by - 1;
         my $op = $input->{$sp} // die "wtf\n";
         for my $i (0 .. $by) {
            for my $j (0 .. $by) {
               $output[$I * ($by + 1) + $i][$J * ($by + 1) + $j] = $op->[$i][$j];
            }
         }
      }
   }
   return \@output;
}

sub equivalent_arrangements ($arrangement) {
   state $eps = [];
   my @M = split m{/?}mxs, $arrangement;
   my $n = int sqrt scalar @M;
   $eps->[$n] //= equivalent_permutations($n);
   return map { join '/', @M[$_->@*] } $eps->[$n]->@*;
}

sub equivalent_permutations ($n) {
   my @perms;
   my @M = map { my $b = $_ * $n; [ map { $b + $_ } 0 .. $n - 1] } 0 .. $n - 1;
   for (1 .. 2) {
      for (1 .. 4) {
         push @perms, [map {$_->@*} @M];
         my @nM;
         for my $i (0 .. $n - 1) {
            for my $j (0 .. $n - 1) {
               $nM[$i][$j] = $M[$j][$n - $i - 1]
            }
         }
         @M = @nM;
      }
      my @nM;
      for my $i (0 .. $n - 1) {
         for my $j (0 .. $n - 1) {
            $nM[$i][$j] = $M[$j][$i]
         }
      }
      @M = @nM;
   }
   return \@perms;
}


sub printable_m ($M) {
   my @lines = map { "$_->@*" } $M->@*;
   $lines[0] = "/ $lines[0] \\";
   $_ = "| $_ |" for @lines[1 .. $#lines - 1];
   $lines[-1] = "\\ $lines[-1] /";
   return join "\n", @lines;
}
