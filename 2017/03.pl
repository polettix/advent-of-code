#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
$|++;

my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
open my $fh, '<', $filename;
while (<$fh>) {
   chomp;
   print "$_ -> ";
   my ($x, $y) = position_for($_);
   print abs($x) + abs($y), ' ';
   say fill_upto($_);
}
close $fh;

sub position_for ($n) {
   my $side = 1;
   $side += 2 while $n > $side * $side;
   my ($x, $y) = (int($side / 2), int($side / 2));
   return ($x, $y) if $n == $side * $side;

   $n -= ($side - 2) * ($side - 2);
   --$side;

   $y -= $n > $side ? $side : $n;
   $n -= $side;
   return ($x, $y) if $n <= 0;

   $x -= $n > $side ? $side : $n;
   $n -= $side;
   return ($x, $y) if $n <= 0;

   $y += $n > $side ? $side : $n;
   $n -= $side;
   return ($x, $y) if $n <= 0;

   $x += $n > $side ? $side : $n;
   $n -= $side;
   return ($x, $y) if $n <= 0;

   die "side<$side> residual<$n>\n";
}

sub fill_upto ($n) {
   my %memory = ('0,0' => 1);
   my ($position, $x, $y) = (1, 0, 0);
   while ($memory{"$x,$y"} <= $n) {
      ($x, $y) = position_for(++$position);
      my $sum = 0;
      for my $dx (-1 .. 1) {
         my $X = $x + $dx;
         for my $dy (-1 .. 1) {
            my $Y = $y + $dy;
            $sum += $memory{"$X,$Y"} // 0;
         }
      }
      $memory{"$x,$y"} = $sum;
   }
   return $memory{"$x,$y"};
}
