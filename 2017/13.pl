#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper;
$Data::Dumper::Indent = 1;
use Storable 'dclone';
use List::Util 'reduce';
$|++;

{
   my @inputs;

   my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
   open my $fh, '<', $filename;
   while (<$fh>) {
      push @inputs, [split m{\D+}mxs];
   }
   close $fh;

   my ($part1, $part2) = solve(@inputs);
   say 'part1 => ', $part1;
   say 'part2 => ', $part2;
}

sub solve (@inputs) {
   (part1(@inputs), part2(@inputs));
}

sub part1 (@inputs) {
   my $severity = 0;
   for my $i (0 .. $#inputs) {
      my ($depth, $range) = $inputs[$i]->@*;
      next if $depth % ($range * 2 - 2);
      $severity += $depth * $range;
   }
   return $severity;
}

sub part2 (@inputs) {
   @inputs = map {
      my $full_range = $_->[1] * 2 - 2;
      [($full_range - $_->[0]) % $full_range, $full_range];
   } @inputs;
   my $j = -1;
   my $d = 1;
   OUTER:
   while ('necessary') {
      ++$j;
      for my $i (0 .. $#inputs) {
         my ($depth, $range) = $inputs[$i]->@*;
         next OUTER if $j % $range == $depth;
      }
      last;
   }
   return $j;
}
