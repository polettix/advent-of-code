#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
$|++;

my $inputs = get_inputs(@ARGV);
my ($part1, $part2) = solve($inputs);

my $highlight = "\e[1;97;45m";
my $reset     = "\e[0m";
say "part1 $highlight$part1$reset";
say "part2 $highlight$part2$reset";

sub get_inputs ($filename = undef) {
   my $inputs = {};
   $filename //= basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
   open my $fh, '<', $filename;
   while (<$fh>) {
      push $inputs->{code}->@*, [split m{\s+}mxs];
   }
   close $fh;
   return $inputs;
}

sub solve ($inputs) {
   return (part1($inputs), part2($inputs));
}

sub step1 ($state, $code) {
   my $pc = $state->{pc} // die "invalid pc\n";
   my ($op, @args) = $code->[$pc]->@*;
   $pc++;
   if ($op eq 'set') { $state->{value_for}{$args[0]} = resolve($state, $args[1]) }
   elsif ($op eq 'mul') {
      $state->{mults}++;
      $state->{value_for}{$args[0]} *= resolve($state, $args[1]) }
   elsif ($op eq 'sub') { $state->{value_for}{$args[0]} -= resolve($state, $args[1]) }
   elsif ($op eq 'jnz') {
      $pc += resolve($state, $args[1]) - 1 if resolve($state, $args[0]) != 0 }
   else                 { die "wtf?!? '$op' -> (@args)\n" }
   $state->{pc} = (0 <= $pc && $pc <= $code->$#*) ? $pc : undef;
   return $state;
}

sub part1 ($inputs) {
   my $state = { mults => 0, pc => 0, value_for => { map { $_ => 0 } 'a' .. 'h' } };
   while (defined $state->{pc}) {
      # dump_state($state);
      step1($state, $inputs->{code});
   };
   say 'part 1 finished';
   return $state->{mults};
}

sub part2 ($inputs) {
   my $B = 106700;
   my $C = 123700;
   my $count = 0;
   while ($B <= $C) {
      ++$count unless is_prime($B);
      $B += 17;
   }
   return $count;


   my $state = { mults => 0, pc => 0, value_for => { map { $_ => 0 } 'a' .. 'h' } };
   $state->{value_for}{'a'} = 1;
   while (defined $state->{pc}) {
      dump_state($state);
      step1($state, $inputs->{code});
   };
   return $state->{value_for}{h};
}

sub resolve ($state, $operand) {
   return $operand if $operand =~ m{\d}mxs;
   return $state->{value_for}{$operand};
}

sub dump_state ($state) {
   my $registers = join ', ', map { "$_<$state->{value_for}{$_}>" } 'a' .. 'h';
   say "pc<$state->{pc}> [$registers]";
}

sub is_prime ($n) {
   for my $d (2 .. int(sqrt $n) + 1) { return unless $n % $d }
   return 1;
}
