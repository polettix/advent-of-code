#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
$|++;

my $inputs = get_inputs(@ARGV);
my ($part1, $part2) = solve($inputs);

my $highlight = "\e[1;97;45m";
my $reset     = "\e[0m";
say "part1 $highlight$part1$reset";
say "part2 $highlight$part2$reset";

sub get_inputs ($filename = undef) {
   $filename //= basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
   open my $fh, '<', $filename;

   local $/ = '';
   my %inputs;
   @inputs{qw< begin iterations >} = <$fh> =~ m{state\s+(\w+).*?(\d+)}mxs;

   $inputs{transitions_for} = \my %transitions_for;
   while (<$fh>) {
      my @lines = split m{\R+};
      my ($state) = shift(@lines) =~ m{(\S+):\z}mxs;
      my $value;
      for (@lines) {
         if (my ($v) = m{\A\s*If.*([01]):}mxs) {
            $value = $v;
         }
         elsif (m{Write.*([01])}mxs) {
            $transitions_for{$state}{$value}{write} = $1;
         }
         elsif (m{Move.*(left|right)}mxs) {
            state $mv = {left => -1, right => 1};
            $transitions_for{$state}{$value}{move} = $mv->{$1};
         }
         elsif (m{Continue.*(\S+)\.}mxs) {
            $transitions_for{$state}{$value}{next} = $1;
         }
      }
   }
   close $fh;

   say Dumper \%inputs;
   return \%inputs;
} ## end sub get_inputs ($filename = undef)

sub solve ($inputs) {
   return (part1($inputs), part2($inputs));
}

sub part1 ($inputs) {
   my $pos = 0;
   my $state = $inputs->{begin};
   my %tape;
   my $chksum = 0;
   for (1 .. $inputs->{iterations}) {
      my $current = $tape{$pos} //= 0;
      #say "state<$state> current<$current>";
      my $is = $inputs->{transitions_for}{$state}{$current};
      #say 'executing ', Dumper $is;
      if (exists $is->{write}) {
         my $new = $tape{$pos} = $is->{write};
         ($new ? ++$chksum : --$chksum) if $new != $current;
      }
      $pos += $is->{move} if exists $is->{move};
      $state = $is->{next} if exists $is->{next};
   }
   say Dumper \%tape;
   return $chksum;
}

sub part2 ($inputs) {
   return 'part2'
}
