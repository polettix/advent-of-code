#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
$|++;

my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
open my $fh, '<', $filename;
while (<$fh>) {
   my @banks = split m{\s+}mxs;
   my ($part1, $part2) = part12(@banks);
   say "part1 => $part1";
   say "part2 => $part2";
}
close $fh;

sub part12 (@banks) {
   my %flags;
   my $iterations = 0;
   my $previous_iterations;
   while ('necessary') {
      my $key = join ',', @banks;
      #say $key;
      last if defined($previous_iterations = $flags{$key});
      $flags{$key} = $iterations++;

      # Find slot to spread
      my $i_max = 0;
      for my $i (1 .. $#banks) {
         $i_max = $i - @banks if $banks[$i_max] < $banks[$i];
      }

      (my $blocks, $banks[$i_max]) = ($banks[$i_max], 0);
      if (my $all = int($blocks / @banks)) {
         $_ += $all for @banks;
      }
      if (my $add = $blocks % @banks) {
         $_++ for @banks[$i_max + 1 .. $i_max + $add];
      }
   }
   return ($iterations, $iterations - $previous_iterations);
}
