#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
use List::Util qw< max >;
$|++;

my %p1regs;
my $p2max = 0;

my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
open my $fh, '<', $filename;
while (<$fh>) {
   my ($reg, $op, $amount, undef, $lhs, $cmp, $rhs) = split m{\s+}mxs;
   my $lhs_value = $p1regs{$lhs} ||= 0;
   if (eval "$lhs_value $cmp $rhs") {
      $p1regs{$reg} ||= 0;
      $p1regs{$reg} += $op eq 'inc' ? $amount : -$amount;
      $p2max = $p1regs{$reg} if $p2max < $p1regs{$reg};
   }
}
close $fh;

say 'part1 => ', max(values %p1regs);
say "part2 => $p2max";
