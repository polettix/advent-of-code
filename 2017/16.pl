#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
$|++;

my $inputs = get_inputs(@ARGV);
my ($part1, $part2) = solve($inputs);

my $highlight = "\e[41m"; my $reset = "\e[49m";
say "part1 $highlight$part1$reset";
say "part2 $highlight$part2$reset";

sub get_inputs ($filename = undef) {
   my $inputs = {};
   $filename //= basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
   open my $fh, '<', $filename;
   chomp(my $last = <$fh>);
   $inputs->{programs} = ['a' .. $last];
   chomp(my $moves = <$fh>);
   $inputs->{moves} = [ split m{,}, $moves ];
   close $fh;
   return $inputs;
}

sub solve ($inputs) {
   my $part1 = $inputs->{part1} = part1($inputs);
   return ($part1, part2($inputs));
}

sub part1 ($inputs) {
   my $ps = join '', $inputs->{programs}->@*;
   my $length = length $ps;
   for ($inputs->{moves}->@*) {
      if (m{s(\d+)}mxs) {
         $ps .= substr $ps, 0, $length - $1, '';
      }
      elsif (m{p(\w+)/(\w+)}mxs) {
         my $A = index $ps, $1;
         my $B = index $ps, $2;
         substr $ps, $A, 1, substr $ps, $B, 1, substr $ps, $A, 1;
      }
      elsif (m{x(\d+)/(\d+)}mxs) {
         substr $ps, $1, 1, substr $ps, $2, 1, substr $ps, $1, 1;
      }
      else { die "unknown move $_\n" }
   }
   return $ps;
}
sub part2 ($inputs) {
   my $start = join '', $inputs->{programs}->@*;
   my @cache = ($start);
   my @last = split m{}mxs, $start;
   while ('necessary') {
      my $next = part1({programs => \@last, moves => $inputs->{moves}});
      last if $next eq $start;
      push @cache, $next;
      @last = split m{}mxs, $next;
   }
   my $remainder = 1_000_000_000 % @cache;
   return $cache[$remainder];
}
