#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
$|++;

my %nodes;

my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
open my $fh, '<', $filename;
while (<$fh>) {
   my ($id, $weight, $children) = m{
         \A
         (\S+) \s+ \( (\d+) \)
         (?:\s+ -> \s+ (.*?))?
         \s*\z
      }mxs;
   my $node = $nodes{$id} //= {};
   $node->{weight} = $weight;
   if (my @children = split m{[, ]+}mxs, ($children // '')) {
      $nodes{$_}{parent} = $id for @children;
      $node->{children} = \@children;
   }
}
close $fh;

my ($root) = keys %nodes;
$root = $nodes{$root}{parent} while exists $nodes{$root}{parent};

say "part1 => $root";
say 'part2 => ', part2({nodes => \%nodes, root => $root});


sub part2 ($tree) {
   my ($outcome, $target) = chase($tree);
   die "wtf?!?" if $outcome;
   return $target;
}

sub chase ($tree) {
   my ($nodes, $id) = $tree->@{qw< nodes root >};
   my $root = $nodes->{$id};
   my $weight = $root->{weight};
   my $total  = $weight;
   my @references;
   for my $child ($root->{children}->@*) {
      my ($outcome, $child_weight) = chase({nodes => $nodes, root => $child});
      return ($outcome, $child_weight) if !$outcome; # pass solution up
      if (@references >= 2) {
         my $wrong = $child_weight == $references[0][1] ? 1 : 0;
         my $delta = $references[$wrong][1] - $child_weight;
         my $id = $references[$wrong][0];
         return (0, $nodes->{$id}{weight} - $delta);
      }
      elsif (@references == 0 || $references[0][1] != $child_weight) {
         push @references, [$child, $child_weight];
      }
      $total += $child_weight;
   }
   return (1, $total);
}
