#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
use List::Util 'reduce';
$|++;

my $inputs = get_inputs(@ARGV);
my ($part1, $part2) = solve($inputs);

my $highlight = "\e[41m"; my $reset = "\e[49m";
say "part1 $highlight$part1$reset";
say "part2 $highlight$part2$reset";

sub get_inputs ($filename = undef) {
   my $inputs = {};
   $filename //= basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
   open my $fh, '<', $filename or die "open('$filename'): $OS_ERROR\n";
   chomp(my $code = <$fh>);
   close $fh;
   return $code;
}

sub solve ($inputs) {
   return (part1($inputs), part2($inputs));
}

sub part1 ($inputs) {
   my $count = 0;
   for my $id (0 .. 127) {
      my $hash = knot_hash($inputs . '-' . $id);
      while (length $hash) {
         my $N = unpack 'N', pack 'H*', substr $hash, 0, 8, '';
         while ($N) {
            $count++ if $N & 1;
            $N >>= 1;
         }
      }
   }
   return $count;
}
sub part2 ($inputs) {
   my $uf = UnionFind->new(components => [0 .. (128 * 128) - 1]);
   my @previous = (0) x 129; # all plus 1 for boundary
   my @is_active;
   for my $row (0 .. 127) {
      my $row_offset = $row * 128;
      my $hash = knot_hash($inputs . '-' . $row);
      my @line = split m{}mxs, unpack('B*', pack 'H*', $hash) . '0';
      for my $col (0 .. 127) {
         next unless $line[$col];
         my $P = $row_offset + $col;
         push @is_active, $P;
         $uf->union($P, $P - 1) if $line[$col - 1];
         $uf->union($P, $P - 128) if $previous[$col];
      }
      @previous = @line;
   }
   my %is_root = map { $uf->find($_) => 1 } @is_active;
   return scalar keys %is_root;
}

sub _iterations ($n_items, $lengths, $n) {
   my @buffer = 0 .. $n_items - 1;
   my $current = -$n_items;
   my $skip = 0;
   my $i = 0;
   while ($n-- > 0) {
      for my $length ($lengths->@*) {
         next if $length > $n_items;
         my $last = $current + $length;
         my @range = $current .. $last - 1;
         @buffer[@range] = @buffer[reverse @range];
         $current = $last + $skip++;
         $skip %= $n_items;
         $current -= $n_items while $current >= 0;
      }
   }
   return @buffer;
}

sub knot_hash ($input) {
   my $n_items = 256;
   my @lengths = map { ord($_) } split m{}mxs, $input;
   push @lengths, 17, 31, 73, 47, 23;
   my @sparse = _iterations($n_items, \@lengths, 64);
   my @dense;
   push @dense, reduce { $a ^ $b } splice @sparse, 0, 16 while @sparse > 0;
   die unless @dense == $n_items / 16;
   join '', map {sprintf '%02x', $_} @dense;
}


package UnionFind; # Sedgewick & Wayne, Algorithms 4th ed, §1.5
use strict;

sub add;    # see below
sub connected { return $_[0]->find($_[1]) eq $_[0]->find($_[2]) }
sub count     { return $_[0]{count} }
sub find      { return $_[0]{cs}{$_[0]->find_id($_[1])}[1] }
sub find_id;    # see below
sub new;        # see below
sub union;      # see below

sub add {
   my $id = $_[0]{id_of}->($_[1]);
   return $_[0] if $_[0]{cs}{$id};
   $_[0]{cs}{$id} = [$id, $_[1], 1];
   $_[0]{count}++;
   return $_[0];
}

sub find_id {
   my $r = my $i = $_[0]{id_of}->($_[1]);
   return unless exists $_[0]{cs}{$r};
   $r = $_[0]{cs}{$r}[0] while $r ne $_[0]{cs}{$r}[0];
   ($i, $_[0]{cs}{$i}) = ($_[0]{cs}{$i}[0], $_[0]{cs}{$r}) while $i ne $r;
   return $r;
} ## end sub find_id

sub new {
   my ($pk, %args) = (@_ > 0 && ref($_[1])) ? ($_[0], %{$_[1]}) : @_;
   my $id_of = $args{identifier} || sub { return "$_[0]" };
   my $self = bless {id_of => $id_of, count => 0}, $pk;
   $self->add($_) for @{$args{components} || []};
   return $self;
} ## end sub new

sub union {
   my ($i, $j) = ($_[0]->find_id($_[1]), $_[0]->find_id($_[2]));
   return $_[0] if $i eq $j;
   ($i, $j) = ($j, $i) if $_[0]{cs}{$i}[2] < $_[0]{cs}{$j}[2];   # i -> max
   $_[0]{cs}{$i}[2] += $_[0]{cs}{$i}[2];
   $_[0]{cs}{$j} = $_[0]{cs}{$i};
   $_[0]{count}--;
   return $_[0];
} ## end sub union

1;
