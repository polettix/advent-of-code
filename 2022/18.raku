#!/usr/bin/env raku
use v6;

sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.sample')) {
   return solve($filename) if $filename.IO.e;
}

sub solve ($filename) {
   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";

   my $inputs = get-inputs($filename);
   my ($start, $elapsed);

   $start = now;
   my $part1 = part1($inputs);
   $elapsed = now - $start;
   put "part1 ($elapsed) $highlight$part1$reset";

   $start = now;
   my $part2 = part2($inputs);
   $elapsed = now - $start;
   put "part2 ($elapsed) $highlight$part2$reset";

   return 0;
}

sub get-inputs ($filename) {
   [ $filename.IO.lines.map({ [ .comb(/ \-? \d+ /) «+» 1 ] }) ]
}

sub surface ($inputs) {
   my $overall = SetHash.new;
   my @by-dimension = (0..2).map({ SetHash.new });
   state @faces = [1, 0, 0], [0, 1, 0], [0, 0, 1];
   for @$inputs -> $cube {
      die 'duplicate' if $overall{$cube.join(',')};
      $overall.set($cube.join(','));
      for 0 .. 2 -> $dim {
         for 0 .. 1 -> $offset {
            my $coords = $cube «+» (@faces[$dim] «*» $offset);
            my $key = $coords.join(',');
            my $sh = @by-dimension[$dim];
            if $sh{$key} { $sh.unset($key) }
            else         { $sh.set($key)   }
         }
      }
   }
   return [+] @by-dimension».elems;
}

sub part1 ($inputs) { return surface($inputs) }

sub part2 ($inputs) {
   my $is-full = $inputs».join(',').Set;
   my $transposed = (0..2).map({$inputs»[$_]}).Array;

   # We immerse our droplet in a "box", totally surrounding it with
   # at least one layer in each direction
   my $mins = $transposed».min «-» 1;
   my $maxs = $transposed».max «+» 1;

   # This box has an exterior area that can be easily calculated.
   # If it has dimensions A, B, and C, then the area will be
   # 2 * (A*B + A*C + B*C). Note that A*B = (A*B*C)/C
   my $deltas = ($maxs «-» $mins) «+» 1;
   my $wrap = ([*] @$deltas);
   my $exterior = (($wrap «/» $deltas) «*» 2).sum;

   # Now we put our droplet in the box and fill it as much as we can,
   # creating a "cast" whose internal area is exactly what we are after.
   # It's just a plain visit through the graph of reachable voxels inside
   # the box, starting from one of the corners that we know *for sure* to
   # be on the outside.
   my %external;
   my @queue = [[$mins.Slip],$mins.join(',')],;
   state @offsets = [1, 0, 0], [0, 1, 0], [0, 0, 1];
   while @queue {
      my ($p, $pkey) = @queue.shift.Slip;
      next if %external{$pkey}:exists;
      %external{$pkey} = $p;
      for 0 .. 2 -> $dim {
         for -1, 1 -> $sign {
            my $q = $p «+» (@offsets[$dim] «*» $sign);
            next unless $mins[$dim] <= $q[$dim] <= $maxs[$dim];
            my $qkey = $q.join(',');
            next if $is-full{$qkey};
            @queue.push: [$q, $qkey];
         }
      }
   }

   # Our "surface" function above will calculate the interal area *plus*
   # the external area that we already calculated above as $exterior
   return surface([%external.values]) - $exterior;
}
