#!/usr/bin/env raku
use v6;

sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.sample')) {
   return solve($filename) if $filename.IO.e;
}

sub solve ($filename) {
   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";

   my $inputs = get-inputs($filename);
   my ($start, $elapsed);

   $start = now;
   my $part1 = part1($inputs);
   $elapsed = now - $start;
   put "part1 ($elapsed) $highlight$part1$reset";

   $start = now;
   my $part2 = part2($inputs, $part1);
   $elapsed = now - $start;
   put "part2 ($elapsed) $highlight$part2$reset";

   return 0;
}

sub get-inputs ($filename) {
   $filename.IO.lines.Array
}

sub part1 ($inputs) { return 271;
   class Field1 { ... };
   class Astar { ... };
   my $field = Field1.create($inputs);

   # graph is defined with nodes as pairs [$step, $position]
   # distance between adjacent nodes is 1
   # heuristic is Manhattan
   my $target = $field.target;
   my $nav = Astar.new(
      identifier => -> $v { $v[1] == $target ?? 'TARGET' !! $v.join(',') },
      distance   => -> $v, $w { 1 },
      heuristic  => -> $v, $w { $field.manhattan($v[1], $w[1]) },
      successors => -> $v {
         my $position = $v[1];
         return if $position == $target; # avoid bothering
         my $step = $v[0] + 1;
         $field.adjacents($step, $position).map: { [$step, $_] };
      },
   );
   my @path = $nav.best-path((0, 0), (Nil, $target));
   #.say for @path;
   return @path.end;
}

sub part2 ($inputs, $start) {
   class Field1 { ... };
   class Astar { ... };
   my $field = Field1.create($inputs);

   # graph is defined with nodes as pairs [$step, $position]
   # distance between adjacent nodes is 1
   # heuristic is Manhattan
   my $target = 0;
   my $nav = Astar.new(
      identifier => -> $v { $v[1] == $target ?? 'TARGET' !! $v.join(',') },
      distance   => -> $v, $w { 1 },
      heuristic  => -> $v, $w { $field.manhattan($v[1], $w[1]) },
      successors => -> $v {
         my $position = $v[1];
         return if $position == $target; # avoid bothering
         my $step = $v[0] + 1;
         $field.adjacents($step, $position).map: { [$step, $_] };
      },
   );
   say "start at $start";
   my @return = $nav.best-path(($start, $field.target), (Nil, $target));

   $target = $field.target;
   my @go-again = $nav.best-path((@return[*-1][0], 0), (Nil, $target));

   #.say for @path;
   return @go-again[*-1][0];
}

sub l-perm ($x, $y) { (^$y).map({ (|(1..^$x), 0) «+» $_ * $x }).flat }
sub r-perm ($x, $y) { (^$y).map({ ($x - 1, |^($x - 1)) «+» $_ * $x }).flat }
sub u-perm ($x, $y) { |($x..($x * $y - 1)), |^$x }
sub d-perm ($x, $y) { |(^$x «+» ($x * $y - $x)), |^($x * $y - $x) }


class Field1 {
   has $!size-x is built;
   has $!size-y is built;
   has @!layers is built;
   has @!permutations;
   has @!state-at;
   has $!period;
   has @!adjacents-for;

   submethod create ($lines) {
      my $size-x = $lines[0].chars - 2;
      my $size-y = $lines.elems - 2;
      my @array = $lines[1 .. $size-y].join('').subst('#', '', :g).comb;
      my @layers = '<>^v'.comb.map({ True, |(@array «ne» $_), True });
      return Field1.new(:$size-x, :$size-y, :@layers);
   }
   submethod TWEAK {
      @!permutations  = (&l-perm, &r-perm, &u-perm, &d-perm).map: -> &inner {
         (0, |(&inner($!size-x, $!size-y) «+» 1), 1 + $!size-x * $!size-y)
      }

      my ($A, $B) = $!size-x, $!size-y;
      ($A, $B) = $B % $A, $A while $A;       # GCD
      $!period = $!size-x * $!size-y div $B; # LCM

      @!state-at.push: self!squash;

      my $inner = $!size-x * $!size-y;
      @!adjacents-for.push: [0, 1];
      for 1 .. $inner -> $p {
         my @adjacents = $p; # wait
         @adjacents.push: $p - 1 if ($p - 1) % $!size-x; # left, consider 1-offset
         @adjacents.push: $p + 1 if $p % $!size-x;       # right
         @adjacents.push: $p - $!size-x if $p > $!size-x; # up
         @adjacents.push: $p + $!size-x if $p + $!size-x <= $inner; # down
         @!adjacents-for.push: @adjacents;
      }
      @!adjacents-for[1].push: 0; # for going back...
      @!adjacents-for[$inner].push: $inner + 1;
      @!adjacents-for.push: [ $inner + 1, $inner ]; # for going back...

      #for @!adjacents-for.kv -> $i, $v { say "$i. ($v)" }
      say "period is $!period";
   }

   method !squash { @!layers.reduce({ $^a «&» $^b })».so }

   method stringify ($n = Nil) {
      (
         gather {
            my $global = self.state-at($n // @!state-at.end);
            my $hborder = '-' x ($!size-x - 1);
            my @range = 1 .. $!size-x;
            take "+ {$hborder}+";
            for ^$!size-y {
               take '|' ~ $global[@range].map({ $_ ?? ' ' !! '#' }).join('') ~ '|';
               @range «+=» $!size-x;

            }
            take "+{$hborder} +";
         }
      ).join: "\n";
   }

   method state-at ($n is copy) {
      $n %= $!period;
      while @!state-at.end < $n {
         @!layers[$_] = @!layers[$_][|@!permutations[$_]] for ^4;
         @!state-at.push: self!squash;
      }
      return @!state-at[$n];
   }

   method target { $!size-x * $!size-y + 1 }
   method adjacents ($step, $p) {
      my $state = self.state-at($step);
      @!adjacents-for[$p].grep: { $state[$_] };
   }

   method manhattan ($p is copy, $q is copy) {
      my $value = 0;
      if $p == 0           { ++$value; ++$p }
      if $q == self.target { ++$value; --$q }
      --$p; --$q;
      return $value +
         (($p mod $!size-x) - ($q mod $!size-x)).abs + # delta-x
         (($p div $!size-x) - ($q div $!size-x)).abs;  # delta-y
   }
}

class Astar {
   has (&!distance, &!successors, &!heuristic, &!identifier);

   method best-path ($start!, $goal!) {
      my ($id, $gid) = ($start, $goal).map: {&!identifier($^a)};
      my %node-for = $id => {pos => $start, g => 0};
      class PriorityQueue { ... }
      my $queue = PriorityQueue.new;
      $queue.enqueue($id, 0);
      while ! $queue.is-empty {
         my $cid = $queue.dequeue;
         my $cx = %node-for{$cid};
         next if $cx<visited>++;

         return self!unroll($cx, %node-for) if $cid eq $gid;

         my $cv = $cx<pos>;
         for &!successors($cv) -> $sv {
            my $sid = &!identifier($sv);
            my $sx = %node-for{$sid} ||= {pos => $sv};
            next if $sx<visited>;;
            my $g = $cx<g> + &!distance($cv, $sv);
            next if $sx<g>:exists && $g >= $sx<g>;
            $sx<p> = $cid; # p is the id of "best previous"
            $sx<g> = $g;   # with this cost
            $queue.enqueue($sid, $g + &!heuristic($sv, $goal));
         }
      }
      return ();
   }

   submethod BUILD (:&!distance!, :&!successors!,
      :&!heuristic = &!distance, :&!identifier = {~$^a}) {}

   method !unroll ($node is copy, %node-for) {
      my @path = $node<pos>;
      while $node<p>:exists {
         $node = %node-for{$node<p>};
         @path.unshift: $node<pos>;
      }
      return @path;
   }

   class PriorityQueue {
      has @!items  = ('-');
      method is-empty { @!items.end < 1 }
      method dequeue () { # includes "sink"
         return if @!items.end < 1;
         my $r = @!items.end > 1 ?? @!items.splice(1, 1, @!items.pop)[0] !! @!items.pop;
         my $k = 1;
         while (my $j = $k * 2) <= @!items.end {
            ++$j if $j < @!items.end && @!items[$j + 1]<w> < @!items[$j]<w>;
            last if @!items[$k]<w> < @!items[$j]<w>;
            (@!items[$j, $k], $k) = (|@!items[$k, $j], $j);
         }
         return $r<id>;
      }
      method enqueue ($id, $weight) { # includes "swim"
         @!items.push: {id => $id, w => $weight};
         my $k = @!items.end;
         (@!items[$k/2, $k], $k) = (|@!items[$k, $k/2], ($k/2).Int)
            while $k > 1 && @!items[$k]<w> < @!items[$k/2]<w>;
         return self;
      }
   }
}
