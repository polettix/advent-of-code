#!/usr/bin/env raku
use v6;

class CircularLinkedList { ... }
sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.sample')) {
   my $inputs = get-inputs($filename);

   put do-part($inputs, 1);
   put do-part(@$inputs «*» 811589153, 10);
}

sub do-part ($inputs, $reps) {
   my $cll = CircularLinkedList.create(|$inputs);
   my @list = $cll.linear;
   for ^$reps {
      .say;
      $cll.automove($_) for @list;
   }
   return $cll.result;
}

sub get-inputs ($filename) { [ $filename.IO.lines».Int ] }

class CircularLinkedList {
   my class LinkedListItem {
      has $.value is required;
      has $.main  is readonly = False;
      has $.pred is rw = Nil;
      has $.succ is rw = Nil;
   }

   has $.first is rw = Nil;
   has $.count is rw = 0;

   method create (::?CLASS:U $class: *@values) {
      my $l = $class.new;
      @values.map: {$l.push($^value)};
      return $l;
   }

   method push ($l) { # as last element
      if ! $.count {
         $.first = LinkedListItem.new(value => $l, main => True);
         $.first.pred = $.first.succ = $.first;
         $.count = 1;
         return $.first;
      }

      my $item = LinkedListItem.new(value => $l, main => False,
         succ => $.first, pred => $.first.pred);
      $.first.pred.succ = $item;
      $.first.pred = $item;
      ++$.count;
      return $item;
   }

   method linear {
      my $p = $.first;
      return [ (^$.count).map({ my $r = $p; $p = $p.succ; $r }) ]
   }

   method automove ($item) {
      my $amount = $item.value % ($.count - 1) or return self;
      my $p = $item;
      $p = $p.succ for ^$amount;

      # "detach" $item from current position
      $item.pred.succ = $item.succ;
      $item.succ.pred = $item.pred;

      $p.succ.pred = $item;
      $item.succ = $p.succ;
      $item.pred = $p;
      $p.succ = $item;

      return self;
   }

   method result {
      my $zero = $.first;
      $zero = $zero.succ while $zero.value != 0;
      my $offset = 0;
      my $sum = 0;
      for 1000, 2000, 3000 -> $shift {
         my $mods = $shift mod $.count;
         my $p = $zero;
         $p = $p.succ for ^$mods;
         $sum += $p.value;
      }
      return $sum;
   }

   method printout { self.linear».value.say }
}
