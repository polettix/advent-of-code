#!/usr/bin/env raku
use v6;

sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.sample')) {
   return solve($filename) if $filename.IO.e;
}

sub solve ($filename) {
   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";

   my $inputs = get-inputs($filename);
   my ($start, $elapsed);

   $start = now;
   my $part1 = part1($inputs);
   $elapsed = now - $start;
   put "part1 ($elapsed) $highlight$part1$reset";

   $start = now;
   my $part2 = part2($inputs);
   $elapsed = now - $start;
   put "part2 ($elapsed) $highlight$part2$reset";

   return 0;
}

sub get-inputs ($filename) {
   $filename.IO.slurp
}

sub part1 ($inputs is copy) {
   my @lines = $inputs.lines;
   my $ny = @lines.end;
   my $nx = @lines[0].chars - 1;
   my @sets;
   @sets.push: visible($inputs, sub ($x, $y) { ($x, $y).join(',') });
   @sets.push: visible(flip-vertical($inputs), sub ($x, $y) { ($x, $ny - $y).join(',') });
   $inputs = transpose($inputs);
   @sets.push: visible($inputs, sub ($x, $y) { ($y, $x).join(',') });
   @sets.push: visible(flip-vertical($inputs), sub ($x, $y) { ($ny - $y, $x).join(',') });
   return ([∪] @sets).elems;
}

sub part2 ($inputs) {
   my @grid = $inputs.lines.map({.comb».Int.Array}).Array;
   return (
      gather for 1 ..^ @grid.end -> $y {
         for 1 ..^ @grid[$y].end -> $x {
            take scenic-score(@grid, $x, $y);
         }
      }
   ).max;
   return 'part2'
}

multi sub scenic-score (@grid, $x, $y) {
   my @scores;
   @scores.push: scenic-score-vs(@grid, $x, $y, sub ($x is rw, $y is rw) { ++$x } );
   @scores.push: scenic-score-vs(@grid, $x, $y, sub ($x is rw, $y is rw) { --$x } );
   @scores.push: scenic-score-vs(@grid, $x, $y, sub ($x is rw, $y is rw) { ++$y } );
   @scores.push: scenic-score-vs(@grid, $x, $y, sub ($x is rw, $y is rw) { --$y } );
   return [*] @scores;
}

multi sub scenic-score-vs (@grid, $x, $y, &advance) {
   my $score = 0;
   my ($nx, $ny) = $x, $y;
   loop {
      &advance($nx, $ny);
      #say "x<$x> nx<$nx> y<$y> ny<$ny>";
      last unless 0 <= $ny < @grid;
      last unless 0 <= $nx < @grid[$y].elems;
      ++$score;
      last if @grid[$y][$x] <= @grid[$ny][$nx];
   }

   return $score;
}

sub visible ($text, &id-for) {
   my @lines = $text.lines.map({.comb».Int.Array}).Array;
   my @maxes = -1 xx @lines[0].elems;
   my @retval;
   for ^@lines -> $y {
      for ^@lines[$y].elems -> $x {
         next if @lines[$y][$x] <= @maxes[$x];
         @maxes[$x] = @lines[$y][$x];
         @retval.push: &id-for($x, $y);
      }
   }
   return @retval;
}

multi sub transpose (Str:D $text) {
   my @ilines = $text.lines.Array;
   my $incols = @ilines».chars.max;
   $_ ~= ' ' x ($incols - $_.chars) for @ilines;
   (^$incols)
      .map( { @ilines».substr($_, 1).join('').subst(/\s+/, '') })
      .join("\n");
}
multi sub flip-horizontal (Str:D $text) { $text.lines».flip.join("\n")   }
multi sub flip-vertical   (Str:D $text) { $text.lines.reverse.join("\n") }
multi sub rotate-ccw (Str:D $text)  { transpose(flip-horizontal($text))     }
multi sub rotate-cw (Str:D $text)   { flip-horizontal(transpose($text))     }
multi sub rotate-d180 (Str:D $text) { flip-horizontal(flip-vertical($text)) }
