#!/usr/bin/env raku
use v6;

sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.sample')) {
   return solve($filename) if $filename.IO.e;
}

sub solve ($filename) {
   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";

   my $inputs = get-inputs($filename);
   my ($start, $elapsed);

   $start = now;
   my $part1 = part1($inputs);
   $elapsed = now - $start;
   put "part1 ($elapsed) $highlight$part1$reset";

   $start = now;
   my $part2 = part2($inputs);
   $elapsed = now - $start;
   put "part2 ($elapsed) $highlight$part2$reset";

   return 0;
}

sub get-inputs ($filename) {
   return $filename.IO.comb(/\S/).Array
}

sub sprites { # each is reversed
   return (['****'], < .*. *** .*. >, < ..* ..* *** >, < * * * * >, < ** ** >)
      .map( { $_».comb».Array.Array } ).Array;
}

class ArrayIterator {
   has @!items is built;
   has $!i = 0;
   method get {
      my $j = $!i++;
      $!i %= @!items;
      return @!items[$j];
   }
   method at-start { $!i == 0 }
   method idx { return $!i }
   method dump { say @!items.elems }
}

class Field {
   has @!data;
   has $!top = -1;
   has $!offset = 0;

   method fits ($sprite, $x, $y is copy) {
      @!data.push: [< . . . . . . . >] while @!data <= $y;
      for @$sprite -> $row {
         for 0 .. $row.end -> $dx {
            return False if $row[$dx] eq '*' && @!data[$y][$x + $dx] ne '.';
         }
         --$y;
      }
      return True;
   }

   method overlay ($sprite, $x, $y is copy, $char = '*') {
      $!top = max($!top, $y);
      for @$sprite -> $row {
         @!data[$y][$x + $_] = $row[$_] eq '.' ?? @!data[$y][$x + $_] !! $char
            for 0 .. $row.end;
         --$y;
      }
   }

   method landing-position ($sprite, $dit) {
      my $x = 2;
      my $y = $!top + $sprite.elems + 3;
      loop {
         my $movement = $dit.get;
         my $nx = $movement eq '<' ?? $x - 1 !! $x + 1;
         $x = $nx
            if 0 <= $nx <= 7 - $sprite[0].elems
            && self.fits($sprite, $nx, $y);
         #say "$movement $x $y";

         my $ny = $y - 1;
         $y = $ny if $ny >= 0 && self.fits($sprite, $x, $ny);
         return $x, $y if $y != $ny;
         #say "v $x $y";
      }
   }

   method drop ($sit, $dit, $c = '*') {
      my $sprite = $sit.get;
      my ($x, $y) = self.landing-position($sprite, $dit);
      self.overlay($sprite, $x, $y, $c);
      return self;
   }

   method check-period ($period) {
      my ($n, $height) = $period.Slip;
      return False unless 4 * $height + 10 <= $!top;
      for 0 ..^ $height -> $offset {
         my $closer  = @!data[$!top - 1 * $height - $offset].join('');
         my $farther = @!data[$!top - 2 * $height - $offset].join('');
         return False if $closer ne $farther;
      }
      return True;
   }

   method print {
      for @!data.reverse -> $row {
         put '|', $row.join(''), '|';
      }
      put '+-------+';
   }

   method top { $!top }
}

sub part1 ($inputs) { return part12($inputs, 2022) }
sub part2 ($inputs) { return part12($inputs, 1000000000000) }

sub part12 ($inputs, $max) {
   my $field = Field.new;
   my $dit = ArrayIterator.new(items => @$inputs);
   my $sit = ArrayIterator.new(items => sprites());

   my %last-seen-indexes;
   for 0 ..^ $max -> $i {
      my $ip = $sit.idx ~ '/' ~ $dit.idx;
      if %last-seen-indexes{$ip}:exists { # look for period
         my $last = %last-seen-indexes{$ip};
         my $period = (($i, $field.top) «-» $last).Array;
         if $field.check-period($period) {
            my $delta = $max - $i;
            $field.drop($sit, $dit, '#') for ^ ($delta % $period[0]);
            return 1 + $field.top + ($delta / $period[0]).Int * $period[1];
         }
      }
      %last-seen-indexes{$ip} = ($i, $field.top);
      $field.drop($sit, $dit, '#');
   }

   return 1 + $field.top;
}
