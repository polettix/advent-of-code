#!/usr/bin/env raku
use v6;

sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.sample')) {
   return solve($filename) if $filename.IO.e;
}

sub solve ($filename) {
   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";

   my $inputs = get-inputs($filename);
   my ($start, $elapsed);

   $start = now;
   my $part1 = part1($inputs);
   $elapsed = now - $start;
   put "part1 ($elapsed) $highlight$part1$reset";

   $start = now;
   my $part2 = part2($inputs);
   $elapsed = now - $start;
   put "part2 ($elapsed) $highlight$part2$reset";

   return 0;
}

sub get-inputs ($filename) {
   $filename.IO.lines.Array
}

sub snafu-to-b10 ($snafu) {
   state %value-for = "=" => -2, '-' => -1, '0' => 0, '1' => 1, '2' => 2;
   my $weight = 1;
   return $snafu.comb.reverse.map(
      {
         my $part = $weight * %value-for{$_};
         $weight *= 5;
         $part;
      }
   ).sum;
}

sub b10-to-snafu ($decimal is copy) {
   state %symbol-for = 0 => '0', 1 => '1', 2 => '2', 3 => '=', 4 => '-';
   (
      gather while $decimal > 0 {
         my $rest = $decimal % 5;
         take %symbol-for{$rest};
         $decimal = ($decimal - ($rest > 2 ?? $rest - 5 !! $rest)) div 5;
      }
   ).reverse.join('');
}

sub part1 ($inputs) { b10-to-snafu($inputs.map({snafu-to-b10($_)}).sum); }

sub part2 ($inputs) { return 'there is no part 2...' }
