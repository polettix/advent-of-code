#!/usr/bin/env raku
use v6;

class Astar {...}

sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.sample')) {
   return solve($filename) if $filename.IO.e;
}

sub solve ($filename) {
   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";

   my $inputs = get-inputs($filename);
   my ($start, $elapsed);

   $start = now;
   my $part1 = part1($inputs);
   $elapsed = now - $start;
   put "part1 ($elapsed) $highlight$part1$reset";

   $start = now;
   my $part2 = part2($inputs);
   $elapsed = now - $start;
   put "part2 ($elapsed) $highlight$part2$reset";

   return 0;
}

sub get-inputs ($filename) {
   my @field = $filename.IO.lines.map(
      { .comb.map(
         {
            $_ eq 'S'    ?? -101
            !! $_ eq 'E' ?? 101
            !! .ord - 'a'.ord
         }).Array }
   );
   my (@start, @end);
   for (^@field X ^@field[0]) -> ($y, $x) {
      if @field[$y][$x] < -100 { @start = $x, $y; @field[$y][$x] = 0 }
      if @field[$y][$x] > 100  { @end   = $x, $y; @field[$y][$x] = 'z'.ord - 'a'.ord }
   }
   return {
      start => @start,
      end   => @end,
      field => @field,
   };
}

sub path ($inputs, $from, $to) {
   my \rows = $inputs<field>.elems;
   my \cols = $inputs<field>[0].elems;
   my $nav = Astar.new(
      distance => -> $u, $v { 1 },
      successors => -> $pos {
         my ($px, $py) = @$pos;
         my $max = $inputs<field>[$py][$px] + 1;
         my @valid =
         gather for ([$px-1,$py], [$px+1,$py], [$px,$py-1], [$px,$py+1]) -> ($x, $y) {
            next unless 0 <= $y < rows && 0 <= $x < cols;
            take [$x, $y] if $inputs<field>[$y][$x] <= $max;
         };
         @valid;
      },
      heuristic => {($^v «-» $^w).map(*²).sum.sqrt},
      identifier => {$^v.join(',')},
   );
   return $nav.best-path($from, $to);
}

sub part1 ($inputs) {
   my @path = path($inputs, $inputs<start>, $inputs<end>);
   return @path.elems - 1;
}

sub part2 ($inputs) {
   my \rows = $inputs<field>.elems;
   my \cols = $inputs<field>[0].elems;
   my $best = cols * rows;
   for (0 X ^rows) -> ($x, $y) {
      next if $inputs<field>[$y][$x] > 0;
      my @path = path($inputs, [$x, $y], $inputs<end>);
      my $n = @path.elems;
      $best = $n if $best > $n;
   }
   return $best - 1;
}

class Astar {
   has (&!distance, &!successors, &!heuristic, &!identifier);

   method best-path ($start!, $goal!) {
      my ($id, $gid) = ($start, $goal).map: {&!identifier($^a)};
      my %node-for = $id => {pos => $start, g => 0};
      class PriorityQueue { ... }
      my $queue = PriorityQueue.new;
      $queue.enqueue($id, 0);
      while ! $queue.is-empty {
         my $cid = $queue.dequeue;
         my $cx = %node-for{$cid};
         next if $cx<visited>++;

         return self!unroll($cx, %node-for) if $cid eq $gid;

         my $cv = $cx<pos>;
         for &!successors($cv) -> $sv {
            my $sid = &!identifier($sv);
            my $sx = %node-for{$sid} ||= {pos => $sv};
            next if $sx<visited>;;
            my $g = $cx<g> + &!distance($cv, $sv);
            next if $sx<g>:exists && $g >= $sx<g>;
            $sx<p> = $cid; # p is the id of "best previous"
            $sx<g> = $g;   # with this cost
            $queue.enqueue($sid, $g + &!heuristic($sv, $goal));
         }
      }
      return ();
   }

   submethod BUILD (:&!distance!, :&!successors!,
      :&!heuristic = &!distance, :&!identifier = {~$^a}) {}

   method !unroll ($node is copy, %node-for) {
      my @path = $node<pos>;
      while $node<p>:exists {
         $node = %node-for{$node<p>};
         @path.unshift: $node<pos>;
      }
      return @path;
   }

   class PriorityQueue {
      has @!items  = ('-');
      method is-empty { @!items.end < 1 }
      method dequeue () { # includes "sink"
         return if @!items.end < 1;
         my $r = @!items > 2 ?? @!items.splice(1, 1, @!items.pop)[0] !! @!items.pop;
         my $k = 1;
         while (my $j = $k * 2) <= @!items.end {
            ++$j if $j < @!items.end && @!items[$j + 1]<w> < @!items[$j]<w>;
            last if @!items[$k]<w> < @!items[$j]<w>;
            (@!items[$j, $k], $k) = (|@!items[$k, $j], $j);
         }
         return $r<id>;
      }
      method enqueue ($id, $weight) { # includes "swim"
         @!items.push: {id => $id, w => $weight};
         my $k = @!items.end;
         (@!items[$k/2, $k], $k) = (|@!items[$k, $k/2], ($k/2).Int)
            while $k > 1 && @!items[$k]<w> < @!items[$k/2]<w>;
         return self;
      }
   }
}
