#!/usr/bin/env raku
use v6;

sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.sample')) {
   return solve($filename) if $filename.IO.e;
}

sub solve ($filename) {
   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";

   my $inputs = get-inputs($filename);
   my ($start, $elapsed);

   $start = now;
   my $part1 = part1($inputs);
   $elapsed = now - $start;
   put "part1 ($elapsed) $highlight$part1$reset";

   $start = now;
   my $part2 = part2($inputs);
   $elapsed = now - $start;
   put "part2 ($elapsed) $highlight$part2$reset";

   return 0;
}

sub parse-expression ($expr) {
   grammar G {
      rule TOP              { <parenthesized> }
      rule element          { <intvalue> | <parenthesized> }
      rule intvalue         { 0 | <[ 1 .. 9 ]>\d* }
      rule parenthesized    { '[' [<list-of-elements>]? ']' }
      rule list-of-elements { <element> [ ',' <list-of-elements> ]? }
   }
   class Collect {
      method TOP ($/) { make $<parenthesized>.made }
      method element ($/) {
         make $<intvalue> ?? $<intvalue>.made !! $<parenthesized>.made;
      }
      method intvalue ($/) { make $/.Int }
      method parenthesized ($/) {
         make $<list-of-elements> ?? $<list-of-elements>.made !! $[];
      }
      method list-of-elements ($/) {
         my $retval = [ $<element>.made ];
         $retval.push: $<list-of-elements>.made.Slip if $<list-of-elements>;
         make $retval;
      }
   }
   return G.parse($expr, actions => Collect.new).made;
}

sub get-inputs ($filename) {
   $filename.IO.split(/\n\n+/).map({
      [ .split(/\n/)[0,1].map: &parse-expression ]
   }).Array;
}

multi sub compare (Int $left, Int $right) { return ($right <=> $left).Int }
multi sub compare (    @left, Int $right) { return compare(@left, [$right]) }
multi sub compare (Int $left,     @right) { return compare([$left], @right) }
multi sub compare (@left is copy, @right is copy) {
   while @left && @right {
      my $comparison = compare(@left.shift, @right.shift);
      return $comparison if $comparison != 0;
   }
   return @left ?? -1 !! @right ?? 1 !! 0;
}

sub part1 ($inputs) {
   (gather for $inputs.kv -> $i, ($left, $right) {
      my $is_correct = compare($left, $right) > 0;
      take $i + 1 if $is_correct;
   }).sum
}

sub part2 ($inputs) {
   my $two = [[2],];
   my $six = [[6],];
   my @all = $two, $six, (gather $inputs.map({.take for @$_})).Slip;
   my @sorted = @all.sort({ compare($^a, $^b) }).reverse;
   my %targets = ($two, $six).map({$_.raku}).Set;
   return [*] (^@sorted).grep({@sorted[$_].raku ∈ %targets }).map: * + 1;
}
