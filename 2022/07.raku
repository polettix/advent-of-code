#!/usr/bin/env raku
use v6;

sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.sample')) {
   return solve($filename) if $filename.IO.e;
}

sub solve ($filename) {
   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";

   my %filesystem = get-inputs($filename);
   my ($start, $elapsed);

   $start = now;
   my $sum = 0;
   my $needed = 30000000 - (70000000 - %filesystem</><size>);
   my ($best, $best_size);
   for %filesystem.keys -> $path {
      next unless $path ~~ / \/$ /;
      my $size = %filesystem{$path}<size>;
      $sum += $size if $size <= 100000;
      if $size > $needed {
         if defined($best) {
            ($best, $best_size) = $path, $size
               if $best_size > $size;
         }
         else {
            ($best, $best_size) = $path, $size;
         }
      }
   }
   $elapsed = now - $start;

   put "part1 ($elapsed) $highlight$sum$reset";
   put "part2 ($elapsed) $highlight$best_size$reset";

   return 0;
}

sub get-inputs ($filename) {
   my (@cwd, $cwd, %fs);
   %fs</> = { name => '/' };
   for $filename.IO.lines -> $line {
      if $line ~~ /^ \$ \s+ cd \s+ (.*) / {
         my $dir = $/[0].Str;
         $dir = '' if $dir eq '/';
         if $dir eq '..' { @cwd.pop }
         else            { @cwd.push: $dir }
         $cwd = (@cwd.Slip, '').join('/');
      }
      elsif $line ~~ /^ \$ \s+ ls / {
         %fs{$cwd} = { name => @cwd[*-1], size => 0, children => [] };
      }
      elsif $line ~~ /^ dir \s+ (.*) / {
         %fs{$cwd}<children>.push: $/[0].Str ~ '/';
      }
      else {
         $line ~~ /^ (\d+) \s+ (.*) /;
         %fs{$cwd}<children>.push: $/[1].Str;
         %fs{$cwd ~ $/[1].Str} = {size => $/[0].Int};
      }
   }
   update-sizes(%fs, '/');
   return %fs;
}

sub update-sizes (%fs, $path) {
   return %fs{$path}<size> unless %fs{$path}<children>:exists;
   my $size = 0;
   for %fs{$path}<children>.Slip -> $child {
      $size += update-sizes(%fs, $path ~ $child);
   }
   %fs{$path}<size> = $size;
   return $size;
}
