#!/usr/bin/env raku
use v6;

sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.sample')) {
   return solve($filename) if $filename.IO.e;
}

sub solve ($filename) {
   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";

   my $inputs = get-inputs($filename);
   my ($start, $elapsed);

   $start = now;
   my $part1 = part1($inputs);
   $elapsed = now - $start;
   put "part1 ($elapsed) $highlight$part1$reset";

   $start = now;
   my $part2 = part2($inputs);
   $elapsed = now - $start;
   put "part2 ($elapsed) $highlight$part2$reset";

   return 0;
}

sub get-inputs ($filename) {
   $filename.IO.lines».comb(/\S+/).Array
}

sub part1 ($inputs) {
   return $inputs
      .map({[$_[0].ord - 'A'.ord, $_[1].ord - 'X'.ord]})
      .map({
           1 + $_[1]
         + 3 * ((1 + $_[1] - $_[0]) % 3)
      })
      .sum;
}

sub part2 ($inputs) {
   return $inputs
      .map({[$_[0].ord - 'A'.ord, $_[1].ord - 'X'.ord]})
      .map({
           3 * $_[1]
         + 1 + ($_[0] + $_[1] - 1) % 3;
      })
      .sum;
}
