#!/usr/bin/env raku
use v6;

class Node {
   has $.name;
   has $.value = Nil;
   has $.left  = Nil;
   has $.right = Nil;
   has $.op    = Nil;

   method set-as-unknown { $!value = 'x' }
   multi method init ($value) { $!value = $value }
   multi method init ($left, Str() $op, $right) {
      $!left  = $left;
      $!right = $right;
      $!op    = $op;
   }

   method simplified {
      return $!value if defined $!value;
      my $left  = $!left.simplified;
      my $right = $!right.simplified;
      return "($left $.op $right)" unless $left ~~ Int && $right ~~ Int;
      return $!op eq '+' ?? $left  +  $right
          !! $!op eq '-' ?? $left  -  $right
          !! $!op eq '*' ?? $left  *  $right
          !! $!op eq '/' ?? $left div $right
          !! die("unknown op $.op");
   }

   # return expression as first item, then value. No validation.
   method !expr-value {
      my $rv = $!right.simplified;
      return ($!left, $rv) if $rv ~~ Int;
      return ($!right, $!left.simplified);
   }

   method solve-as-equal {
      my ($expr, $value) = self!expr-value;

      while $expr.simplified ne 'x' {
         my $op = $expr.op;
         if $op ~~ m{ <[ + * ]> } { # commutative
            my ($se, $sv) = $expr!expr-value;
            $expr = $se;
            $value = $op eq '+' ?? $value - $sv !! $value div $sv;
         }
         elsif $op ~~ m{ <[ / - ]> } {  # non-commutative
            my ($l, $r) = $expr.left, $expr.right;
            if $l.simplified ~~ Int {  # K <op> f(x) = V --> f(x) = K <op> V
               $expr = $r;
               $value = $op eq '-' ?? $l.simplified - $value
                  !! $l.simplified div $value;
            }
            else { # f(x) <op> K = V --> f(x) = V <inv-op> K
               $expr = $l;
               $value = $op eq '-' ?? $value + $r.simplified
                  !! $value * $r.simplified;
            }
         }
         else { die "unknown op $op" }
      }

      return $value;
   }
}

sub MAIN ($filename = 'aoc2022-21.sample') {
   sub nv (Str() $name) { # keep track of all nodes
      state %node-for;
      return %node-for{$name} //= Node.new(name => $name);
   }

   for $filename.IO.lines -> $expression {
      my $match = $expression ~~ m{^
         $<target>=(\w+) \:\s+ $<op1>=(\w+)
            [ \s+ $<op>=(\S) \s+ $<op2>=(\w+) ]?
      $};

      my $nv = nv($match<target>);
      if $match<op> {
         $nv.init(nv($match<op1>), $match<op>, nv($match<op2>));
      }
      else {
         $nv.init($match<op1>.Int);
      }
   }

   my $root = nv('root');
   my $me = nv('humn');

   # part 1
   put $root.simplified;

   # part 2
   $me.set-as-unknown;
   put $root.solve-as-equal;
}
