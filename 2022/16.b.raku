#!/usr/bin/env raku
use v6;

sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.sample')) {
   return solve($filename) if $filename.IO.e;
}

sub solve ($filename) {
   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";

   my $inputs = get-inputs($filename);
   my ($start, $elapsed);

   $start = now;
   my $part1 = part1($inputs);
   $elapsed = now - $start;
   put "part1 ($elapsed) $highlight$part1$reset";

   $start = now;
   my $part2 = part2($inputs);
   $elapsed = now - $start;
   put "part2 ($elapsed) $highlight$part2$reset";

   return 0;
}

sub get-inputs ($filename) {
   my $from-file = $filename.IO.lines.map(
      {
         my $match = m{^Valve \s+ (\S+) .*? (\d+) .*? valves? \s+ (.*)};
         $match[0].Str => {
            name     => $match[0].Str,
            rate     => $match[1].Int,
            adjacent => $match[2].comb(/\w+/).Array,
         };
      }
   ).Hash;
   my $graph = generate-graph($from-file);

   return $graph;
}

sub generate-graph ($inputs, $entry-point = 'AA') {
   class FloydWarshall { ... }
   my $fw = FloydWarshall.new(
      identifier => -> $v { $v },
      distance   => -> $v, $w { 1 },
      successors => -> $v { $inputs{$v}<adjacent>.Slip.Array },
      starts     => ['AA'],
   );

   # all non-zero valves, plus the entry point
   my @keys = $inputs.keys.grep({ $inputs{$_}<rate> }).sort;
   @keys.unshift: 'AA';

   # this variable will hold the minimum cost to reach a specific node of those
   # kept (i.e. $entry-point or non-zero rate). This cost is the distance to
   # reach the node from the closest one, plus 1 to account for opening the
   # valve.
   my @min-cost-to;

   # compute edges for the restricted "overlay" graph. The "distance" is saved
   # as the traveling distance, plus 1 to account for opening the valve; as
   # such, it assumes that there's no going back (explicitly), but this is not
   # accounted anyway in any visit algorithm.
   my @edges = gather for @keys.kv -> $vid, $v { take [ (1..^@keys)

         # skip self-edges
         .grep({ $vid != $_ })

         # collect integer identifier and cost (= distance + 1)
         .map(
            -> $wid {
               my $v-to-w = $fw.distance($v, @keys[$wid]) + 1;
               @min-cost-to[$wid] = $v-to-w
                  if $v-to-w < (@min-cost-to[$wid] // ($v-to-w + 1));
               [ $wid, $v-to-w, $inputs{@keys[$wid]}<rate> ];
            }
         )

         # reverse sort, highest flow rates first
         .sort({ $^b[*-1] <=> $^a[*-1] })
      ];
   }

   # collect the rates for each valve, only consider non-zero except
   # $entry-point
   my @rates = @keys.map({$inputs{$_}<rate>});


   # for "meaningful" times, pre-compute the best lineup of valves that still
   # make sense to open, reverse sorted by the advantage that they can provide.
   # This will provide an optimistic estimation (i.e. upper limit) on what can
   # still be extracted at a point, assuming the valve can be visited in the
   # minimum distance it has from any other possible valve.
   # The calculation starts at 0 so that the array can be directly indexed by
   # time value.
   my @best-valves = (0 .. 30).map: -> $t {
      (^@rates)
         .map(
            -> $i {
               my $min-cost = @min-cost-to[$i];
               my $rate = @rates[$i];
               my $gain = $rate ?? ($t - $min-cost) * $rate !! 0;
               [$i, $min-cost, $rate, $gain];
            }
         )
         .grep({ $_[*-1] > 0 }) # keep only gains > 0
         .sort({ $^b[*-1] <=> $^a[*-1] })
         .Array;
   };

   return {
      nodes => @keys.kv.map(-> $id, $name
         { $name => hash(:$name, :$id, rate => $inputs{$name}<rate>) }
      ).Hash,
      :@keys,
      :@min-cost-to,
      :@edges,
      :@rates,
      :@best-valves,
   };
}

sub part1 ($inputs) { return max-pressure-release($inputs, 0, [30,  0]) }
sub part2 ($inputs) { return max-pressure-release($inputs, 0, [26, 26]) }

# algorithm taken from Rust implementation at:
# https://github.com/orlp/aoc2022/blob/master/src/bin/day16.rs
sub max-pressure-release ($inputs, $start-id, $time) {
   class BasicPriorityQueue { ... } # see below

   # This class represents a "State" that is analyzed during the search for
   # the optimal solution. It basically represents a state in A*.
   class State {
      has $.opened      is built;
      has @.pos         is built;
      has @.time        is built;
      has $.score       is built = 0;
      has $.upper-bound is built = Inf;

      # this allows detecting two identical states and avoid repetitions
      method id { ($!opened, @!pos, @!time).flat.join(',') }

      # this method calculates the upper bound for the score in this state,
      # by evaluating the maximum possible additional score that can be
      # obtained starting from the state itself. This is what provides us
      # with an acceptable heuristic for pruning states expansion.
      method set-upper-bound ($best-valves)  {
         my ($max-t, $min-t) = @!time.Slip;
         my $opened = $!opened;
         $!upper-bound = $!score;
         VALVE: loop {
            for $best-valves[$max-t].Slip -> ($i, $min-cost, $rate, $gain) {
               my $weight = 0x01 +< $i;
               next if $opened +& $weight;
               $opened +|= $weight;
               $max-t -= $min-cost;
               $!upper-bound += $rate * $max-t;
               ($max-t, $min-t) = $min-t, $max-t if $max-t < $min-t;
               next VALVE;
            }
            last VALVE;
         }
      }
   }

   my $best-score = 0;      # keep track of the best score as we move on

   # this is a priority queue where higher values for upper-bound are
   # dequeued first.
   my $states = BasicPriorityQueue.new(
      before => { $^b.upper-bound < $^a.upper-bound },
      items  => [
         State.new(
            opened => (0x01 +< $start-id),
            pos    => [$start-id, $start-id],
            time   => [ @$time ],
            score  => 0,
            upper-bound => $best-score + 1, # just to kickstart the process
         )
      ],
   );

   my $seen = SetHash.new;  # don't expand states twice or more

   while $states.elems > 0 {
      my $state = $states.dequeue;

      # if the upper-bound of the state we extracted is worse than the best
      # score so far, we can call it a day because neither this $state, nor
      # any other residual state in the priority queue can possibly make
      # things better.
      last if $state.upper-bound < $best-score;

      # avoid expanding this state again
      next if $state.id ∈ $seen;
      $seen.set($state.id);

      # OK, it makes sense to expand this state, so we will check all
      # neighbors from here. The "active" player is always the "first" one
      # in $state.time and $state.pos arrays, so we use $state.time[0] and
      # $state.pos[0] *always*. When we need to pass control to the other
      # player, we swap the two so that position 0 is always the one in
      # charge.
      for $inputs<edges>[$state.pos[0]].Slip -> ($id, $cost, $rate) {
         next if $state.time[0] <= $cost; # skip if we can't pay the cost

         # the "set" of opened valves is kept as a bit vector to make things
         # fast. This is the weight of the neighbor id.
         my $weight = 0x01 +< $id;
         next if $state.opened +& $weight; # skip if valve already open

         # there is the serious chance that we are up to something at this
         # point. Simulate moving onto the valve by paying the "toll" in
         # terms of cost to calculate the new state's score.
         my $time = $state.time[0] - $cost;
         my $score = $state.score + $rate * $time; # FIXME check $inputs<rates>[$id]
         $best-score = $score if $best-score < $score;

         # generate a new (candidate) state. Players are possibly swapped to
         # make sure that $new-state.time[0] is not less than
         # $new-state.time[1].
         my $swap   = $time < $state.time[1];
         my $p1id   = $state.pos[1];
         my $p1time = $state.time[1];
         my $new-state = State.new(
            opened => ($state.opened +| $weight),
            pos    => ($swap ?? [$p1id, $id]     !! [$id, $p1id]),
            time   => ($swap ?? [$p1time, $time] !! [$time, $p1time]),
            score  => $score,
         );

         # set the upper-bound to make the heuristic work. This heuristic still
         # guarantees that the result is optimal because it does *not*
         # underestimate the final score (A* with maximization instead of
         # minimization).
         $new-state.set-upper-bound($inputs<best-valves>);

         # only enqueue the new state if it has any hope of improving over the
         # $best-score so far, otherwise don't bother and spare memory.
         $states.enqueue($new-state) if $new-state.upper-bound > $best-score;
      }
   }

   return $best-score;
}

# **REDUCED** version from:
# https://github.com/polettix/cglib-raku/blob/main/BasicPriorityQueue.rakumod
class BasicPriorityQueue {
   has @!items;
   has &!before;

   submethod BUILD (:&!before = {$^a < $^b}, :@items) {
      @!items = '-';
      self.enqueue($_) for @items;
   }

   method elems { @!items.end }

   method dequeue () { # includes "sink"
      return unless @!items.end;
      my $r = @!items.pop;
      ($r, @!items[1]) = (@!items[1], $r) if @!items.end >= 1;
      my $k = 1;
      while (my $j = $k * 2) <= @!items.end {
         ++$j if $j < @!items.end && &!before(@!items[$j + 1], @!items[$j]);
         last if &!before(@!items[$k], @!items[$j]);
         (@!items[$j, $k], $k) = (|@!items[$k, $j], $j);
      }
      return $r;
   }

   method enqueue ($obj) { # includes "swim"
      @!items.push: $obj;
      my $k = @!items.end;
      (@!items[$k/2, $k], $k) = (|@!items[$k, $k/2], ($k/2).Int)
         while $k > 1 && &!before(@!items[$k], @!items[$k/2]);
      return self;
   }
}

# **REDUCED** version from:
# https://github.com/polettix/cglib-raku/blob/main/FloydWarshall.rakumod
class FloydWarshall {
   has &!identifier;
   has %!d;  # output, distances
   has %!p;  # output, predecessors
   has %!nf; # output, node data for identifier

   method BUILD (:&distance!, :&successors!, :&!identifier = { ~$^a }, :@starts where *.elems > 0) {
      my @q = @starts;
      while (@q) {
         next if %!nf{my $vi = &!identifier(my $v = @q.shift)}:exists;
         for &successors(%!nf{$vi} = $v) -> $w {
            next if $vi eq (my $wi = &!identifier($w)); # avoid self-edges
            %!d{$vi}{$wi} = &distance($v, $w);
            %!p{$vi}{$wi} = $vi;
            @q.push($w) unless %!nf{$wi}:exists;
         }
      }
      my @vs = %!nf.keys;
      for @vs -> $vi {
         for @vs -> $vv {
            next unless %!p{$vv}{$vi}:exists;
            for @vs -> $vw {
               next unless %!d{$vi}{$vw}:exists;
               my $newd = %!d{$vv}{$vi} + %!d{$vi}{$vw};
               next if %!d{$vv}{$vw}:exists && %!d{$vv}{$vw} <= $newd;
               %!d{$vv}{$vw} = $newd;
               %!p{$vv}{$vw} = %!p{$vi}{$vw};
            }
            die 'negative cycle, bail out' if %!d{$vv}{$vv} < 0;
         }
      }
   }

   method distance ($v, $w) {
      my ($vi, $wi) = ($v, $w).map({&!identifier($_)}).Slip;
      return unless (%!d{$vi}:exists) && (%!d{$vi}{$wi}:exists);
      return %!d{$vi}{$wi};
   }
}
