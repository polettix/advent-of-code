#!/usr/bin/env raku
use v6;

sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.sample')) {
   return solve($filename) if $filename.IO.e;
}

sub solve ($filename) {
   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";

   my $inputs = get-inputs($filename);
   my ($start, $elapsed);

   $start = now;
   my $part1 = part1($inputs);
   $elapsed = now - $start;
   put "part1 ($elapsed) $highlight$part1$reset";

   $start = now;
   my $part2 = part2($inputs);
   $elapsed = now - $start;
   put "part2 ($elapsed) $highlight$part2$reset";

   return 0;
}

sub get-inputs ($file) { [$file.IO.lines.map({[.comb(/\S+/)]})] }

sub head-to-tail (@head-positions) {
   my @tail = @head-positions[0].Slip;
   my @tail-positions;
   for @head-positions -> $head {
      my @diff = @$head «-» @tail;
      if ([*] @diff).abs > 1 { # diagonal movement needed
         @tail «+=» (@diff «/» @diff».abs);
      }
      elsif @diff[0].abs > 1 {
         @tail[0] += (@diff[0] / 2).Int;
      }
      elsif @diff[1].abs > 1 {
         @tail[1] += (@diff[1] / 2).Int;
      }
      else { next if @tail-positions } # no need to move in this case
      @tail-positions.push: [@tail.Slip];
   }
   return @tail-positions;
}

sub moves-to-positions (@moves) {
   state %delta = R => (1, 0), L => (-1, 0), U => (0, 1), D => (0, -1);
   my @head = 0, 0;
   return [
      [0, 0],
      @moves.map(-> ($direction, $amount) {
         (^$amount).map({[@head «+=» %delta{$direction}]}).Slip
      }).Slip
   ];
}

sub count-tail ($inputs, $knots) {
   my @positions = moves-to-positions($inputs);
   @positions = head-to-tail(@positions) for 1..^$knots;
   return @positions».join(' ').Set.elems;
}

sub part1 ($inputs) { count-tail($inputs, 2) }
sub part2 ($inputs) { count-tail($inputs, 10) }
