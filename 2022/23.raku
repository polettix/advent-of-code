#!/usr/bin/env raku
use v6;

sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.sample')) {
   return solve($filename) if $filename.IO.e;
}

sub solve ($filename) {
   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";

   my $inputs = get-inputs($filename);
   my ($start, $elapsed);

   $start = now;
   my $part1 = part1($inputs);
   $elapsed = now - $start;
   put "part1 ($elapsed) $highlight$part1$reset";

   $start = now;
   my $part2 = part2($inputs);
   $elapsed = now - $start;
   put "part2 ($elapsed) $highlight$part2$reset";

   return 0;
}

sub get-inputs ($filename) {
   $filename.IO.lines.Array
}

class Planter {
   has $.P is built;

   method pos { $!P.join(',') }

   method make-proposal ($team, $direction is copy) {
      state @deltas = [-1, -1], [-1, 0], [-1, 1],
                      [ 0, -1],          [ 0, 1],
                      [ 1, -1], [ 1, 0], [ 1, 1];
      state @tests-for =
         [2, 4, 7], # north
         [0, 3, 5], # south
         [0, 1, 2], # west
         [5, 6, 7]; # east
      state @move-for = [0, 1], [0, -1], [-1, 0], [1, 0];

      my @is-empty = @deltas.map: { $team{($!P «+» $_).join(',')}:!exists };
      return if @is-empty.all;

      for ^4 {
         return ($!P «+» @move-for[$direction])
            if @is-empty[|@tests-for[$direction]].all;
         $direction = ($direction + 1) % 4;
      }

      return;
   }

   method move-to ($Q) { $!P = $Q }
}

sub part1 ($inputs) {
   my %team;
   for ^@$inputs -> $dy {
      my $y = $inputs.end - $dy;
      for ^$inputs[$dy].chars -> $x {
         next unless $inputs[$dy].substr($x, 1) eq '#';
         my $elf = Planter.new(P => [$x, $y]);
         %team{$elf.pos} = $elf;
      }
   }

   my $direction = -1;
   for ^10 {
      $direction = ($direction + 1) % 4;

      # collect proposals, keep moving one separated
      my (%moving, %freezing);
      for %team.values -> $elf {
         my $proposal = $elf.make-proposal(%team, $direction) or next;
         my $key = $proposal.join(',');
         next if %freezing{$key};
         if %moving{$key} {
            %moving{$key}:delete;
            %freezing{$key} = 1;
         }
         else {
            %moving{$key} = [$elf, $proposal];
         }
      }

      # move
      for %moving.values -> ($elf, $target) {
         %team{$elf.pos}:delete;
         $elf.move-to($target);
         %team{$elf.pos} = $elf;
      }
   }

   my @Ps = %team.values».P;
   my $area = [*] (0, 1).map({ my @Vs = @Ps»[$_]; 1 + @Vs.max - @Vs.min });

   return $area - %team.elems;
}

sub part2 ($inputs) {
   my %team;
   for ^@$inputs -> $dy {
      my $y = $inputs.end - $dy;
      for ^$inputs[$dy].chars -> $x {
         next unless $inputs[$dy].substr($x, 1) eq '#';
         my $elf = Planter.new(P => [$x, $y]);
         %team{$elf.pos} = $elf;
      }
   }

   my $direction = -1;
   for 1 .. * -> $i {
      $direction = ($direction + 1) % 4;

      # collect proposals, keep moving one separated
      my (%moving, %freezing);
      for %team.values -> $elf {
         my $proposal = $elf.make-proposal(%team, $direction) or next;
         my $key = $proposal.join(',');
         next if %freezing{$key};
         if %moving{$key} {
            %moving{$key}:delete;
            %freezing{$key} = 1;
         }
         else {
            %moving{$key} = [$elf, $proposal];
         }
      }

      return $i unless %moving.elems;

      say "moving $i";

      # move
      for %moving.values -> ($elf, $target) {
         %team{$elf.pos}:delete;
         $elf.move-to($target);
         %team{$elf.pos} = $elf;
      }
   }
}
