sub detect-different($string, $n) {
   my ($i, $window) = $n - 1, BagHash.new($string.substr(0, $n - 1).comb);
   loop {
      $window.add($string.substr($i++, 1));
      return $i if $window.elems == $n;
      $window.remove($string.substr($i - $n, 1));
   };
}
put '06.input'.IO.lines.map({detect-different($_, 4)});
put '06.input'.IO.lines.map({detect-different($_, 14)});
