#!/usr/bin/env raku
use v6;

sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.sample')) {
   return solve($filename) if $filename.IO.e;
}

sub solve ($filename) {
   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";

   my $inputs = get-inputs($filename);
   my ($start, $elapsed);

   $start = now;
   my $part1 = part1($inputs);
   $elapsed = now - $start;
   put "part1 ($elapsed) $highlight$part1$reset";

   $start = now;
   my $part2 = part2($inputs);
   $elapsed = now - $start;
   put "part2 ($elapsed) $highlight$part2$reset";

   return 0;
}

sub get-inputs ($filename) {
   $filename.IO.lines.Array;
}

sub part1 ($inputs) {
   return $inputs.map({
      my $hlen = ($_.chars / 2).Int;
      my ($h, $l) =
         ($_.substr(0, $hlen), $_.substr($hlen)).map({ $_.comb.Set });
      char-to-value(($h ∩ $l).keys);
   }).sum;
}

sub part2 ($inputs) {
   gather {
      for @$inputs -> $a, $b, $c {
         take char-to-value(([∩] ($a, $b, $c)».comb».Set).keys)
      }
   }.sum;
}

sub char-to-value ($char) {
   return 1 + $char.ord - 'a'.ord if $char ~~ /<[ a .. z ]>/;
   return 27 + $char.ord - 'A'.ord;
}
