#!/usr/bin/env raku
use v6;

sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.sample')) {
   return solve($filename) if $filename.IO.e;
}

sub solve ($filename) {
   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";

   my $inputs = get-inputs($filename);
   say $inputs;
   my ($start, $elapsed);

   $start = now;
   my $part1 = part1($inputs);
   $elapsed = now - $start;
   put "part1 ($elapsed) $highlight$part1$reset";

   $start = now;
   my $part2 = part2($inputs);
   $elapsed = now - $start;
   put "part2 ($elapsed) $highlight$part2$reset";

   return 0;
}

sub get-inputs ($filename) { [ $filename.IO.lines».Int ] }

sub part1 ($inputs) {#return 'part 1 FIXME'; # FIXME
   my @original = |$inputs;
   my @fs = 0 ..^ @original;  # forward
   my @rs = 0 ..^ @original;  # reverse
   for @original.kv -> $n is copy, $offset {
      say "{$n+1}/{0+@original}";
      my $current-position = @fs[$n];
      #say "moving item originally at $n, now at $current-position, offset<$offset>, dc-offset<@original[@rs[$current-position]]> r<{@rs[$current-position]}>";
      #say "from f({@original[@fs]}) r({@original[@rs]})";

      my $target-position = $current-position + $offset;
      #say "       target<$target-position>";
      if $target-position >= @original.end { $target-position %= @original.end }
      elsif $target-position < 0           { $target-position %= @original.end }
      #say "   new target<$target-position>";

      my $delta = $target-position < $current-position ?? -1 !! +1;
      while $current-position != $target-position {
         my $next-position = $current-position + $delta;
         my $n-next = @rs[$next-position];
         #say "  cur<$current-position> nex<$next-position> n<$n> nn<$n-next>";
         @fs[$n, $n-next] = @fs[$n-next, $n];
         @rs[$current-position, $next-position]
            = @rs[$next-position, $current-position];
         $current-position = $next-position + 0;
         #say "  >> f({@original[@fs]}) r({@original[@rs]}) fr({@original[@fs[@rs]]})";
      }

      #say "step f({@original[@fs]}) r({@original[@rs]}) fr({@original[@fs[@rs]]})";
      #say '';
   }
   my @modified = @original[@rs];
   my ($i) = (^@modified).grep({! @modified[$_]});
   say "final ({@modified})";
   say "0 is at $i";
   return [+] (1000, 2000, 3000).map({ @modified[($i + $_) % @modified] });
}

sub part2 ($inputs) {
   my @original = @$inputs «*» 811589153;
   my @fs = 0 ..^ @original;  # forward
   my @rs = 0 ..^ @original;  # reverse
   for 1 .. 10 {
      for @original.kv -> $n is copy, $offset {
         say "{$n+1}/{0+@original}";
         my $current-position = @fs[$n];
         #say "moving item originally at $n, now at $current-position, offset<$offset>, dc-offset<@original[@rs[$current-position]]> r<{@rs[$current-position]}>";
         #say "from f({@original[@fs]}) r({@original[@rs]})";

         my $target-position = ($current-position + $offset) % @original.end;
         #say "       target<$target-position>";
         #if $target-position >= @original.end { $target-position %= @original.end }
         #elsif $target-position < 0           { $target-position %= @original.end }
         #say "   new target<$target-position>";

         my $delta = $target-position < $current-position ?? -1 !! +1;
         while $current-position != $target-position {
            my $next-position = $current-position + $delta;
            my $n-next = @rs[$next-position];
            #say "  cur<$current-position> nex<$next-position> n<$n> nn<$n-next>";
            @fs[$n, $n-next] = @fs[$n-next, $n];
            @rs[$current-position, $next-position]
               = @rs[$next-position, $current-position];
            $current-position = $next-position + 0;
            #say "  >> f({@original[@fs]}) r({@original[@rs]}) fr({@original[@fs[@rs]]})";
         }

         #say "step f({@original[@fs]}) r({@original[@rs]}) fr({@original[@fs[@rs]]})";
         #say '';
      }
   }
   my @modified = @original[@rs];
   my ($i) = (^@modified).grep({! @modified[$_]});
   say "final ({@modified})";
   say "0 is at $i";
   return [+] (1000, 2000, 3000).map({ @modified[($i + $_) % @modified] });
   return 'part2'
}
