#!/usr/bin/env raku
use v6;

sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.sample')) {
   return solve($filename) if $filename.IO.e;
}

sub solve ($filename) {
   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";

   my $inputs = get-inputs($filename);
   my ($start, $elapsed);

   $start = now;
   my $part1 = part1($inputs);
   $elapsed = now - $start;
   put "part1 ($elapsed) $highlight$part1$reset";

   $start = now;
   my $part2 = part2($inputs);
   $elapsed = now - $start;
   put "part2 ($elapsed) $highlight$part2$reset";

   return 0;
}

sub get-inputs ($filename) {
   my ($lines, $code) = $filename.IO.slurp.split(/\n\n+/);
   my @moves = $code.trim.split(/ \d+ /, :v, :skip-empty)
      .map: { $_ ~~ Match ?? $_.Int !! $_ };
   return {:$lines, :@moves};
}

class Field {
   has @.lines;
   has $.x = 0;
   has $.y = 0;
   has $.d = 0;
   has $.n-cols;

   submethod TWEAK (:$lines) {
      @!lines = $lines.split(/\n+/, :skip-empty);
      $!n-cols = @!lines».chars.max;
      $_ ~= (' ' x ($!n-cols - .chars)) for @!lines;
      $!x = @!lines[0].index('.');
   }
   method clone { nextwith :lines(@!lines.clone), |%_ }

   method dump {
      say "($!x, $!y), facing {self.direction}, width $!n-cols, map:";
      say '   +', ('-' x $!n-cols), '+';
      @.lines.map: { "   |$_|".say };
      say '   +', ('-' x $!n-cols), '+';
   }

   method direction { '>v<^'.substr($!d, 1) }

   method place ($c = Nil) { @!lines[$!y].substr-rw($!x, 1) = $c // self.direction }

   multi method move (Str $rotations, $place = True) {
      $!d = ($!d + ($_ eq 'R' ?? 1 !! 3)) % 4 for $rotations.comb;
      self.place if $place;
   }
   multi method move (Int $steps, $place = True) {
      state @deltas = [1, 0], [0, 1], [-1, 0], [0, -1];

      self.place if $place;

      my ($dx, $dy) = @deltas[$!d].Slip;
      my ($x, $y) = $!x, $!y;
      for ^$steps {
         my $c;
         loop {
            $x = ($x + $dx) % $!n-cols;
            $y = ($y + $dy) % @!lines;
            last if ($c = @!lines[$y].substr($x, 1)) ne ' ';
         }

         last if $c eq '#'; # The Wall

         # save position, rinse, repeat
         ($!x, $!y) = $x, $y;
         self.place if $place;
      }
   }

   method apply (@moves) {
      self.move($_, True) for @moves;
      return self;
   }
}

sub part1 ($inputs) {
   my $field = Field.new(lines => $inputs<lines>);
   $field.apply($inputs<moves>);
   return 1004 + $field.y * 1000 + $field.x * 4 + $field.d;
}


class DieSide {
   has $.face;
   has $.d;
   has $.cw  is rw = Nil;
   has $.ccw is rw = Nil;
   has $.adj is rw = Nil;

   method attach ($other-side) {
      self.adj = $other-side;
      $other-side.adj = self;
      return self;
   }

   method id { "{$!face.name}/$!d" }
   method adj-id { defined($!adj) ?? $!adj.id !! '---' }
   method dump {
      my ($cw, $ccw, $adj) = ($!cw, $!ccw, $!adj).map: { $_ ?? $_.id !! '' };
      say "{self.id} cw<$cw> ccw<$ccw> adj<$adj>" }
}

class DieFace {
   has $.name;
   has $.x;
   has $.y;
   has @.sides is rw; # a face has four sides
   submethod TWEAK (:$!name, :$!x, :$!y) {
      for ^4 -> $d {
         my $side = DieSide.new(face => self, :$d);
         if @!sides {
            $side.ccw = @!sides[*-1];
            @!sides[*-1].cw = $side;
         }
         @!sides.push: $side;
      }
      @!sides[0].ccw = @!sides[*-1];
      @!sides[*-1].cw = @!sides[0];
   }
   method attach ($other-face, $d) {
      @!sides[$d].attach($other-face.sides[(2 + $d) % 4]);
      return self;
   }
   method stringify {
      my ($e, $s, $w, $n) = @!sides».adj-id;
      return join "\n",
         "($!x, $!y)",
         '+-----------+',
         "|    $n    |",
         '|           |',
         "|$w [{self.name}] $e|",
         '|           |',
         "|    $s    |",
         '+-----------+',
         '';
   }
}

class Die {
   has %.faces;
   submethod TWEAK (:$lines, :$face-size) {
      my @macro-map;
      my $y = 0;
      my $n-faces = 0;
      while $y * $face-size < $lines.elems {
         @macro-map.push: [];
         my $x = 0;
         while $x * $face-size < $lines[0].chars {
            my $face = Nil;
            if $lines[$y * $face-size].substr($x * $face-size, 1) ne ' ' {
               $face = DieFace.new(name => ++$n-faces, :$x, :$y);
               $face.attach(@macro-map[$y][$x - 1], 2) # to west
                  if $x > 0 && defined @macro-map[$y][$x - 1];
               $face.attach(@macro-map[$y - 1][$x], 3) # to north
                  if $y > 0 && defined @macro-map[$y - 1][$x];
               %!faces{"$x,$y"} = $face;
            }
            @macro-map[$y].push: $face;
            ++$x;
         }
         ++$y;
      }
      loop {
         my $n-updates = 0;
         for %!faces.values -> $face {
            my $side = $face.sides[0];
            for ^4 {
               my $cw = $side.cw;
               if defined($side.adj) && defined($cw.adj) {
                  my $n1 = $side.adj.ccw;
                  if ! defined $n1.adj { # set new adjacency
                     $n1.attach($cw.adj.cw);
                     ++$n-updates;
                  }
               }
               $side = $cw; # go to next pair
            }
         }
         last if $n-updates == 0;
      }
   }
}

class DicedField {
   has @.lines;
   has $.x = 0;
   has $.y = 0;
   has $.d = 0;
   has $.n-cols;
   has $.face-size;
   has $.die;

   submethod TWEAK (:$lines) {
      @!lines = $lines.split(/\n+/, :skip-empty);
      $!n-cols = @!lines».chars.max;
      $_ ~= (' ' x ($!n-cols - .chars)) for @!lines;
      $!x = @!lines[0].index('.');
      $!face-size = $!n-cols < 150 ?? 4 !! 50;
      $!die = Die.new(lines => @!lines, :$!face-size);
   }
   method clone { nextwith :lines(@!lines.clone), |%_ }

   method dump {
      say "($!x, $!y), facing {self.direction}, width $!n-cols, map:";
      say '   +', ('-' x $!n-cols), '+';
      @.lines.map: { "   |$_|".say };
      say '   +', ('-' x $!n-cols), '+';
   }

   method direction { '>v<^'.substr($!d, 1) }

   method place ($c = Nil) { @!lines[$!y].substr-rw($!x, 1) = $c // self.direction }

   multi method move (Str $rotations, $place = True) {
      $!d = ($!d + ($_ eq 'R' ?? 1 !! 3)) % 4 for $rotations.comb;
      self.place if $place;
   }
   multi method move (Int $steps, $place = True) {
      state @deltas = [1, 0], [0, 1], [-1, 0], [0, -1];

      self.place if $place;

      my ($x, $y, $d) = $!x, $!y, $!d;
      my ($dx, $dy) = @deltas[$d].Slip;
      for ^$steps {
         $x = $x + $dx;
         $y = $y + $dy;

         my $in-bounds = (0 <= $x < $!n-cols) && (0 <= $y < @!lines);
         my $c = $in-bounds ?? @!lines[$y].substr($x, 1) !! ' ';
         if $c eq ' ' { # do the magic
            my $offset = ($d %% 2 ?? $!y !! $!x) % $!face-size;

            # get original die face
            my $dfx = $!x div $!face-size;
            my $dfy = $!y div $!face-size;
            my $face = $.die.faces{"$dfx,$dfy"};

            my $side = $face.sides[$d];
            my $landing-side = $side.adj;


            my $landing-face = $landing-side.face;
            my $target-d = $landing-side.d;

            my $target-src-d = ($target-d + 2) % 4;
            while $d != $target-src-d {
               $offset = $.face-size - 1 - $offset if $d %% 2;
               $d = ($d + 1) % 4;
            }

            $x = $landing-face.x * $!face-size;
            $y = $landing-face.y * $!face-size;
            if $d %% 2 {
               $x += $!face-size - 1 if $d > 1;
               $y += $offset;
            }
            else {
               $x += $offset;
               $y += $!face-size - 1 if $d > 1;
            }

            $c = @!lines[$y].substr($x, 1);
            ($dx, $dy) = @deltas[$d].Slip;
         }

         last if $c eq '#'; # The Wall

         # save position, rinse, repeat
         ($!x, $!y, $!d) = $x, $y, $d;
         self.place if $place;
      }
   }

   method apply (@moves) {
      self.move($_, True) for @moves;
      return self;
   }
}

sub part2 ($inputs) {
   my $field = DicedField.new(lines => $inputs<lines>);
   $field.apply($inputs<moves>);
   return 1004 + $field.y * 1000 + $field.x * 4 + $field.d;
}
