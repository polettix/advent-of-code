#!/usr/bin/env raku
use v6;

sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.sample')) {
   return solve($filename) if $filename.IO.e;
}

sub solve ($filename) {
   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";

   my $inputs = get-inputs($filename);
   my ($start, $elapsed);

   $start = now;
   my $part1 = part1($inputs);
   $elapsed = now - $start;
   put "part1 ($elapsed) $highlight$part1$reset";

   $start = now;
   my $part2 = part2($inputs);
   $elapsed = now - $start;
   put "part2 ($elapsed) $highlight$part2$reset";

   return 0;
}

sub get-inputs ($filename) {
   $filename.IO.lines.map(
      {
         my $match = m{^Valve \s+ (\S+) .*? (\d+) .*? valves? \s+ (.*)};
         $match[0].Str => {
            name     => $match[0].Str,
            rate     => $match[1].Int,
            adjacent => $match[2].comb(/\w+/).Array,
         };
      }
   ).Hash;
}

sub generate-graph ($inputs) {
   class FloydWarshall { ... }
   my $fw = FloydWarshall.new(
      identifier => -> $v { $v },
      distance   => -> $v, $w { 1 },
      successors => -> $v { $inputs{$v}<adjacent>.Slip.Array },
      starts     => ['AA'],
   );
   my @keys = $inputs.keys.grep({ $inputs{$_}<rate> }).sort;
   my %allowed = @keys.Set;
   my %edges;
   for ('AA', @keys).flat -> $v {
      for @keys -> $w {
         next if $v eq $w;
         my @path = $fw.path($v, $w);
         %edges{$v}{$w} = @path - 1;
      }
   }
   return {
      nodes => ('AA', @keys).flat.map(
         { $_ => hash(name => $_, rate => $inputs{$_}<rate>) }
      ).Hash,
      edges => %edges,
   };
}

sub find-best-score ($graph, $node, $minutes, $done = {}) {
   return 0 if $minutes <= 0 || $done{$node};
   $done{$node} = 1;

   my $score = $graph<nodes>{$node}<rate> * ($minutes - 1); # take

   my $best-sub-score = 0;
   for $graph<edges>{$node}.kv -> $neighbor, $cost {
      next if $done{$neighbor};
      my $score = find-best-score($graph, $neighbor, $minutes - 1 - $cost, $done);
      $best-sub-score = $score if $best-sub-score < $score;
   }

   $done{$node} = 0;
   return $score + $best-sub-score;
}

sub part1 ($inputs) {
   my $graph = generate-graph($inputs);
   return find-best-score($graph, 'AA', 31);
}

sub find-all-solutions ($graph, $node, $minutes, $done = {}) {
   my $node-score = $graph<nodes>{$node}<rate> * $minutes;

   my @solutions;
   for $graph<edges>{$node}.kv -> $neighbor, $cost {
      next if $done{$neighbor} || $minutes - 1 - $cost <= 0;

      $done{$neighbor} = 1;
      for samewith($graph, $neighbor, $minutes - 1 - $cost, $done) -> ($score, $path) {
         $path.unshift: $node;
         @solutions.push: [$node-score + $score, $path];
      }
      $done{$neighbor} = 0;
   }

   # no solutions from children? No worries, we're in a leaf node!
   @solutions.push: [$node-score, [$node]] unless @solutions;

   return @solutions;
}

sub fas ($graph, $minutes, $entry-node = 'AA') {
   my %weight-for;
   my $next-weight = 0x00; # avoid tracking the entry node
   my $following-weight = 0x01;
   my @parent-for = Nil,;
   my @children-for = [];
   my @node-at = Nil;
   my @weight-at = Nil;
   my @set-at = 0;
   my @score-at = 0;
   my %positions-for;
   my @leaves;
   my %seen;
   sub r-fas ($parent-pos, $node, $minutes) {
      return if $minutes <= 0;
      return if %seen{$node}++;

      if %weight-for{$node}:!exists {
         %weight-for{$node} = $next-weight +| 0x00;
         $next-weight = $following-weight +| 0x00;
         $following-weight +<= 1;
      }
      my $node-wg = %weight-for{$node};
      @node-at.push: $node;
      @weight-at.push: $node-wg;
      @parent-for.push: $parent-pos;
      @score-at.push: @score-at[$parent-pos] + $graph<nodes>{$node}<rate> * $minutes;
      @children-for.push: [];
      @set-at.push: @set-at[$parent-pos] +| $node-wg;

      my $my-pos = @parent-for.end;
      %positions-for{$node-wg}.push: $my-pos;
      @children-for[$parent-pos].push: $my-pos;

      # recurse
      for $graph<edges>{$node}.kv -> $neighbor, $cost {
         samewith($my-pos, $neighbor, $minutes - 1 - $cost);
      }
      @leaves.push: $my-pos if $my-pos == @parent-for.end;

      %seen{$node} = 0; # free up this $node
   }
   r-fas(0, $entry-node, $minutes);

   return {
      :@parent-for,
      :@children-for,
      :@node-at,
      :@set-at,
      :@score-at,
      :%positions-for,
      :@leaves,
      :@weight-at,
   };
}

sub fas-best ($solutions) {
   my $best-score = 0;
   my ($p1, $p2);
   sub r-fas-best ($position, $leaves) {
      my $base-score = $solutions<score-at>[$position];
      my $weight = $solutions<weight-at>[$position];
      my $set = $solutions<set-at>[$position];

      # establish applicable leaves for calculations, start with inherited ones
      my @leaves;
      for @$leaves -> $leaf {
         next if $leaf < $position; # cut duplicate checks
         next if $solutions<set-at>[$leaf] +& $set; # intersection
         @leaves.push: $leaf;
         my $score = $base-score + $solutions<score-at>[$leaf];
         if $score > $best-score {
            $best-score = $score;
            $p1 = $position;
            $p2 = $leaf;
         }
      }

      # add nodes before intersections
      for $solutions<positions-for>{$weight}.Slip -> $after-leaf {
         my $leaf = $solutions<parent-for>[$after-leaf];
         #say "   checking leaf<$leaf> before<$after-leaf>";
         next if $leaf < $position; # cut duplicate checks
         next if $solutions<set-at>[$leaf] +& $set; # intersection
         @leaves.push: $leaf;
         my $score = $base-score + $solutions<score-at>[$leaf];
         if $score > $best-score {
            $best-score = $score;
            $p1 = $position;
            $p2 = $leaf;
         }
      }

      # the very first run finds the best single-track solution, so we
      # bothering further here, as "no leaves" means "single track".
      return unless @leaves > 0;

      samewith($_, @leaves) for $solutions<children-for>[$position].Slip;
   }
   r-fas-best(1, $solutions<leaves>);
   say expand-path($solutions, $_) for $p1, $p2;
   return $best-score;
}

sub expand-path ($solutions, $p is rw) {
   my @path;
   while ($p // 0) > 0 {
      @path.unshift: $solutions<node-at>[$p];
      $p = $solutions<parent-for>[$p];
   }
   return @path;
}

sub part2 ($inputs) {
   my %graph = generate-graph($inputs);
   my %solutions = fas(%graph, 26, 'AA');
   return fas-best(%solutions);
}

class FloydWarshall {
   has &!identifier;
   has %!d;  # output, distances
   has %!p;  # output, predecessors
   has %!nf; # output, node data for identifier

   method BUILD (:&distance!, :&successors!, :&!identifier = { ~$^a }, :@starts where *.elems > 0) {
      my @q = @starts;
      while (@q) {
         next if %!nf{my $vi = &!identifier(my $v = @q.shift)}:exists;
         for &successors(%!nf{$vi} = $v) -> $w {
            next if $vi eq (my $wi = &!identifier($w)); # avoid self-edges
            %!d{$vi}{$wi} = &distance($v, $w);
            %!p{$vi}{$wi} = $vi;
            @q.push($w) unless %!nf{$wi}:exists;
         }
      }
      my @vs = %!nf.keys;
      for @vs -> $vi {
         for @vs -> $vv {
            next unless %!p{$vv}{$vi}:exists;
            for @vs -> $vw {
               next unless %!d{$vi}{$vw}:exists;
               my $newd = %!d{$vv}{$vi} + %!d{$vi}{$vw};
               next if %!d{$vv}{$vw}:exists && %!d{$vv}{$vw} <= $newd;
               %!d{$vv}{$vw} = $newd;
               %!p{$vv}{$vw} = %!p{$vi}{$vw};
            }
            die 'negative cycle, bail out' if %!d{$vv}{$vv} < 0;
         }
      }
   }

   method has-path ($v, $w) {
      my ($vi, $wi) = ($v, $w).map({&!identifier($_)}).Slip;
      return (%!d{$vi}:exists) && (%!d{$vi}{$wi}:exists);
   }
   method distance ($v, $w) {
      my ($vi, $wi) = ($v, $w).map({&!identifier($_)}).Slip;
      return unless (%!d{$vi}:exists) && (%!d{$vi}{$wi}:exists);
      return %!d{$vi}{$wi};
   }
   method path ($v, $w) {
      my ($fi, $ti) = ($v, $w).map({&!identifier($_)}).Slip;
      return unless (%!d{$fi}:exists) && (%!d{$fi}{$ti}:exists);
      return reverse gather {
         while $ti ne $fi {
            take %!nf{$ti};
            $ti = %!p{$fi}{$ti};
         }
         take %!nf{$ti}; # take the last too
      }
   }
}
