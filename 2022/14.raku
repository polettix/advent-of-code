#!/usr/bin/env raku
use v6;

sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.sample')) {
   return solve($filename) if $filename.IO.e;
}

sub solve ($filename) {
   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";

   my $inputs = get-inputs($filename);
   #.say for @$inputs;
   my ($start, $elapsed);

   $start = now;
   my $part1 = part1($inputs);
   $elapsed = now - $start;
   put "part1 ($elapsed) $highlight$part1$reset";

   $start = now;
   my $part2 = part2($inputs);
   $elapsed = now - $start;
   put "part2 ($elapsed) $highlight$part2$reset";

   return 0;
}

sub get-inputs ($filename) {
   [
      $filename.IO.lines.map(
         {
            [ .split(/\s*\-\>\s*/).map({[.comb(/\d+/)».Int]}) ]
         }
      )
   ];
}

sub part1 ($inputs) { return 'part 1';
   my $min_x = $inputs.map({$_»[0]}).flat.min;
   my $max_x = $inputs.map({$_»[0]}).flat.max;
   my $min_y = $inputs.map({$_»[1]}).flat.min;
   my $max_y = $inputs.map({$_»[1]}).flat.max;
   my %field;
   for @$inputs -> $poly {
      my ($fx, $fy) = $poly[0].Slip;
      for 1 .. $poly.end -> $i {
         my ($tx, $ty) = $poly[$i].Slip;
         my @xrange = min($fx, $tx) .. max($fx, $tx);
         my @yrange = min($fy, $ty) .. max($fy, $ty);
         for @xrange X @yrange -> ($x, $y) { %field{"$x,$y"} = '#' }
         ($fx, $fy) = $tx, $ty;
      }
   }

   print-field(%field, $min_x .. $max_x, 0 .. $max_y);

   for 1 .. * -> $i {
      my $x = 500;
      my $y = 0;
      loop {
         my $in_bounds = $min_x <= $x <= $max_x && $y <= $max_y;
         if ! $in_bounds {
            print-field(%field, $min_x .. $max_x, 0 .. $max_y);
            return $i - 1;
         }

         my $ny = $y + 1;
         if    %field{"$x,$ny"}:!exists       {      }
         elsif %field{"{$x - 1},$ny"}:!exists { --$x }
         elsif %field{"{$x + 1},$ny"}:!exists { ++$x }
         else                                 { last }
         $y = $ny;
      }
      %field{"$x,$y"} = 'O';
   }

}

sub print-field (%field, @xr, @yr) {
   for @yr -> $y {
      @xr.map( { %field{"$_,$y"} // ' ' }).join('').put;
   }
}

sub part2 ($inputs) {
   my $min_y = $inputs.map({$_»[1]}).flat.min;
   my $max_y = $inputs.map({$_»[1]}).flat.max;
   $max_y += 2;

   my $min_x = min(500 - $max_y, $inputs.map({$_»[0]}).flat.min);
   my $max_x = max(500 + $max_y, $inputs.map({$_»[0]}).flat.max);

   my %field;
   for @$inputs -> $poly {
      my ($fx, $fy) = $poly[0].Slip;
      for 1 .. $poly.end -> $i {
         my ($tx, $ty) = $poly[$i].Slip;
         my @xrange = min($fx, $tx) .. max($fx, $tx);
         my @yrange = min($fy, $ty) .. max($fy, $ty);
         for @xrange X @yrange -> ($x, $y) { %field{"$x,$y"} = '#' }
         ($fx, $fy) = $tx, $ty;
      }
   }

   %field{"$_,$max_y"} = '#' for $min_x .. $max_x;

   print-field(%field, $min_x .. $max_x, 0 .. $max_y);

   for 1 .. * -> $i {
      my $x = 500;
      my $y = 0;
      loop {
         if %field{'500,0'}:exists {
            print-field(%field, $min_x .. $max_x, 0 .. $max_y);
            return $i - 1;
         }

         my $ny = $y + 1;
         if    %field{"$x,$ny"}:!exists       {      }
         elsif %field{"{$x - 1},$ny"}:!exists { --$x }
         elsif %field{"{$x + 1},$ny"}:!exists { ++$x }
         else                                 { last }
         $y = $ny;
      }
      %field{"$x,$y"} = 'O';
   }
}
