#!/usr/bin/env raku
use v6;

sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.sample')) {
   return solve($filename) if $filename.IO.e;
}

sub solve ($filename) {
   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";

   my $inputs = get-inputs($filename);
   my ($start, $elapsed);

   $start = now;
   my $part1 = part1($inputs);
   $elapsed = now - $start;
   put "part1 ($elapsed) $highlight$part1$reset";

   $start = now;
   my $part2 = part2($inputs);
   $elapsed = now - $start;
   put "part2 ($elapsed) $highlight$part2$reset";

   return 0;
}

sub get-inputs ($filename) {
   my @list-for = [], ;
   for $filename.IO.lines -> $line {
      if $line ~~ /^ \s* $/ { @list-for.push: [] }
      else { @list-for[*-1].push: $line.Int }
   }
   @list-for.pop unless @list-for[*-1].elems;
   return @list-for;
}

sub part1 ($inputs) { $inputs».sum.max }

sub part2 ($inputs) { $inputs».sum.sort.reverse[0..2].sum }
