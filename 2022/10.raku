#!/usr/bin/env raku
use v6;

sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.sample')) {
   return solve($filename) if $filename.IO.e;
}

sub solve ($filename) {
   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";

   my $inputs = get-inputs($filename);
   my ($start, $elapsed);

   $start = now;
   my $part1 = part1($inputs);
   $elapsed = now - $start;
   put "part1 ($elapsed) $highlight$part1$reset";

   $start = now;
   my $part2 = part2($inputs);
   $elapsed = now - $start;
   put "part2 ($elapsed) $highlight$part2$reset";

   return 0;
}

sub get-inputs ($filename) {
   my $X = 1;
   return [
      1, $filename.IO.lines.map(
         {
            $_ eq 'noop'
               ?? $X + 0
               !! ($X + 0, ($X += .comb(/ \-? \d+ /)[0]) + 0);
         }
      ).flat.Slip
   ];
}

sub part1 ($inputs) {
   my @targets = 20, 60, 100, 140, 180, 220;
   return (@targets «*» $inputs[@targets «-» 1]).sum;
}

sub part2 ($inputs) {
   my @crt = '';
   for $inputs.kv -> $i, $v {
      @crt.push: '' if $i %% 40;
      my $brush = @crt[*-1].chars;
      @crt[*-1] ~= ($v - 1 <= $brush <= $v + 1) ?? '█' !! ' ';
      last if $i == 239;
   }
   return @crt.join("\n");
}
