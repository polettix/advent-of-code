#!/usr/bin/env raku
use v6;

sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.sample')) {
   return solve($filename) if $filename.IO.e;
}

sub solve ($filename) {
   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";

   my $inputs = get-inputs($filename);
   my ($start, $elapsed);

   $start = now;
   my $part1 = part1($inputs);
   $elapsed = now - $start;
   put "part1 ($elapsed) $highlight$part1$reset";

   $start = now;
   my $part2 = part2($inputs);
   $elapsed = now - $start;
   put "part2 ($elapsed) $highlight$part2$reset";

   return 0;
}

class BluePrint { ... }
sub get-inputs ($filename) {
   [ $filename.IO.lines.map: { BluePrint.new(line => $_) } ]
}

sub part1 ($inputs) {
   $inputs.map({ $_.bid * $_.max-geodes(24) }).sum;
}

sub part2 ($inputs) {
   return [*] $inputs[^3].map({ $_.max-geodes(32) });
}


class BluePrint {
   has $.bid;
   has @!costs-for;
   has @!robot-cap-for;
   has @!resource-cap-for;

   submethod BUILD (:$line) {
      my ($bid, $rr, $cr, $br, $bc, $gr, $gb) = $line.comb(/\d+/)».Int.Slip;
      $!bid = $bid;
      @!costs-for =
         [ $rr,   0,   0,   0 ], # ore
         [ $cr,   0,   0,   0 ], # clay
         [ $br, $bc,   0,   0 ], # obsidian
         [ $gr,   0, $gb,   0 ]; # geodes
      @!robot-cap-for = ($rr, $cr, $br, $gr).max, $bc, $gb, Inf;
      @!resource-cap-for = (@!robot-cap-for «*» 2) «-» 2;
   }

   method can-build (@resources, @n-robots, $robot) {
      return @n-robots[$robot] < @!robot-cap-for[$robot]
         && all(@!costs-for[$robot] «<=» @resources).so;
   }

   method build (@resources, @n-robots, $robot) {
      @resources «+=« (@n-robots «-» @!costs-for[$robot]);
      @n-robots[$robot]++;
   }

   method max-geodes ($time) {
      $*ERR.say("blueprint $!bid");
      my @consider-all = True xx 3;
      my @stack = [[0 xx 4], [1, |(0 xx 3)], $time - 1],;
      my %seen;
      my $best = -1;
      while @stack {
         my $frame = @stack.pop;
         my ($resources, $n-robots, $time, $consider) = @$frame;
         $consider //= @consider-all;

         # cut seen states, cap resources. No need to cap geodes!
         for ^3 -> $i {
            $resources[$i] = @!resource-cap-for[$i]
               if $resources[$i] > @!resource-cap-for[$i];
         }
         my $key = (|$resources, |$n-robots, $time).join(' ');
         next if %seen{$key}++;

         my $best-hope = $resources[3] + $n-robots[3] * (1 + $time)
            + ((1 + $time) * $time) div 2;
         next if $best-hope < $best;

         if self.can-build($resources, $n-robots, 3) { # geode-cracking, yay!
            $resources = [ |$resources ]; # clone
            $n-robots = [ |$n-robots ];         # clone
            self.build($resources, $n-robots, 3);
            @stack.push: [$resources, $n-robots, $time - 1]
               if $time > 0;
         }
         else {
            my @future-consider = |@consider-all;
            for ^3 -> $robot { # don't consider geode-cracking, see above
               next unless $consider[$robot]
                  && self.can-build($resources, $n-robots, $robot);
               @future-consider[$robot] = False;
               my @new-resources = |$resources;
               my @new-n-robots  = |$n-robots;
               self.build(@new-resources, @new-n-robots, $robot);
               @stack.push: [@new-resources, @new-n-robots, $time - 1]
                  if $time > 0;
            }

            # just let time pass here
            $resources «+=» $n-robots;
            @stack.push: [$resources, $n-robots, $time - 1] if $time > 0;
         }

         $best = $resources[3] if $time == 0 && $best < $resources[3];
      }
      return $best;
   }
}
