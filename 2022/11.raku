#!/usr/bin/env raku
use v6;

sub MAIN ($full = Nil) { return solve($full.so) }

sub solve ($with-full) {
   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";

   my ($start, $elapsed);

   $start = now;
   my $part1 = part1(get-inputs($with-full));
   $elapsed = now - $start;
   put "part1 ($elapsed) $highlight$part1$reset";

   $start = now;
   my $part2 = part2(get-inputs($with-full));
   $elapsed = now - $start;
   put "part2 ($elapsed) $highlight$part2$reset";

   return 0;
}

sub get-inputs ($with-full) {
   return [
      monkey([79, 98],           sub { $^old * 19 },     23, 2, 3),
      monkey([54, 65, 75, 74],   sub { $^old + 6 },      19, 2, 0),
      monkey([79, 60, 97],       sub { $^old * $^old },  13, 1, 3),
      monkey([74],               sub { $^old + 3 },      17, 0, 1),
   ] unless $with-full;
   return [
      monkey([64],                              sub { $^old * 7 }    , 13, 1, 3),
      monkey([60, 84, 84, 65],                  sub { $^old + 7 }    , 19, 2, 7),
      monkey([52, 67, 74, 88, 51, 61],          sub { $^old * 3 }    ,  5, 5, 7),
      monkey([67, 72],                          sub { $^old + 3 }    ,  2, 1, 2),
      monkey([80, 79, 58, 77, 68, 74, 98, 64],  sub { $^old * $^old }, 17, 6, 0),
      monkey([62, 53, 61, 89, 86],              sub { $^old + 8  }   , 11, 4, 6),
      monkey([86, 89, 82],                      sub { $^old + 2 }    ,  7, 3, 0),
      monkey([92, 81, 70, 96, 69, 84, 83],      sub { $^old + 4 }    ,  3, 4, 5),
   ];
}

sub monkey ($items, &op, $divisor, $next-true, $next-false) {
   my %retval =
      items   => $items,
      op      => &op,
      divisor => $divisor,
      true    => $next-true,
      false   => $next-false;
   return %retval;
}

sub round (@monkeys, @stats, $divisor = 1) {
   state $period = [*] (2, 3, 5, 7, 11, 13, 17, 19, 23);
   for @monkeys.kv -> $i, $monkey {
      while $monkey<items>.elems {
         my $item = $monkey<items>.shift;
         @stats[$i]++;
         my $new = ($monkey<op>($item) / $divisor).Int % $period;
         if $new %% $monkey<divisor> { @monkeys[$monkey<true> ]<items>.push: $new }
         else                        { @monkeys[$monkey<false>]<items>.push: $new }
      }
   }
}

sub part1 (@monkeys) {
   my @stats;
   round(@monkeys, @stats, 3) for ^20;
   return [*] @stats.sort.tail(2);
}

sub part2 (@monkeys) {
   my @stats;
   round(@monkeys, @stats, 1) for ^10000;
   return [*] @stats.sort.tail(2);
}
