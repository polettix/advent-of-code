#!/usr/bin/env raku
use v6;

sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.sample')) {
   return solve($filename) if $filename.IO.e;
}

sub solve ($filename) {
   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";

   my $inputs = get-inputs($filename);
   my ($start, $elapsed);

   $start = now;
   my $part1 = part1($inputs);
   $elapsed = now - $start;
   put "part1 ($elapsed) $highlight$part1$reset";

   $start = now;
   my $part2 = part2($inputs);
   $elapsed = now - $start;
   put "part2 ($elapsed) $highlight$part2$reset";

   return 0;
}

sub get-inputs ($filename) {
   my @lines = $filename.IO.lines;
   my $inputs = {};

   my @stacks = [''],;
   while @lines {
      my $line = @lines.shift;
      last if $line ~~ /^ \s* $/;
      next if $line !~~ / \[ /;
      my $i = 1;
      while $line.chars >= 3 {
         my $char = $line.substr(1, 1);
         @stacks[$i].unshift($char) if $char ne ' ';
         $line.substr-rw(0, 4) = '';
         ++$i;
      }
   }
   $inputs<stacks> = @stacks;

   $inputs<moves> = @lines.map({ [.comb(/\d+/)] }).Array;

   return $inputs;
}

sub part1 ($inputs) {
   my $data = $inputs<stacks>.deepmap(*.clone);
   for $inputs<moves>.Slip -> $move {
      my ($amount, $from, $to) = @$move;
      $data[$to].push: $data[$from].pop for ^$amount;
   }
   $data»[*-1].join('');
}

sub part2 ($inputs) {
   my $data = $inputs<stacks>.deepmap(*.clone);
   for $inputs<moves>.Slip -> $move {
      my ($amount, $from, $to) = @$move;
      $data[$to].push: $data[$from][*-$amount .. *-1].Slip;
      $data[$from][*-$amount .. *-1]:delete;
   }
   $data»[*-1].join('');
}
