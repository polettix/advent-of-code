#!/usr/bin/env raku
use v6;

sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.sample')) {
   return solve($filename) if $filename.IO.e;
}

sub solve ($filename) {
   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";

   my $inputs = get-inputs($filename);
   my ($start, $elapsed);

   $start = now;
   my $part1 = part1($inputs);
   $elapsed = now - $start;
   put "part1 ($elapsed) $highlight$part1$reset";

   $start = now;
   my $part2 = part2($inputs);
   $elapsed = now - $start;
   put "part2 ($elapsed) $highlight$part2$reset";

   return 0;
}

sub get-inputs ($filename) {
   $filename.IO.lines.Array
}

sub part1 ($inputs) {
   my %score-for = <X 1 Y 2 Z 3 A 1 B 2 C 3>;
   my %dscore-for = <0 3 1 0 2 6>;
   my $sum = 0;
   for @$inputs -> $line {
      my ($elf, $me) = $line.comb(/\S+/).map({%score-for{$_}});
      my $outcome = (($elf - $me) % 3);
      $sum += $me + %dscore-for{$outcome};
   }
   return $sum;
}

sub part2 ($inputs) {
   my %score-for = <X 0 Y 1 Z 2 A 0 B 1 C 2>;
   my %dscore-for = <0 3 1 0 2 6>;
   my $sum = 0;
   for @$inputs -> $line {
      my ($elf, $outcome) = $line.comb(/\S+/).map({%score-for{$_}});
      my $me = $outcome == 0 ?? $elf - 1 !! $outcome == 1 ?? $elf !! $elf + 1;
      $sum += $outcome * 3;
      $sum += 1 + $me % 3;
   }
   return $sum;
   return 'part2'
}
