#!/usr/bin/env raku
use v6;

sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.sample'), $y = 10) {
   return solve($filename, $y) if $filename.IO.e;
}

sub solve ($filename, $y) {
   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";

   my $inputs = get-inputs($filename);
   my ($start, $elapsed);

   $start = now;
   my $part1 = part1($inputs, $y);
   $elapsed = now - $start;
   put "part1 ($elapsed) $highlight$part1$reset";

   $start = now;
   my $part2 = part2($inputs, $y);
   $elapsed = now - $start;
   put "part2 ($elapsed) $highlight$part2$reset";

   return 0;
}

sub get-inputs ($filename) {
   $filename.IO.lines.map({ [.comb(/\-?\d+/)».Int]  }).Array;
}

class Ranges { # open on the left, close to the right
   has @!parts;

   method add-part ($min, $max) {
      my $low = $min - 1;
      my @old-parts = @!parts.Slip;
      @!parts = [];
      while @old-parts {
         my $part = @old-parts.shift;
         if $max < $part[0] {  # disjoint, close
            @!parts.push: [$low, $max], $part;
            $low = Nil;
            last;
         }
         elsif $max <= $part[1] { # fuse & close
            @!parts.push: [min($low, $part[0]), max($max, $part[1])];
            $low = Nil;
            last;
         }
         elsif $low <= $part[0] { } # ($low, $max] swallows $part
         elsif $low <= $part[1] { $low = $part[0] }
         else { @!parts.push: $part }
      }
      @!parts.push: @old-parts.Slip;
      @!parts.push: [$low, $max] if defined($low);
   }

   method remove-part ($min, $max) {
      my $low = $min - 1;
      my @old-parts = @!parts.Slip;
      @!parts = [];
      while @old-parts {
         my $part = @old-parts.shift;
         if $max <= $part[0] { @!parts.push: $part; last }
         elsif $part[1] <= $low { @!parts.push: $part }
         elsif $low <= $part[0] {
            @!parts.push: [$max, $part[1]] if $max < $part[1];
            last;
         }
         else { # $part[0] < $low
            @!parts.push: [$part[0], $low];
            @!parts.push: [$max, $part[1]] if $max < $part[1];
            last;
         }
      }
      @!parts.push: @old-parts.Slip;
   }

   method count { @!parts.map({$_[1] - $_[0]}).sum }
}

sub part1 ($inputs, $y) {
   my $ranges = Ranges.new;
   my @inline;
   for @$inputs -> ($sx, $sy, $bx, $by) {
      @inline.push: [$sx, $sx] if $sy == $y;
      @inline.push: [$bx, $bx] if $by == $y;
      my $distance = ($sx - $bx, $sy - $by)».abs.sum;
      my $overlap = $distance - abs($sy - $y);
      next if $overlap < 0;
      $ranges.add-part($sx - $overlap, $sx + $overlap);
   }
   $ranges.remove-part(|$_) for @inline;
   return $ranges.count;
}

sub part2 ($inputs, $midsize) {
   my $size = $midsize * 2;
   my @t-rex = $inputs.map: ->($x, $y, $bx, $by) {
      my @r = (1 + ($x - $bx, $y - $by)».abs.sum) «*» (-1, 1);
      (($x + $y) «+» @r, ($x - $y) «+» @r).flat.Array;
   };

   for @t-rex -> $h {
      my ($hx-min, $hx-max, $hy-min, $hy-max) = |$h;
      for @t-rex -> $v {
         next if $h === $v;
         my ($vx-min, $vx-max, $vy-min, $vy-max) = |$v;
         PAIR:
         for ($hy-min, $hy-max) X ($vx-min, $vx-max) -> ($hy, $vx) {
            next unless $vy-min <= $hy <= $vy-max
                     && $hx-min <= $vx <= $hx-max;
            next PAIR if ($vx + $hy) % 2;
            for @t-rex -> ($cx-min, $cx-max, $cy-min, $cy-max) {
               next PAIR if $cx-min < $vx < $cx-max
                         && $cy-min < $hy < $cy-max;
            }
            my $x = ($vx + $hy) div 2;
            next PAIR unless 0 <= $x <= $size;
            my $y = ($vx - $hy) div 2;
            next PAIR unless 0 <= $y <= $size;
            return $x * $size + $y;
         }
      }
   }

   return 'part2';
}
