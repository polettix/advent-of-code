#!/usr/bin/env raku
use v6;

sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.sample')) {
   return solve($filename) if $filename.IO.e;
}

sub solve ($filename) {
   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";

   my $inputs = get-inputs($filename);
   my ($start, $elapsed);

   $start = now;
   my $part1 = part1($inputs);
   $elapsed = now - $start;
   put "part1 ($elapsed) $highlight$part1$reset";

   $start = now;
   my $part2 = part2($inputs);
   $elapsed = now - $start;
   put "part2 ($elapsed) $highlight$part2$reset";

   return 0;
}

sub get-inputs ($filename) {
   $filename.IO.lines.Array
}

sub detect-different($string, $n = 4) {
   my $i = $n - 1;
   my $window = BagHash.new($string.substr(0, $i).comb);
   loop {
      $window.add($string.substr($i, 1));
      return $i + 1 if $window.elems == $n;
      $window.remove($string.substr($i + 1 - $n, 1));
      ++$i;
   };
}

sub part1 ($inputs) { @$inputs.map({ detect-different($_) }) }
sub part2 ($inputs) { @$inputs.map({ detect-different($_, 14) }) }
