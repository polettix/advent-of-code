#!/bin/sh
me="$(readlink -f "$0")"
md="$(dirname "$me")"
read session <"$md/local/session-cookie"

if [ $# -eq 2 ] ; then
   year="$1"
   shift
else
   year="$(basename "$PWD")"
   if printf %s "$year" | grep -v '^20[0-9][0-9]$' >/dev/null 2>&1 ; then
      year="$(date +%Y)"
   fi
fi

if [ $# -eq 1 ] ; then
   day="$(printf %s "$1" | sed -e 's/^0//')"
   day2="$(printf %s "0$day" | sed -e 's/^.*\(..\)$/\1/')"
else
   read  day day2 <<END
$(date +'%e  %d')
END
fi

printf %s\\n "$day2"

blogurl='https://etoobusy.polettix.it/2022/11/28/aoc-inputs-downloader/'
email='flavio@polettix.it'
curl -sL --cookie "session=$session" \
   -A "$blogurl" -H "From: $email" \
   "https://adventofcode.com/$year/day/$day/input" \
   | tee "$day2.input"
