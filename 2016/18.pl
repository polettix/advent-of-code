#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
$|++;

my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
open my $fh, '<', $filename;
while (<$fh>) {
   my ($n_rows, $first_row) = split m{\s+}mxs;
   say count_safe_tiles($n_rows, $first_row);
}
close $fh;

sub count_safe_tiles ($n, $row) {
   my $count = 0;
   while ('necessary') {
      $count += (my $tmp = $row) =~ tr{.}{ };
      last unless --$n;
      $row = next_row($row);
   }
   return $count;
}

sub next_row ($row) {
   my @previous_row = (split(m{}mxs, $row), '.'); # add "walls"
   my $v = $previous_row[0] eq '^' ? 0b010 : 0;
   state $char_for = {
      0b000 => '.',
      0b001 => '^',
      0b010 => '.',
      0b011 => '^',
      0b100 => '^',
      0b101 => '.',
      0b110 => '^',
      0b111 => '.',
   };
   return join '', map {
      $v |= 0b100 if $previous_row[$_] eq '^';
      my $char = $char_for->{$v};
      $v >>= 1;
      $char;
   } 1 .. $#previous_row;
}
