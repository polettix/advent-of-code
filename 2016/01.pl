#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
$|++;

my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.input}rmxs;
open my $fh, '<', $filename;
chomp(my @moves = split m{\s*,\s*}mxs, <$fh>);
close $fh;

{
   my ($x, $y, $d) = (0, 0, 'n');
   my %delta_for = (
      n => [0, 1],
      e => [1, 0],
      s => [0, -1],
      w => [-1, 0],
   );
   my %new_dir_for = (
      L => {qw< n e e s s w w n >},
      R => {qw< n w w s s e e n >},
   );
   my (%flag_for, $first_twice);
   $flag_for{"0,0"} = 1;
   for my $move (@moves) {
      my ($turn, $amplitude) = $move =~ m{\A (L|R) (\d+) \z}mxs;
      $d = $new_dir_for{$turn}{$d};
      my ($dx, $dy) = $delta_for{$d}->@*;
      for (1 .. $amplitude) {
         $x += $dx;
         $y += $dy;
         $first_twice //= [$x, $y] if $flag_for{"$x,$y"}++;
      }
   }
   say 'part1 => ', abs($x) + abs($y);
   say 'part2 => ', abs($first_twice->[0]) + abs($first_twice->[1]);
}
