#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
$|++;

my @rooms;

my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
open my $fh, '<', $filename;
while (<$fh>) {
   chomp;
   my ($name, $sector_id, $checksum) = m{\A (.*) - (\d+) \[(.*)\] \z}mxs;
   push @rooms, {
      name => $name,
      sector_id => $sector_id,
      checksum => $checksum,
      original => $_,
   };
}
close $fh;

my $sum1 = 0;
for my $room (@rooms) {
   next unless checksum_for($room->{name}) eq $room->{checksum};
   $sum1 += $room->{sector_id};
   my $letters = join '', 'a' .. 'z';
   my $rot = $room->{sector_id} % length($letters);
   my $substitutes = substr($letters, $rot) . substr($letters, 0, $rot);
   my $name = $room->{name};
   eval "\$name =~ tr{-$letters}{ $substitutes}";
   say "$room->{sector_id} $name" if $name =~ m{north};
}
say 'part1 => ', $sum1;


sub checksum_for ($name) {
   my %count_for;
   $count_for{$_}++ for ($name =~ m{([a-z])}gmxs);
   my %letters_for;
   push $letters_for{$count_for{$_}}->@*, $_ for keys %count_for;
   my @sorted;
   push @sorted, sort {$a cmp $b} $letters_for{$_}->@*
      for reverse sort {$a <=> $b} keys %letters_for;
   my $retval = substr join('', @sorted), 0, 5;
   return $retval;
}
