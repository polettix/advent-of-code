#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
$|++;

my @recipes;
my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
open my $fh, '<', $filename;
while (<$fh>) {
   chomp;
   push @recipes, [split m{}mxs];
}
close $fh;

{
   my ($x, $y) = (0, 0);
   my ($x2, $y2) = (0, 2);
   my %delta_for = (
      L => [-1,  0],
      R => [ 1,  0],
      U => [ 0, -1],
      D => [ 0,  1],
   );
   my (@digits, @digits2);
   my @keypad2 = (
      [ qw< X X 1 X X > ],
      [ qw< X 2 3 4 X > ],
      [ qw< 5 6 7 8 9 > ],
      [ qw< X A B C X > ],
      [ qw< X X D X X > ],
   );
   for my $recipe (@recipes) {
      for my $step ($recipe->@*) {
         my ($dx, $dy) = $delta_for{$step}->@*;
         $x += $dx; $x = $x > 1 ? 1 : $x < -1 ? -1 : $x;
         $y += $dy; $y = $y > 1 ? 1 : $y < -1 ? -1 : $y;
         $x2 = part2_adjust($x2 + $dx, $y2);
         $y2 = part2_adjust($y2 + $dy, $x2);
      }
      push @digits, (5 + $y * 3 + $x);
      push @digits2, $keypad2[$y2][$x2];
   }
   say 'part1 => ', @digits;
   say 'part2 => ', @digits2;
}

sub part2_adjust ($v, $u) {
   if ($u == 2) {
      return 0 if $v < 0;
      return 4 if $v > 4;
      return $v;
   }
   elsif ($u == 1 || $u == 3) {
      return 1 if $v < 1;
      return 3 if $v > 3;
      return $v;
   }
   else { return 2 };
}
