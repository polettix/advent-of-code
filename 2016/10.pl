#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
$|++;

my %slots;
my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
open my $fh, '<', $filename;
while (<$fh>) {
   if (m{\A bot\ (\d+)\ gives\ low\ to\ (bot|output)\ (\d+) \s+
                         and\ high\ to\ (bot|output)\ (\d+)}mxs) {
      my ($id, $t1, $i1, $t2, $i2) = ($1, $2, $3, $4, $5);
      my $bot = $slots{bot}{$id} //= {store => []};
      $slots{$t1}{$i1} //= {store => []};
      $slots{$t2}{$i2} //= {store => []};
      $bot->{lo} = {type => $t1, id => $i1};
      $bot->{hi} = {type => $t2, id => $i2};
   }
   elsif (m{\A value .*? (\d+) .*? (\d+)}mxs) {
      push $slots{bot}{$2}{store}->@*, $1;
   }
}
close $fh;

my $changes = -1;
my ($p1, $p2);
while ($changes && !($p1 && $p2)) {
   #print_slots(\%slots);
   $changes = part1_one_pass(\%slots);
   if (!$p1 && $changes && $changes->[1] == 17 && $changes->[2] == 61) {
      say 'part1 => ', $changes->[0];
      $p1 = 1;
   }
   if (!$p2) {
      my @v = grep {defined} map { ($slots{output}{$_}{store}->@*)[0] } 0 .. 2;
      if (@v == 3) {
         say 'part2 => ', $v[0] * $v[1] * $v[2];
         $p2 = 1;
      }
   }
}

sub part1_one_pass ($state) {
   BOT:
   for my $id (keys $state->{bot}->%*) {
      my $bot = $state->{bot}{$id};
      #while (my ($id, $bot) = each $state->{bot}->%*) {
      my ($lo, $hi) = sort {$a <=> $b} $bot->{store}->@*;
      next unless defined $hi;

      my %target_for;
      for my $key (qw< lo hi >) {
         my ($type, $id) = $bot->{$key}->@{qw< type id >};
         $target_for{$key} = $state->{$type}{$id};
         next BOT if $type eq 'bot' && $target_for{$key}{store}->@* > 1;
      }

      push $target_for{lo}{store}->@*, $lo;
      push $target_for{hi}{store}->@*, $hi;
      $bot->{store}->@* = ();

      return [$id, $lo, $hi];
   }
   return;
}

sub print_slots ($state) {
   for my $type (qw< bot output >) {
      my $slot = $state->{$type};
      for my $id (sort {$a <=> $b} keys $slot->%*) {
         printf {*STDOUT} "%s %d (%s)\n", $type, $id,
            join(', ', $slot->{$id}{store}->@*);
      }
   }
   say {*STDOUT} '-' x 48;
   return;
}
