#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
$|++;

use constant TOP => $ENV{TOP} // 0xFFFFFFFF;

my @ranges;

my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
open my $fh, '<', $filename;
while (<$fh>) {
   push @ranges, [split m{\D+}mxs];
}
close $fh;

my $candidate = 0;
my $allowed = 0;
for my $range (sort {$a->[0] <=> $b->[0]} @ranges) {
   my ($l, $h) = $range->@*;
   if ($l > $candidate) { # yay!
      say 'part1 => ', $candidate unless $allowed;
      $allowed += $l - $candidate;
   }
   $candidate = $h + 1 if $h + 1 > $candidate;
}
$allowed += TOP - $candidate + 1;
say 'part2 => ', $allowed;
