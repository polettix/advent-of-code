#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
$|++;

my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
open my $fh, '<', $filename;
my $text = join '', <$fh>;
close $fh;
$text =~ s{\s+}{}gmxs;

my $decompressed = decompress($text);
say 'part1 => ', length $decompressed;
say 'part2 => ', decompress_length_2($text);

sub decompress ($compressed) {
   my $decompressed = '';
   while (length $compressed) {
      $compressed =~ s{
         \A
         ([^\(]*)
         \( (\d+) x (\d+) \)
      }{}mxs or last;
      $decompressed .= $1 . substr($compressed, 0, $2, '') x $3;
   }
   return $decompressed . $compressed;
}

sub decompress_length_2 ($compressed) {
   my $length = 0;
   while (length $compressed) {
      $compressed =~ s{
         \A
         ([^\(]*)
         \( (\d+) x (\d+) \)
      }{}mxs or last;
      my ($prefix, $n_chars, $repetitions) = ($1, $2, $3);
      $length += length($prefix);
      $length += decompress_length_2(substr $compressed, 0, $n_chars, '') * $repetitions;
   }
   return $length + length($compressed);
}
