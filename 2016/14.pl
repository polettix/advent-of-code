#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
use Digest::MD5 'md5_hex';
$|++;

use constant KEYS => 64;
use constant WINDOW_SIZE => 1000;

my $salt;
my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
open my $fh, '<', $filename;
chomp($salt = <$fh>);
close $fh;

for my $stretch (0, 1) {
   my %window;
   my %waiting;
   my $counter = -1;
   my @keys;
   while ((@keys < KEYS) || ($keys[KEYS - 1] > $counter - WINDOW_SIZE + 10)){
      my $hash = hash($salt . (++$counter), $stretch);
      if (my $statistics = statistics_for($hash)) {
         $statistics->{counter} = $counter;
         my ($three, $fives) = $statistics->@{qw< three fives >};
         for my $five ($fives->@*) {
            my $new_keys = delete $waiting{$five} or next;
            delete $window{$_} for $new_keys->@*;
            @keys = sort {$a <=> $b} @keys, $new_keys->@*;
         }
         push $waiting{$three}->@*, $counter;
         $window{$counter} = $three;
      }
      my $old_counter = $counter - WINDOW_SIZE;
      defined(my $old_three = delete $window{$old_counter}) or next;
      shift $waiting{$old_three}->@*;
      delete $waiting{$old_three} unless $waiting{$old_three}->@*;
   }

   say $keys[KEYS - 1];
}

sub hash ($string, $stretch) {
   my $hash = md5_hex($string);
   if ($stretch) { $hash = md5_hex($hash) for 1 .. 2016 }
   return $hash;
}

sub statistics_for ($string) {
   my ($three, %fives);
   while ($string =~ m{(?<char>\S)(?<other>\g{char}\g{char}+)}gmxs) {
      my ($char, $len) = ($+{char}, 1 + length $+{other});
      $three //= $char;
      $fives{$char} = 1 if $len >=5;
   }
   return unless defined $three;
   return {string => $string, three => $three, fives => [keys %fives]};
}
