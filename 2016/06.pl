#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper;
$Data::Dumper::Indent = 1;
use Storable 'dclone';
$|++;

my @stats_for;
my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
open my $fh, '<', $filename;
while (<$fh>) {
   chomp;
   my @chars = split m{}mxs;
   $stats_for[$_]{$chars[$_]}++ for 0 .. $#chars;
}
close $fh;

{
   my @message = map {
      my ($c) =
        reverse sort { $_->{$a} <=> $_->{$b} }
        keys $_->%*
   } @stats_for;
   say 'part1 => ', @message;
}

{
   my @message = map {
      my ($c) =
        sort { $_->{$a} <=> $_->{$b} }
        keys $_->%*
   } @stats_for;
   say 'part2 => ', @message;
}
