#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
$|++;

my @program;
my %registers = map {$_ => 0} 'a' .. 'd';
my $pc = 0;

my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
open my $fh, '<', $filename;
while (<$fh>) {
   if (my ($from, $to) = m{\A cpy \s* (\S+) \s* (\S+)}mxs) {
      if ($from =~ m{\d}mxs) {
         push @program, sub {$registers{$to} = $from; ++$pc};
      }
      else {
         push @program, sub {$registers{$to} = $registers{$from}; ++$pc}
      }
   }
   elsif (my ($rinc) = m{\A inc \s+ (\S+)}mxs) {
      push @program, sub {$registers{$rinc}++; ++$pc};
   }
   elsif (my ($rdec) = m{\A dec \s+ (\S+)}mxs) {
      push @program, sub {$registers{$rdec}--; ++$pc};
   }
   elsif (my ($condition, $delta) = m{\A jnz \s+ (\S+) \s+ (\S+)}mxs) {
      if ($condition =~ m{\d}) {
         push @program, $condition ? sub {$pc += $delta} : sub {++$pc};
      }
      else {
         push @program, sub {$pc += $registers{$condition} ? $delta : 1};
      }
   }
   else { die "wtf?!?"; }
}
close $fh;

while ($pc <= $#program) {
   #say join ' ', $pc, map {$registers{$_}} sort {$a cmp $b} keys %registers;
   $program[$pc]->();
}
say Dumper \%registers;
