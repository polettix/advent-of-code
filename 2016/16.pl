#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
$|++;

my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
open my $fh, '<', $filename;
while (<$fh>) {
   my ($string, $target_length) = split m{\s+};
   my $checksum = checksum(dragonify($string, $target_length));
   $checksum = checksum($checksum) while length($checksum) % 2 == 0;
   say $checksum;
}
close $fh;

sub dragonify ($string, $target_length) {
   while (length $string < $target_length) {
      (my $copy = reverse $string) =~ tr{01}{10};
      $string .= '0' . $copy;
   }
   return substr $string, 0, $target_length;
}

sub checksum ($string) {
   my @retval;
   for (my $i = 0; $i < length $string; $i += 2) {
      push @retval, ((substr($string, $i, 1) xor substr($string, $i + 1, 1)) ? 0 : 1);
   }
   return join '', @retval;
}
