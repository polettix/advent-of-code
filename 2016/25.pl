#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
$|++;

my (@init, @program);
my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
open my $fh, '<', $filename;
while (<$fh>) {
   if (my ($from, $to) = m{\A cpy \s* (\S+) \s* (\S+)}mxs) {
      push @program, [cpy => $from => $to];
   }
   elsif (my ($rinc) = m{\A inc \s+ (\S+)}mxs) {
      push @program, [inc => $rinc];
   }
   elsif (my ($rdec) = m{\A dec \s+ (\S+)}mxs) {
      push @program, [dec => $rdec];
   }
   elsif (my ($condition, $delta) = m{\A jnz \s+ (\S+) \s+ (\S+)}mxs) {
      push @program, [jnz => $condition => $delta];
   }
   elsif (my ($target) = m{\A tgl \s+ (\S+)}mxs) {
      push @program, [tgl => $target];
   }
   elsif (my ($reg, $value) = m{\A set \s+ (\S+) \s+ (\S+)}mxs) {
      push @init, sub ($process) { $process->{r}{$reg} = $value };
   }
   elsif (my ($out) = m{\A out \s+ (\S+)}mxs) {
      push @program, [out => $out];
   }
   else { die "wtf?!?"; }
}
close $fh;

my %cb = (
);

my $A = 0;
my $IO = Assembunny::IO->new;
while ('necessary') {
   $A++;
   $IO->rst;
   print "$A ---> "; # if $A % 42 == 0;
   eval {
      run([sub ($p) {$p->{r}{a} = $A}], \@program, \%cb);
   } or do {
      die $@ unless ref $@;
      if ($@->{status} eq 'OK') {
         say "\npart1 => $A";
         last;
      }
      else {
         say " nope";
      }
   };
}

sub run ($init, $program, $cb = {}) {
   my %process = (
      pc => 0,
      r  => { map {$_ => 0} 'a' .. 'd' },
      p  => [$program->@*],
   );
   $_->(\%process) for $init->@*;
   my %cb_for;
   while ($process{pc} <= $#{$process{p}}) {
      my $pc = $process{pc};
      $cb->{$pc}->(\%process) if exists $cb->{$pc};
      my ($op, @args) = $process{p}[$pc]->@*;
      my $cb = $cb_for{$op} //= __PACKAGE__->can('op_' . $op)
         or die "unhandled op <$op>";
      $cb->(\%process, @args);
   }
   return \%process;
}

sub op_out ($process, $out) {
   $IO->out(_resolve($process, $out));
   $process->{pc}++;
   return $process;
}

sub op_cpy ($process, $from, $to) {
   $process->{r}{$to} = _resolve($process, $from) if __is_reg($to);
   $process->{pc}++;
   return $process;
}

sub op_inc ($process, $register) {
   $process->{r}{$register}++ if __is_reg($register);
   $process->{pc}++;
   return $process;
}

sub op_dec ($process, $register) {
   $process->{r}{$register}-- if __is_reg($register);
   $process->{pc}++;
   return $process;
}

sub op_jnz ($process, $c, $d) {
   $process->{pc} += _resolve($process, $c) ? _resolve($process, $d) : 1;
   return $process;
}

sub ___dump ($process) { say ___state($process) }

sub ___state ($process) {
   return "pc<$process->{pc}> op<$process->{p}[$process->{pc}]->@*> [", ___regs($process), ']';
}
sub ___regs ($process) {
   my $r = $process->{r};
   return join ', ', map { "$_<$r->{$_}>" } sort keys $r->%*;
}

sub op_tgl ($process, $target) {
   my $abs_target = $process->{pc} + _resolve($process, $target);
   say "toggle $target -> $abs_target ", ___regs($process);
   ___regs($process);
   my $program = $process->{p};
   if (0 <= $abs_target && $abs_target <= $#$program) {
      die Dumper [$abs_target, $process, $#$program] unless defined $program->[$abs_target];
      my ($op, @args) = $program->[$abs_target]->@*;
      if ($op eq 'inc') {
         $program->[$abs_target] = [dec => @args];
      }
      elsif (@args == 1) {
         $program->[$abs_target] = [inc => @args];
      }
      elsif ($op eq 'jnz') {
         $program->[$abs_target] = [cpy => @args];
      }
      elsif (@args == 2) {
         $program->[$abs_target] = [jnz => @args];
      }
      else {
         die "wtf?!? ($op @args)";
      }
   }
   $process->{pc}++;
   return $process;
}

sub _resolve ($p, $t) { return __is_reg($t) ? $p->{r}{$t} : $t }
sub __is_reg ($target) { return $target !~ m{\A [-\d]}mxs }

package Assembunny::IO;
use strict;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;

sub new ($p) { bless({}, $p)->rst }
sub rst ($s) { %$s = (v => 0, n => 10); $s }
sub out ($s, $v) {
   print $v;
   die { status => 'Not OK' } unless $s->{v} == $v;
   die { status => 'OK' } if $s->{n}-- <= 0;
   $s->{v} = $v ? 0 : 1;
   return $s;
}
