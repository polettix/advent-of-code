#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
use Digest::MD5 'md5_hex';
$|++;

my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
open my $fh, '<', $filename;
chomp(my $name = <$fh>);
close $fh;

my $index = 0;
my @password;
my @password2 = ('*') x 8;
my $left = 8;
while ($left > 0) {
   my $md5 = md5_hex($name . $index++);
   my ($x, $y) = $md5 =~ m{\A 00000 (.)(.)}mxs or next;
   push @password, $x if @password < 8;
   next unless $x =~ m{\A [0-7] \z}mxs;
   next if $password2[$x] ne '*';
   $password2[$x] = $y;
   --$left;
   print "\r", @password2;
}
say '';
say 'part1 => <', @password, '>';
say 'part2 => <', @password2, '>';
