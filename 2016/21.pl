#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper;
$Data::Dumper::Indent = 1;
use Storable 'dclone';
$|++;

if (0) {
   my $pass1 = 'abcdefgh';
   $pass1 = 'abcde';
   $pass1 = swap_positions($pass1, 4, 0);
   $pass1 = swap_letters($pass1, 'd', 'b');
   $pass1 = reverse_positions($pass1, 0, 4);
   $pass1 = rotate($pass1, left => 1);
   $pass1 = move($pass1, 1, 4);
   $pass1 = move($pass1, 3, 0);
   $pass1 = rotate_positional($pass1, 'b');
   $pass1 = rotate_positional($pass1, 'd');

   say $pass1;
   exit 0;
} ## end if (0)

my ($pass1, $pass2);

my @operations = (
   [
      qr{swap position (\d+) with position (\d+)}ms, \&swap_positions,
      \&swap_positions
   ],
   [
      qr{swap letter (\w) with letter (\w)}ms, \&swap_letters,
      \&swap_letters
   ],
   [qr{rotate (left|right) (\d+) step}ms, \&rotate, \&derotate],
   [
      qr{rotate based on position of letter (\w)}ms, \&rotate_positional,
      \&derotate_positional
   ],
   [
      qr{reverse positions (\d+) through (\d+)}ms, \&reverse_positions,
      \&reverse_positions
   ],
   [qr{move position (\d+) to position (\d+)}ms, \&move, \&demove],
);

my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
open my $fh, '<', $filename;
chomp($pass1 = <$fh>);
chomp($pass2 = <$fh>);
my @pass2_sequence;
while (<$fh>) {
   for my $op (@operations) {
      my @captures = m{$op->[0]} or next;
      $pass1 = $op->[1]->($pass1, @captures);
      push @pass2_sequence, [$op->[2], @captures];
   }
} ## end while (<$fh>)
close $fh;

say 'part1 => ', $pass1;

for my $op2 (reverse @pass2_sequence) {
   my ($sub, @args) = $op2->@*;
   $pass2 = $sub->($pass2, @args);
}
say 'part2 => ', $pass2;

sub swap_positions ($string, $x, $y) {
   substr $string, $x, 1, substr $string, $y, 1, substr $string, $x, 1;
   return $string;
}

sub swap_letters ($string, $x, $y) {
   eval "\$string =~ tr{$x$y}{$y$x}";
   return $string;
}

sub reverse_positions ($string, $from, $to) {
   my $n = $to - $from + 1;
   substr $string, $from, $n, scalar reverse substr $string, $from, $n;
   return $string;
}

sub rotate ($string, $direction, $amount) {
   my $l = length $string;
   $amount %= $l;
   $amount = $l - $amount if $direction eq 'right';
   $string .= substr $string, 0, $amount, '';
   return $string;
} ## end sub rotate

sub derotate ($string, $d, $amount) {
   return rotate($string, ($d eq 'right' ? 'left' : 'right'), $amount);
}

sub move ($string, $from, $to) {
   substr $string, $to, 0, substr $string, $from, 1, '';
   return $string;
}

sub demove ($string, $from, $to) { return move($string, $to, $from) }

sub rotate_positional ($string, $letter) {
   my $index = index $string, $letter;
   $index = 1 + $index + ($index >= 4 ? 1 : 0);
   return rotate($string, right => $index);
}

sub derotate_positional ($string, $letter) {
   my $candidate = $string;
   for (1 .. length $string) {
      my $rotated = rotate_positional($candidate, $letter);
      return $candidate if $rotated eq $string;
      $candidate = rotate($candidate, left => 1);
   }
   die 'wtf?!?';
}
