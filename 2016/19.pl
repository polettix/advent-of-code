#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
$|++;

my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
open my $fh, '<', $filename;
while (<$fh>) {
   chomp;
   if (my ($from, $to) = m{(\d+)-(\d+)}ms) {
      for my $n ($from .. $to) {
         #say $n, ' -> ', josephus_bf($n), '~', josephus_ternary($n);
         printf "%3d -> %3d~%3d\n", $n, josephus_bf($n), josephus_dynamic($n);
      }
   }
   else {
      say josephus($_);
      say josephus_ternary($_);
      say josephus_part2_dynamic($_);
   }
}
close $fh;

sub josephus ($n) {
   my $p2 = 0x1;
   $p2 = ($p2 << 1) | 0x1 while $p2 < $n;
   my $k = $n & ($p2 >> 1);
   return $k << 1 | 0x1;
}

sub josephus_bf ($n) {
   my @slots = 1 .. $n;
   while ((my $N = @slots) > 1) {
      my $opponent = $N % 1 ? ($N - 1) / 2 : $N / 2;
      splice @slots, $opponent, 1;
      push @slots, shift @slots;
   }
   return $slots[0];
}

sub josephus_ternary ($n) {
   my $u3 = 3 ** int(log($n) / log(3));
   return $n if $n == $u3;
   my $threshold = int($u3 * 2);
   return $n - $u3 if $n <= $threshold;
   return ($n - $u3) + ($n - $threshold);
}

sub josephus_dynamic ($n) {
   my $i  = 1;
   my $in = 2;
   my $k  = 1;
   while ($in < $n) {
      ++$in;
      $k = int($in / 2);
      #++$k if $in % 2 == 0;
      $i = $i < $k      ? $i + 1
         : $i < $in - 1 ? $i + 2
         :                 1;
   }
   return $i;
}

sub josephus_part2_dynamic ($N) {
   my $i = 1;
   my $n = 2;
   my $k = 1;
   while ($n < $N) {
      ++$n;
      $k = int($n / 2);
      $i = $i < $k     ? $i + 1
         : $i < $n - 1 ? $i + 2
         :                 1;
   }
   return $i;
}
