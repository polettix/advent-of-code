#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
$|++;

my ($offset, $tx, $ty);

my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
open my $fh, '<', $filename;
($offset, $tx, $ty) = split m{\s+}mxs, scalar readline $fh;
close $fh;

my $outcome = astar(
   start => [1, 1],
   goal => [$tx, $ty],
   distance => sub { 1 },
   identifier => sub ($p) { join ',', $p->@[0, 1] },
   heuristic => sub ($p, $q) { abs($p->[0] - $q->[0]) + abs($p->[1] - $q->[1]) },
   successors => sub ($p) { return neighbors_for($p->@[0, 1], $offset) },
);
say scalar($outcome->@*) - 1;

for my $y (0 .. 6) {
   for my $x (0 .. 9) {
      print is_open($x, $y, $offset) ? '.' : '#';
   }
   say '';
}

say upto(50, $offset);

sub upto ($limit, $offset) {
   my %marked = ('1,1' => 1);
   my @queue = ([1, 1, $limit]);
   while (@queue) {
      my ($x, $y, $l) = shift(@queue)->@*;
      next unless $l--;
      for my $n (neighbors_for($x, $y, $offset)) {
         my ($X, $Y) = $n->@[0, 1];
         next if $marked{"$X,$Y"}++;
         next unless $l > 0;
         push @queue, [$X, $Y, $l];
      }
   }
   return scalar keys %marked;
}

sub neighbors_for ($x, $y, $offset) {
   my @retval;
   for my $dimension (0, 1) {
      for my $delta (-1, 1) {
         my @c = ($x, $y);
         $c[$dimension] += $delta;
         next if $c[$dimension] < 0;
         push @retval, \@c if is_open(@c, $offset);
      }
   }
   return @retval;
}

sub is_open ($X, $Y, $offset) {
   my $v = ($X + $Y) ** 2 + 3 * $X + $Y + $offset;
   my $is_even = 1;
   while ($v) {
      $is_even = ! $is_even if $v & 0x1;
      $v >>= 1;
   }
   return $is_even;
}

sub astar {
   my %args = (@_ && ref($_[0])) ? %{$_[0]} : @_;
   my @reqs = qw< start goal distance successors >;
   exists($args{$_}) || die "missing parameter '$_'" for @reqs;
   my ($start, $goal, $dist, $succs) = @args{@reqs};
   my $h     = $args{heuristic}  || $dist;
   my $id_of = $args{identifier} || sub { return "$_[0]" };

   my ($id, $gid) = ($id_of->($start), $id_of->($goal));
   my %node_for = ($id => {value => $start, g => 0});
   my $queue = bless ['-', {id => $id, f => 0}], __PACKAGE__;

   while (!$queue->_is_empty) {
      my $cid = $queue->_dequeue->{id};
      my $cx  = $node_for{$cid};
      next if $cx->{visited}++;

      my $cv = $cx->{value};
      return __unroll($cx, \%node_for) if $cid eq $gid;

      for my $sv ($succs->($cv)) {
         my $sid = $id_of->($sv);
         my $sx = $node_for{$sid} ||= {value => $sv};
         next if $sx->{visited};
         my $g = $cx->{g} + $dist->($cv, $sv);
         next if defined($sx->{g}) && ($g >= $sx->{g});
         @{$sx}{qw< p g >} = ($cid, $g);    # p: id of best "previous"
         $queue->_enqueue({id => $sid, f => $g + $h->($sv, $goal)});
      } ## end for my $sv ($succs->($cv...))
   } ## end while (!$queue->_is_empty)
   return;
} ## end sub astar

sub _dequeue {                              # includes "sink"
   my ($k, $self) = (1, @_);
   my $r = ($#$self > 1) ? (splice @$self, 1, 1, pop @$self) : pop @$self;
   while ((my $j = $k * 2) <= $#$self) {
      ++$j if ($j < $#$self) && ($self->[$j + 1]{f} < $self->[$j]{f});
      last if $self->[$k]{f} < $self->[$j]{f};
      (@{$self}[$j, $k], $k) = (@{$self}[$k, $j], $j);
   }
   return $r;
} ## end sub _dequeue

sub _enqueue {                              # includes "swim"
   my ($self, $node) = @_;
   push @$self, $node;
   my $k = $#$self;
   (@{$self}[$k / 2, $k], $k) = (@{$self}[$k, $k / 2], int($k / 2))
     while ($k > 1) && ($self->[$k]{f} < $self->[$k / 2]{f});
} ## end sub _enqueue

sub _is_empty { return !$#{$_[0]} }

sub __unroll {    # unroll the path from start to goal
   my ($node, $node_for, @path) = ($_[0], $_[1], $_[0]{value});
   while (defined(my $p = $node->{p})) {
      $node = $node_for->{$p};
      unshift @path, $node->{value};
   }
   return wantarray ? @path : \@path;
} ## end sub __unroll
