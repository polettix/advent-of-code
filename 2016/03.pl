#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
$|++;

my @triangulars;
my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
open my $fh, '<', $filename;
while (<$fh>) {
   s{\A\s+|\s+\z}{}gmxs;
   push @triangulars, [split m{\s+}mxs];
}
close $fh;

{
   my $count1 = 0;
   for my $triangular (@triangulars) {
      ++$count1 if is_triangle($triangular->@*);
   }
   say 'part1 => ', $count1;

   my $count2 = 0;
   while (@triangulars) {
      my ($t1, $t2, $t3) = splice @triangulars, 0, 3;
      while ($t1->@*) {
         ++$count2 if is_triangle(map {shift $_->@*} ($t1, $t2, $t3));
      }
   }
   say 'part2 => ', $count2;
}

sub is_triangle ($x, $y, $z) {
   return ($x + $y > $z) && ($y + $z > $x) && ($z + $x > $y);
}
