#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
$|++;

my $count1 = 0;
my $count2 = 0;

my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
open my $fh, '<', $filename;
INPUT:
while (<$fh>) {
   my $has_abba = 0;
   my $does_not_have_abba = 0;
   my %aba_flag;
   my %bab_flag;
   my $has_aba_bab = 0;
   my @chunks = split m{[\[\]\s]};
   for my $i (0 .. $#chunks) {
      my @abas = get_abas($chunks[$i]);
      for my $xyx (@abas) {
         my $yxy = substr($xyx, 1, 2) . substr($xyx, 1, 1);
         if ($i % 2) {
            $has_aba_bab = 1 if $aba_flag{$yxy};
            $bab_flag{$xyx}++;
         }
         else {
            $has_aba_bab = 1 if $bab_flag{$yxy};
            $aba_flag{$xyx}++;
         }
      }
      next unless has_abba($chunks[$i]);
      if ($i % 2) { $does_not_have_abba = 1 }
      else        { $has_abba = 1 }
   }
   ++$count1 if $has_abba && ! $does_not_have_abba;
   ++$count2 if $has_aba_bab;
}
close $fh;

say 'part1 => ', $count1;
say 'part2 => ', $count2;

sub has_abba ($string) {
   my @string = split m{}mxs, $string;
   while (@string >= 4) {
      my $c = shift @string;
      next if $c eq $string[0];
      next unless $c eq $string[2];
      next unless $string[0] eq $string[1];
      return 1;
   }
   return 0;
}

sub get_abas ($string) {
   my ($pp, $p, @rest) = split m{}mxs, $string;
   return map {
      my @retval = ($pp eq $_ && $pp ne $p) ? ("$pp$p$_") : ();
      ($pp, $p) = ($p, $_);
      @retval;
   } @rest;
}
