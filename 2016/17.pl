#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper;
$Data::Dumper::Indent = 1;
use Storable 'dclone';
use Digest::MD5 'md5_hex';
use Carp 'confess';
$|++;

my $seed;

my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
open my $fh, '<', $filename;
while (<$fh>) {
   chomp;
   my $goal = {x => 3, y => 3};
   my $path = astar(
      start      => {x => 0, y => 0, string => $_},
      goal       => $goal,
      distance   => \&manhattan_distance,
      successors => \&successors_for,
      heuristic  => \&manhattan_distance,
      identifier => sub ($P) {
         return '*' if $P->{x} == $goal->{x} && $P->{y} == $goal->{y};
         return $P->{string};
      },
   );
   say substr $path->[-1]{string}, length($_);

   my $longest = complete_visit({x => 0, y => 0, string => $_}, $goal);
   say length($longest);
} ## end while (<$fh>)
close $fh;

sub manhattan_distance ($P, $Q) {
   return abs($P->{x} - $Q->{x}) + abs($P->{y} - $Q->{y});
}

sub successors_for ($P) {
   my ($x, $y, $string) = $P->@{qw< x y string >};
   my @hash_stuff = split m{}mxs, substr md5_hex($string), 0, 4;
   my @retval;
   for my $alt (
      [$x,     $y - 1, 'U', $hash_stuff[0]],
      [$x,     $y + 1, 'D', $hash_stuff[1]],
      [$x - 1, $y,     'L', $hash_stuff[2]],
      [$x + 1, $y,     'R', $hash_stuff[3]],
     )
   {
      my ($X, $Y, $c, $d) = $alt->@*;
      next if $X < 0 || $X > 3 || $Y < 0 || $Y > 3 || $d !~ m{[b-f]}mxs;
      push @retval, { x => $X, y => $Y, string => $string . $c };
   } ## end for my $alt ([$x - 1, $y...])
   return @retval;
} ## end sub successors_for ($P)

sub complete_visit ($start, $goal) {
   my @queue = ($start);
   my $longest = '';
   while (@queue) {
      my $node = shift @queue;
      if ($node->{x} == $goal->{x} && $node->{y} == $goal->{y}) {
         $longest = $node->{string} if length($longest) < length($node->{string});
      }
      else {
         push @queue, successors_for($node);
      }
   }
   return substr $longest, length($start->{string});
}

sub astar {
   my %args = (@_ && ref($_[0])) ? %{$_[0]} : @_;
   my @reqs = qw< start goal distance successors >;
   exists($args{$_}) || die "missing parameter '$_'" for @reqs;
   my ($start, $goal, $dist, $succs) = @args{@reqs};
   my $h     = $args{heuristic}  || $dist;
   my $id_of = $args{identifier} || sub { return "$_[0]" };

   my ($id, $gid) = ($id_of->($start), $id_of->($goal));
   my %node_for = ($id => {value => $start, g => 0});
   my $queue = bless ['-', {id => $id, f => 0}], __PACKAGE__;

   while (!$queue->_is_empty) {
      my $cid = $queue->_dequeue->{id};
      my $cx  = $node_for{$cid};
      next if $cx->{visited}++;

      my $cv = $cx->{value};
      return __unroll($cx, \%node_for) if $cid eq $gid;

      for my $sv ($succs->($cv)) {
         my $sid = $id_of->($sv);
         my $sx = $node_for{$sid} ||= {value => $sv};
         next if $sx->{visited};
         my $g = $cx->{g} + $dist->($cv, $sv);
         next if defined($sx->{g}) && ($g >= $sx->{g});
         @{$sx}{qw< p g >} = ($cid, $g);    # p: id of best "previous"
         $queue->_enqueue({id => $sid, f => $g + $h->($sv, $goal)});
      } ## end for my $sv ($succs->($cv...))
   } ## end while (!$queue->_is_empty)

   return;
} ## end sub astar

sub _dequeue {                              # includes "sink"
   my ($k, $self) = (1, @_);
   my $r = ($#$self > 1) ? (splice @$self, 1, 1, pop @$self) : pop @$self;
   while ((my $j = $k * 2) <= $#$self) {
      ++$j if ($j < $#$self) && ($self->[$j + 1]{f} < $self->[$j]{f});
      last if $self->[$k]{f} < $self->[$j]{f};
      (@{$self}[$j, $k], $k) = (@{$self}[$k, $j], $j);
   }
   return $r;
} ## end sub _dequeue

sub _enqueue {                              # includes "swim"
   my ($self, $node) = @_;
   push @$self, $node;
   my $k = $#$self;
   (@{$self}[$k / 2, $k], $k) = (@{$self}[$k, $k / 2], int($k / 2))
     while ($k > 1) && ($self->[$k]{f} < $self->[$k / 2]{f});
} ## end sub _enqueue

sub _is_empty { return !$#{$_[0]} }

sub __unroll {    # unroll the path from start to goal
   my ($node, $node_for, @path) = ($_[0], $_[1], $_[0]{value});
   while (defined(my $p = $node->{p})) {
      $node = $node_for->{$p};
      unshift @path, $node->{value};
   }
   return wantarray ? @path : \@path;
} ## end sub __unroll
