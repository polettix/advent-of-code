#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
use List::Util qw< min max >;
$|++;

my %data_for;
my ($max_x, $max_y) = (-1, -1);
my %count_for;
my %used_for;
my %perc_for;

my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
open my $fh, '<', $filename;
scalar readline $fh for 1 .. 2;
while (<$fh>) {
   my ($id, $x, $y, $size, $used, $avail, $pused) = m{
      /dev/grid/node-(x(\d+)-y(\d+)) \s+ (\d+)T \s+ (\d+)T \s+ (\d+)T \s+ (\d+)%
   }mxs or die $_;
   $max_x = $x if $x > $max_x;
   $max_y = $y if $y > $max_y;
   $count_for{$size}++;
   $used_for{$used}++;
   $perc_for{$pused}++;
   $data_for{$id} = {
      id => $id,
      x => $x,
      y => $y,
      size => $size,
      used => $used,
      available => $avail,
      used_percent => $pused,
   };
}
close $fh;

my @nodes_by_availability = reverse sort {$a->{available} <=> $b->{available}} values %data_for;
my @nodes_by_usage        = reverse sort {$a->{used} <=> $b->{used}} grep {$_->{used}} values %data_for;

my $n_viable_pairs = 0;
my ($Ai, $Ui) = (0, 0);
my %can_host;
my %can_be_hosted;
while ($Ai <= $#nodes_by_availability && $Ui <= $#nodes_by_usage) {
   my $A = $nodes_by_availability[$Ai];
   my $U = $nodes_by_usage[$Ui];
   say "receiver $A->{id} ($A->{available}) vs donor $U->{id} ($U->{used})";
   if ($A->{available} >= $U->{used}) {
      $n_viable_pairs += @nodes_by_usage - $Ui;
      ++$Ai;
      say "--> up to $n_viable_pairs";
      my %hostable = map {$nodes_by_usage[$_]{id} => 1} $Ui .. $#nodes_by_usage;
      $can_host{$A->{id}} = {%hostable};
      %can_be_hosted = (%can_be_hosted, %hostable);
   }
   else {
      say '--> nope';
      ++$Ui;
   }
}

# remove "self-"viability
for my $node (@nodes_by_availability) {
   $n_viable_pairs-- if $node->{used} > 0 && $node->{available} >= $node->{used};
}

say 'part1 => ', $n_viable_pairs;

say Dumper [\%can_host, \%can_be_hosted];

my $min_size = min(0, keys %count_for);
my $max_size = max(100, keys %count_for);
say "$min_size $max_size";

for my $size ($min_size .. $max_size) {
   my $has_total = $count_for{$size} ? 'T' : ' ';
   my $has_used  = $used_for{$size} ? 'U' : ' ';
   my $has_pused = $perc_for{$size} ? '%' : ' ';
   printf "%3d %s %s %s\n", $size, $has_total, $has_used, $has_pused;
}

my ($host) = keys %can_host;
my $movability_threshold = $data_for{$host}{size};
say "$host movability_threshold = $movability_threshold";

say '    ', join('', 0 .. 9) x 4;
for my $y (0 .. $max_y) {
   printf '%3d ', $y;
   for my $x (0 .. $max_x) {
      my $id = "x$x-y$y";
      my $used = $data_for{$id}{used};
      my $char = $used == 0 ? '_'
         : $used <= $movability_threshold ? '.'
         : '#';
      print $char;
   }
   print "\n";
}
