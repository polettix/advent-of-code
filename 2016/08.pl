#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
use List::Util 'sum';
$|++;

my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
open my $fh, '<', $filename;

my ($ROWS, $COLS) = split m{\D+}mxs, scalar <$fh>;

my @grid1 = map { [(0) x $COLS] } 1 .. $ROWS;
while (<$fh>) {
   if (my ($w, $h) = m{rect \s+ (\d+)x(\d+)}mxs) {
      for my $i (0 .. $h - 1) {
         for my $j (0 .. $w - 1) {
            $grid1[$i][$j] = 1;
         }
      }
   }
   elsif (my ($col, $ccount) = m{rotate\ column \s+ x=(\d+) .*? (\d+)}mxs) {
      for (1 .. $ccount) {
         my $previous = $grid1[-1][$col];
         ($_->[$col], $previous) = ($previous, $_->[$col]) for @grid1;
      }
   }
   elsif (my ($row, $rcount) = m{rotate\ row \s+ y=(\d+) .*? (\d+)}mxs) {
      my $r = $grid1[$row];
      unshift $r->@*, splice $r->@*, -$rcount, $rcount;
   }
   else { die $_ }
   print;
   print_display(@grid1);
   say total(@grid1);
   say '';
}
close $fh;

say 'part1 => ', sum(map {$_->@*} @grid1);

sub print_display (@display) {
   for (@display) {
      say map { $_ ? '#' : '.' } $_->@*;
   }
}

sub total { sum map {$_->@*} @_ }
