#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
use List::Util 'shuffle';
$|++;

my $filename = shift || basename(__FILE__) =~ s{\.pl\z}{.input}rmxs;
open my $fh, '<', $filename;
my ($boss_hp) = scalar(<$fh>) =~ m{(\d+)}mxs;
my ($boss_damage) = scalar(<$fh>) =~ m{(\d+)}mxs;
say "boss_hp<$boss_hp> boss_damage<$boss_damage>";

my $min;
for (1 .. 1_000_000) {
   my $mana = random_fight($boss_hp, $boss_damage) // next;
   $min = $mana if (! defined $min) || ($mana < $min);
}
say "part1<$min>";

$min = undef;
for (1 .. 1_000_000) {
   my $mana = random_fight($boss_hp, $boss_damage, 1) // next;
   $min = $mana if (! defined $min) || ($mana < $min);
}
say "part2<$min>";


sub random_fight ($boss_hp, $boss_damage, $turn_damage = 0) {
   my $hp = 50;
   my $mana = 500;
   my $spent_mana = 0;
   my ($shield, $poison, $recharge);
   my %cost_of = (
      missile  => 53,
      drain    => 73,
      shield   => 113,
      poison   => 173,
      recharge => 229,
   );
   my $boss_turn = 0;

   while ('necessary') {

      # start-of-turn stuff
      $hp -= $turn_damage;
      return if $hp <= 0;
      if ($poison)   { --$poison; $boss_hp -= 3 }
      return $spent_mana if $boss_hp <= 0;
      if ($shield)   { --$shield }
      if ($recharge) { --$recharge; $mana += 101 }

      if ($boss_turn) {
         $hp -= $shield ? ($boss_damage - 7) : $boss_damage;
         return if $hp <= 0;
      }
      else {
         my $run_spell;
         for my $spell (shuffle keys %cost_of) {
            next if ($spell eq 'shield' && $shield)
               || ($spell eq 'poison' && $poison)
               || ($spell eq 'recharge' && $recharge);
            my $cost = $cost_of{$spell};
            next unless $mana >= $cost;
            $run_spell = 1;
            $mana -= $cost;
            $spent_mana += $cost;
            if ($spell eq 'missile')     { $boss_hp -= 4}
            elsif ($spell eq 'drain')    { $boss_hp -= 2; $hp += 2 }
            elsif ($spell eq 'shield')   { $shield = 6 }
            elsif ($spell eq 'poison')   { $poison = 6 }
            elsif ($spell eq 'recharge') { $recharge = 5 }
            else { die "WTF?!? <$spell>" }
            return $spent_mana if $boss_hp <= 0;
            last;
         }
         return unless $run_spell;
      }
      $boss_turn = !$boss_turn;
   }
}
