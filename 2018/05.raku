#!/usr/bin/env raku
use v6;

sub MAIN ($filename = Nil) {
   my $inputs = get-inputs($filename // $?FILE.subst(/\.raku$/, '.tmp'));
   my ($part1, $part2) = solve($inputs);

   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";
   put "part1 $highlight$part1$reset";
   put "part2 $highlight$part2$reset";
}

sub get-inputs ($filename) {
   my $input = $filename.IO.basename.IO.lines[0];
   return $input;
} ## end sub get_inputs ($filename = undef)

sub solve ($inputs) {
   my $part1 = %*ENV<RX> ?? &part1_rx !! &part1;
   return ($part1($inputs), part2($inputs));
}

sub part1_matcher () {
   my $allpairs =('a' .. 'z').map({ .lc ~ .uc, .uc ~ .lc }).flat.join('|');
   return rx{<$allpairs>};
}

sub part1_rx ($inputs is copy) {
   state $matcher = part1_matcher();
   Nil while $inputs ~~ s:g/$matcher//;
   return $inputs.chars;
}

sub part1 ($inputs is copy) {
   my $changed = -1;
   while ($changed && $inputs.chars) {
      $changed = 0;
      my $current = $inputs.substr(0, 1);
      my $i = 0;
      while $i < $inputs.chars - 1 {
         my $succ = $inputs.substr($i + 1, 1);
         if ($current ne $succ && lc($current) eq lc($succ)) {
            ++$changed;
            $inputs.substr-rw($i, 2) = '';
            $current = substr($i, 1) if $i < $inputs.chars;
         }
         else {
            $current = $succ;
            ++$i;
         }
      }
   }
   return $inputs.chars;
}

sub part2 ($inputs) {
   return 'part2'
}
