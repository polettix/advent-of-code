#!/usr/bin/env raku
use v6;

sub MAIN ($filename = Nil) {
   my $inputs = get-inputs($filename // $?FILE.subst(/\.raku$/, '.tmp'));
   my ($part1, $part2) = solve($inputs);

   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";
   put "part1 $highlight$part1$reset";
   put "part2 $highlight$part2$reset";
}

sub get-inputs ($filename) {
   my @inputs = $filename.IO.basename.IO.lines
      .map: *.comb(/\d+/);
   return @inputs;
} ## end sub get_inputs ($filename = undef)

sub solve ($inputs) {
   return (part1($inputs), part2($inputs));
}

sub percentager ($total) {
   return Nil if $total < 10000;
   return sub ($n) {
      return unless $n %% 10000;
      note sprintf '%.1f', 100 * $n / $total;
   };
}

sub percentage ($n, $total) {
   return unless $total >= 10_000;

}

sub part1-game ($n-players, $n-stones) {
   return 0 if $n-stones < 23;
   my %scores;
   my @board = 0, 2, 1;
   my $current-id = 1;
   my $current-player = 2;
   my $percentager = percentager($n-stones);
   for 3 .. $n-stones -> $stone {
      $current-player = 1 + $current-player % $n-players;
      if $stone %% 23 {
         $current-id = ($current-id + @board.elems - 7) % @board;
         %scores{$current-player} += $stone + @board.splice($current-id, 1).sum;
      }
      else {
         $current-id += 2;
         $current-id %= @board if $current-id > @board.elems;
         @board.splice($current-id, 0, $stone);
      }
      #put "[$current-player] <$current-id> {@board.join: ' '}";
      $percentager($stone) if $percentager;
   }
   return %scores.values.max;
}

sub part1 ($inputs) {
   for @$inputs -> ($players, $stones, $result = -1) {
      put "players<$players> stones<$stones> expected<$result>";
      my $got = part1-game($players, $stones);
      put "got: $got (/$result delta<{$got - $result}>)";
   }
   return 'part1'
}

sub part2 ($inputs) {
   return 'part2'
}
