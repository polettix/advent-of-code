#!/usr/bin/env raku
use v6;

sub MAIN ($filename = Nil) {
   my $inputs = get-inputs($filename // $?FILE.subst(/\.raku$/, '.tmp'));
   my ($part1, $part2) = solve($inputs);

   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";
   put "part1 $highlight$part1$reset";
   put "part2 $highlight$part2$reset";
}

sub get-inputs ($filename) {
   my @input = $filename.IO.basename
      .IO.lines.map({ $^a.split: /\s+/ }).flat;
   my $i = 0;
   sub parse-tree () {
      my $n-children = @input[$i++];
      my $n-meta = @input[$i++];
      my @children = (1 .. $n-children).map: { parse-tree() };
      my @meta = @input[$i ..^ $i + $n-meta];
      $i += $n-meta;
      return (children => @children, meta => @meta).hash;
   }
   return parse-tree();
} ## end sub get_inputs ($filename = undef)

sub solve ($inputs) {
   return (part1($inputs), part2($inputs));
}

sub traverse ($tree, &cb) {
   traverse($_, &cb) for $tree<children>.Slip;
   &cb($tree);
}

sub part1 ($inputs) {
   my $sum = 0;
   traverse($inputs, { $sum += $^a<meta>.sum });
   return $sum;
}

sub part2 ($inputs) {
   traverse($inputs, -> $n {
      my $n-children = $n<children>.elems;
      if ($n-children > 0) {
         $n<value> = 0;
         for $n<meta>.Slip -> $idx {
            next unless 1 <= $idx <= $n-children;
            $n<value> += $n<children>[$idx - 1]<value>;
         }
      }
      else {
         $n<value> = $n<meta>.sum;
      }
   });
   return $inputs<value>;
}
