#!/usr/bin/env raku
use v6;

sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.sample')) {
   return solve($filename);
}

sub solve ($filename) {
   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";

   my $inputs = get-inputs($filename);
   my ($start, $elapsed);

   $start = now;
   my $part1 = part1($inputs);
   $elapsed = now - $start;
   put "part1 ($elapsed) $highlight$part1$reset";

   $start = now;
   my $part2 = part2($inputs);
   $elapsed = now - $start;
   put "part2 ($elapsed) $highlight$part2$reset";

   return 0;
}

sub get-inputs ($filename) {
   my $fh = $filename.IO.open(:r :chomp);
   my $state = $fh.lines(1).split(/\:\s+/)[1].trans('.#' => '01').comb(/<[01]>/);
   $fh.lines(1); # remove this
   my @rules = '0' xx 32;
   for $fh.lines -> $rule {
      my ($from, $to) = $rule.split(' => ')».trans('.#' => '01');
      @rules[$from.parse-base(2)] = $to;
   }
   return { state => $state.Array, :@rules, start => 0 };
}

sub tick ($tunnel) {
   die 'not prepared for this' if $tunnel<rules>[0] ne '0';
   my @new;
   my $idx = 0;
   for (|$tunnel<state>, '0' xx 4).flat -> $c {
      $idx = ($idx * 2 + $c) +& 0x1F;
      @new.push: $tunnel<rules>[$idx]
   }
   @new.pop while @new && @new[*-1] eq '0';
   my $start = $tunnel<start> - 2;
   while @new && @new[0] eq '0' {
      @new.shift;
      ++$start;
   }
   return {
      state => @new,
      rules => $tunnel<rules>,
      :$start,
   };
}

sub count ($tunnel) {
   my $position = $tunnel<start>;
   my $sum = 0;
   for |$tunnel<state> -> $c {
      $sum += $position if $c eq '1';
      ++$position;
   }
   return $sum;
}

sub part1 ($inputs is copy) {
   "0. $inputs<state>   $inputs<start>".note if %*ENV<DEBUG>;
   for 1 .. 20 {
      $inputs = tick($inputs);
      "$_. $inputs<state>   $inputs<start>".note if %*ENV<DEBUG>;
   }
   return count($inputs);
}

sub part2 ($inputs is copy) {
   my $n = 50000000000;
   my $k = 0;
   my ($count, $delta);
   loop {
      my $new = tick($inputs);
      ++$k;
      if ($new<state>.join('') eq $inputs<state>.join('')) {
         "stabilized after $k iterations".note if %*ENV<DEBUG>;
         $count = count($new);
         $delta = $count - count($inputs);
         last;
      }
      $inputs = $new;
   }
   return $count + $delta * ($n - $k);
}
