#!/usr/bin/env raku
use v6;

sub MAIN ($input = '03.tmp') {
   my $fh = $input.IO.open;
   my %fabric;
   my %double is SetHash;
   my %free is SetHash;
   for $fh.lines -> $line {
      my ($id, $left, $top, $width, $height) = $line.comb(/\d+/);
      %free.set($id);
      for +$left ..^ $left + $width -> $h {
         for +$top ..^ $top + $height -> $v {
            my $key = "$h:$v";
            if (%fabric{$key}:exists) {
               %double.set($key);
               %free.unset: ($id, %fabric{$key});
            }
            else {
               %fabric{$key} = $id;
            }
         }
      }
   }
   say %double.elems;
   put %free.keys;
}
