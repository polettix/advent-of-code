#!/usr/bin/env raku
use v6;

sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.sample')) {
   return solve($filename) if $filename.IO.e;
}

sub solve ($filename) {
   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";

   class TransportSystem { ... };
   my $ts = TransportSystem.new($filename.IO);
   my ($start, $elapsed);

   $start = now;
   my $part1 = part1($ts);
   $elapsed = now - $start;
   put "part1 ($elapsed) $highlight$part1$reset";

   $start = now;
   my $part2 = part2($ts);
   $elapsed = now - $start;
   put "part2 ($elapsed) $highlight$part2$reset";

   return 0;
}

sub part1 ($ts) {
   loop {
      my $crashes = $ts.tick;
      return $crashes[0] if $crashes.elems;
   }
}

sub part2 ($ts) {
   $ts.tick while $ts.n-carts > 1;
   return $ts.carts[0];
}

class TransportSystem {
   has @!map    is built;
   has %!carts  is built;
   has $!n-cols is built;

   method n-carts () { %!carts.elems }
   method carts ()   { %!carts.keys.map({self!key2output($_)}) }

   method new ($input) {
      my (@map, %carts, $n-cols);
      for $input.lines -> $line {
         my $row = $line.comb(/./).Array;
         $n-cols //= $row.elems;
         @map.push: $row;
         for ^$row -> $i {
            next unless $row[$i] ~~ any('<', '>', '^', 'v');
            %carts{$n-cols * @map.end + $i} = {direction => $row[$i], rr => 0};
            $row[$i] = $row[$i] ~~ any(< ^ v >) ?? '|' !! '-';
         }
      }
      self.bless(:@map, :%carts, :$n-cols);
   }

   method !key2pos ($key) { ($key.Int / $!n-cols).Int, $key.Int % $!n-cols }
   method !pos2key ($rid, $cid) { $rid * $!n-cols + $cid }
   method !key2output ($key) { self!key2pos($key).reverse.join(',') }

   method !tick-cart ($key is copy) {
      my $cart = %!carts{$key}:delete;
      my ($rid, $cid) = self!key2pos($key);

      # Advance to new position based on current direction
      given $cart<direction> {
         when '<' { --$cid }
         when '>' { ++$cid }
         when '^' { --$rid }
         when 'v' { ++$rid }
      }
      $key = self!pos2key($rid, $cid);

      # Assess collision in new position, and return in case
      (my $collision, %!carts{$key}) = %!carts{$key}:exists, $cart;
      if $collision {
         $cart<direction> = 'X';
         return $key;
      }

      # Update direction if needed, preparing for next round
      my $track = @!map[$rid][$cid];
      my $situation = $track ~ $cart<direction> ~ $cart<rr>;
      given $situation {
         when /^ \\  '>' /   { $cart<direction> = 'v' }
         when /^ \\  '^' /   { $cart<direction> = '<' }
         when /^ \\  '<' /   { $cart<direction> = '^' }
         when /^ \\  'v' /   { $cart<direction> = '>' }

         when /^ '/' '>' /   { $cart<direction> = '^' }
         when /^ '/' '^' /   { $cart<direction> = '>' }
         when /^ '/' '<' /   { $cart<direction> = 'v' }
         when /^ '/' 'v' /   { $cart<direction> = '<' }

         when /^ '+' '>' 0 / { $cart<direction> = '^'; $cart<rr> = 1 }
         when /^ '+' '^' 0 / { $cart<direction> = '<'; $cart<rr> = 1 }
         when /^ '+' '<' 0 / { $cart<direction> = 'v'; $cart<rr> = 1 }
         when /^ '+' 'v' 0 / { $cart<direction> = '>'; $cart<rr> = 1 }

         when /^ '+'  .  1 / {                         $cart<rr> = 2 }

         when /^ '+' '>' 2 / { $cart<direction> = 'v'; $cart<rr> = 0 }
         when /^ '+' '^' 2 / { $cart<direction> = '>'; $cart<rr> = 0 }
         when /^ '+' '<' 2 / { $cart<direction> = '^'; $cart<rr> = 0 }
         when /^ '+' 'v' 2 / { $cart<direction> = '<'; $cart<rr> = 0 }
      }

      return Nil;
   }

   method tick () {
      my @sorted = %!carts.keys.sort({+$^a <=> +$^b});
      my @crashes;
      for @sorted -> $key {
         next unless %!carts{$key}:exists;
         my $outcome = self!tick-cart($key);
         @crashes.push: $outcome if defined $outcome;
      }

      # clean crashes up
      %!carts{$_}:delete for @crashes;

      return @crashes.map({self!key2output($_)}) if @crashes.elems;
   }
}
