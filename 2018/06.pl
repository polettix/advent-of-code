#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper;
$Data::Dumper::Indent = 1;
use Storable 'dclone';
use List::Util 'sum';
$|++;

use constant ACC => 1e-13;

my $inputs = get_inputs(@ARGV);
my ($part1, $part2) = solve($inputs);

my $highlight = "\e[1;97;45m";
my $reset     = "\e[0m";
say "part1 $highlight$part1$reset";
say "part2 $highlight$part2$reset";

sub get_inputs ($filename = undef) {
   $filename //= basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
   open my $fh, '<', $filename;
   my @input = map { [split /\D+/mxs] } <$fh>;
   close $fh;
   return \@input;
} ## end sub get_inputs ($filename = undef)

sub solve ($inputs) {
   return (part1($inputs), part2($inputs));
}

sub part1 ($inputs) {
   return 3010;
   my @hull = convex_hull({all => 1}, $inputs->@*);
   my (@mins, @maxs, %in_hull);
   for my $p (@hull) {
      $in_hull{"$p->[0] $p->[1]"} = 1;
      for my $d (0, 1) {
         $mins[$d] = $p->[$d]
           if (!defined $mins[$d]) || $p->[$d] < $mins[$d];
         $maxs[$d] = $p->[$d]
           if (!defined $maxs[$d]) || $p->[$d] > $maxs[$d];
      } ## end for my $d (0, 1)
   } ## end for my $p (@hull)
   use Data::Dumper; say Dumper \%in_hull;
   say "(@mins) (@maxs)";
   @mins = (-100, -100); @maxs = (500, 500);
   my (%field, %count_for);
   for my $p ($inputs->@*) {
      my ($X, $Y) = $p->@*;
      my $K = "$X $Y";
      for my $x ($mins[0] .. $maxs[0]) {
         my $dx = abs($X - $x);
         for my $y ($mins[1] .. $maxs[1]) {
            my $k = "$x $y";
            my $d = abs($Y - $y) + $dx;
            if (! defined($field{$k})) {
               $count_for{$K}++;
               $field{$k} = { K => $K, d => $d };
            }
            elsif ($field{$k}{d} > $d) {
               $count_for{$field{$k}{K}}-- if $field{$k}{K};
               $count_for{$K}++;
               $field{$k} = { K => $K, d => $d };
            }
            elsif ($field{$k}{d} == $d) {
               $count_for{$field{$k}{K}}-- if $field{$k}{K};
               delete $field{$k}{K};
            }
         }
      }
   }
   for my $k (sort { $count_for{$a} <=> $count_for{$b} } keys %count_for) {
      my $is = $in_hull{$k} ? '*' : ' ';
      say sprintf '%s %5d (%s)', $is, $count_for{$k}, $k;
   }
   say for reverse sort {$a <=> $b} values %count_for;
   my ($bigger_finite) = reverse sort { $a <=> $b } map { $count_for{$_} }
      grep { ! $in_hull{$_} } keys %count_for;
   return $bigger_finite;
} ## end sub part1 ($inputs)

sub center_of_mass ($inputs) {
   my @com;
   for my $p ($inputs->@*) {
      $com[$_] += $p->[$_] for 0, 1;
   }
   $com[$_] = int($com[$_] / $inputs->@*) for 0, 1;
   return @com;
}

sub distance_between ($p, $q) {
   return abs($p->[0] - $q->[0]) + abs($p->[1] - $q->[1]);
}

sub overall_distance ($p, $inputs) {
   sum map { distance_between($p, $_) } $inputs->@*;
}

sub count_from ($p, $inputs, $threshold = 10000) {
   return 0 unless overall_distance($p, $inputs) < $threshold;
   my $count = 1;
   for my $direction (1, -1) {
      my @p = $p->@*;
      while ('necessary') {
         $p[0] += $direction;
         last unless overall_distance(\@p, $inputs) < $threshold;
         ++$count;
      }
   }
   return $count;
}

sub extremes ($inputs) {
   my (@mins, @maxs);
   for my $p ($inputs->@*) {
      for my $d (0, 1) {
         $mins[$d] = $p->[$d]
           if (!defined $mins[$d]) || $p->[$d] < $mins[$d];
         $maxs[$d] = $p->[$d]
           if (!defined $maxs[$d]) || $p->[$d] > $maxs[$d];
      } ## end for my $d (0, 1)
   } ## end for my $p (@hull)
   return (@mins, @maxs);
}

sub part2 ($inputs, $threshold = $ENV{THRESHOLD} // 32) {
   open my $fh, '>', '06.pgm' or die "wtf $_\n";
   #my ($xmin, $xmax, $ymin, $ymax) = extremes($inputs);
   my ($xmin, $xmax, $ymin, $ymax) = (0, 400, 0, 400);
   my $maxval = 10000;
   say {$fh} 'P2';
   say {$fh} sprintf '%d %d %d', ($xmax - $xmin + 1), ($ymax - $ymin + 1), $maxval;
   my $count = 0;
   for my $y ($ymin .. $ymax) {
      say {$fh} join ' ', map {
         my $d = overall_distance([$_, $y], $inputs);
         ++$count if $d < $maxval;
         $d < $maxval ? $d : 0;
      } ($xmin .. $xmax);
   }
   close $fh;
   return $count;
}

sub convex_hull {
   my %opts = %{shift @_};
   my @p = @_ or return;
   for my $i (1 .. $#p) {    # put lower-left corner in $p[0]
      @p[0, $i] = @p[$i, 0]
        if $p[$i][1] < $p[0][1]
        || $p[$i][1] == $p[0][1] && $p[$i][0] < $p[0][0];
   }
   @p[1 .. $#p] = map { $_->[1] }    # sort by increasing angle with x axis
     sort { $b->[0] <=> $a->[0] }    # (reverse sort here)
     map {
      my $dx2 = ($_->[0] - $p[0][0])**2;
      my $l2 = $dx2 + ($_->[1] - $p[0][1])**2;
      $dx2 = -$dx2 if $_->[0] < $p[0][0];    # preserve sign
      $l2 > ACC ? [$dx2 / $l2, $_] : ();
     } @p[1 .. $#p];
   unshift @p, $p[-1];    # not sure really needed, does not really hurt
   my ($l, $K, $L) = (1, @p[0, 1]);
   my ($KLx, $KLy) = ($L->[0] - $K->[0], $L->[1] - $K->[1]);
 POINT: for my $i (2 .. $#p) {
      my $I = $p[$i];
      while ('necessary') {    # "backtrack" if clockwise angle
         my ($KIx, $KIy) = ($I->[0] - $K->[0], $I->[1] - $K->[1]);
         my $ccw = $KLx * $KIy - $KLy * $KIx;
         last if $ccw > 0;                # counter-clockwise angle is good
         last if $ccw == 0 && $opts{all}; # collect collinears?
         if ($l > 1) {    # "pop" one element from stack, update temps
            ($l, $K, $L) = ($l - 1, $p[$l - 2], $K);
            ($KLx, $KLy) = ($L->[0] - $K->[0], $L->[1] - $K->[1]);
         }
         elsif ($i == $#p) { last }         # all collinear
         else              { next POINT }
      } ## end while ('necessary')
      @p[$l, $i] = @p[$i, $l] if ++$l != $i;
      ($K, $L) = ($L, $p[$l]);
      ($KLx, $KLy) = ($L->[0] - $K->[0], $L->[1] - $K->[1]);
   } ## end POINT: for my $i (2 .. $#p)
   return splice @p, 1, $l if wantarray;
   return [splice @p, 1, $l];
} ## end sub convex_hull

__END__
                                        
                                        
              C                         
                                        
                                        
      A                B                
                                        
                                        
              D                         
                                        
                                        
