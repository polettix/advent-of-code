#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
$|++;

my $inputs = get_inputs(@ARGV);
my ($part1, $part2) = solve($inputs);

my $highlight = "\e[1;97;45m";
my $reset     = "\e[0m";
say "part1 $highlight$part1$reset";
say "part2 $highlight$part2$reset";

sub get_inputs ($filename = undef) {
   my %input;
   $filename //= basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
   open my $fh, '<', $filename;
   while (<$fh>) {
      my ($id, $ml, $mt, $w, $h) = m{
         \A \#(\d+) .*?
         (\d+),(\d+) .*?
         (\d+)x(\d+)
      }mxs;
      $input{$id} = {
         left   => $ml,
         top    => $mt,
         width  => $w,
         height => $h
      };
   }
   close $fh;
   return \%input;
} ## end sub get_inputs ($filename = undef)

sub solve ($inputs) {
   return (part1($inputs), part2($inputs));
}

sub part1 ($inputs) {
   my %fabric;
   my %double;
   for my $input (values $inputs->%*) {
      my ($left, $top, $width, $height) = $input->@{qw< left top width height >};
      for my $h ($left .. $left + $width - 1) {
         for my $v ($top .. $top + $height - 1) {
            my $key = "$h:$v";
            $double{$key} = 1 if $fabric{$key}++;
         }
      }
   }
   return scalar keys %double;
}

sub part2 ($inputs) {
   my %fabric;
   my %free;
   while (my ($id, $input) = each $inputs->%*) {
      my ($left, $top, $width, $height) = $input->@{qw< left top width height >};
      $free{$id} = 1;
      for my $h ($left .. $left + $width - 1) {
         for my $v ($top .. $top + $height - 1) {
            my $key = "$h:$v";
            if ($fabric{$key}) {
               delete $free{$id};
               delete $free{$fabric{$key}};
            }
            else {
               $fabric{$key} = $id;
            }
         }
      }
   }
   return keys %free;
}
