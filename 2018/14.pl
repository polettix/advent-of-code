#!/usr/bin/env perl
use v5.24;
use warnings;
use experimental 'signatures';
no warnings 'experimental::signatures';
use Time::HiRes 'time';

my $target = shift // '409551';
my $length = length($target);
my $recipes = '3710101';
my $elf1 = 4;
my $elf2 = 6;
my ($part1, $part2);
my $start = time();
while ((! defined $part1) || (! defined $part2)) {
   my $val1 = substr($recipes, $elf1, 1);
   my $val2 = substr($recipes, $elf2, 1);
   $recipes .= $val1 + $val2;
   my $n = length($recipes);
   if (! defined($part1) && $n >= $target + 10) {
      $part1 = substr($recipes, $target, 10);
      my $elapsed = sprintf '%.2f', time() - $start;
      say "part1 $part1 ($elapsed)";
   }
   $elf1 = ($elf1 + $val1 + 1) % $n;
   $elf2 = ($elf2 + $val2 + 1) % $n;
   next if defined $part2;
   my $index = 1 + index(substr($recipes, $n - 1 - $length), $target) or next;
   $part2 = length($recipes) - $length - 2 + $index;
   my $elapsed = sprintf '%.2f', time() - $start;
   say "part2 $part2 ($elapsed)";
}
