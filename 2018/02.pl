#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
$|++;

my $inputs = get_inputs(@ARGV);
my ($part1, $part2) = solve($inputs);

my $highlight = "\e[1;97;45m";
my $reset     = "\e[0m";
say "part1 $highlight$part1$reset";
say "part2 $highlight$part2$reset";

sub get_inputs ($filename = undef) {
   my @input;
   $filename //= basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
   open my $fh, '<', $filename;
   chomp(@input = <$fh>);
   close $fh;
   return \@input;
} ## end sub get_inputs ($filename = undef)

sub solve ($inputs) {
   return (part1($inputs), part2($inputs));
}

sub part1 ($inputs) {
   my ($count2, $count3) = (0, 0);
   for my $input ($inputs->@*) {
      my %count_for;
      $count_for{$_}++ for split m{}mxs, $input;
      $count2++ if grep { $_ == 2 } values %count_for;
      $count3++ if grep { $_ == 3 } values %count_for;
   }
   return $count2 * $count3;
}

sub part2 ($inputs) {
   for my $i (0 .. $inputs->$#* - 1) {
      my $A = $inputs->[$i];
      for my $j ($i + 1 .. $inputs->$#*) {
         my $B = $inputs->[$j];
         next if levenshtein($A, $B) > 1;
         for my $k (0 .. length($A) - 1) {
            if (substr($A, $k, 1) ne substr($B, $k, 1)) {
               substr $A, $k, 1, '';
               return $A;
            }
         }
      }
   }
   return 'part2'
}

sub levenshtein {
   my ($v, $s, $t) = ([0 .. length($_[0])], @_);
   for my $i (1 .. length($t)) {
      my $w = [$i];              # first "column" of full matrix
      for my $j (1 .. length($s)) {
         my ($D, $I, $S) = ($v->[$j] + 1, $w->[$j - 1] + 1, $v->[$j - 1]);
         $S++ if substr($s, $j - 1, 1) ne substr($t, $i - 1, 1);
         my $mDI = $I < $D ? $I : $D;    # min($D, $I);
         push @$w, ($S < $mDI ? $S : $mDI);    # min($S, min($D, $I))
      } ## end for my $j (1 .. length(...))
      $v = $w;    # "swap" and prepare for nest iteration
   } ## end for my $i (1 .. length(...))
   return $v->[-1];
} ## end sub levenshtein ($s, $t)
