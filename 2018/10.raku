#!/usr/bin/env raku
use v6;

sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.tmp')) {
   my $inputs = get-inputs($filename);
   solve($inputs);
}

sub get-inputs ($filename) {
   $filename.IO.lines.map(
      {
         my @values = .comb(/ \-? \d+ /)».Int;
         {p => [@values[0, 1]], v => [@values[2, 3]]};
      }
   ).Array;
}

sub tick ($inputs, $steps = 1) {
   $inputs.map(
      {
         my @v = $_<v>.Array «*» $steps;
         my $p = $_<p> «+» @v;
         hash(p => $p, v => $_<v>);
      }
   ).Array;
}

sub limits ($inputs) {
   my @min = (0, 1).map: { $inputs».[$_].min };
   my @max = (0, 1).map: { $inputs».[$_].max };
   return @min, @max;
}

sub render ($inputs, $i = 1) {
   my ($min, $max) = limits($inputs);
   my $delta = $max «-» $min;
   my $line = '.' x ($max[1 - $i] - $min[1 - $i] + 1);
   my @render = $line xx ($max[$i] - $min[$i] + 1);
   for @$inputs -> $ip {
      my $p = $ip «-» $min;
      my $idx = $i == 1 ?? $p[0] !! $delta[1] - $p[1];
      @render[$p[$i]].substr-rw($idx, 1) = '#';
   }
   return @render;
}

sub printout-plain ($render) { .map({ S:g/\./ / }).put for @$render }

sub printout-unicode ($render, :$set = '#') {
   state @char-for = ' ', "\c[LOWER HALF BLOCK]", "\c[UPPER HALF BLOCK]",
      "\c[FULL BLOCK]";
   for @$render -> $l1, $l2 {
      $l2 //= ' ' x $l1.chars;
      (0 ..^ $l1.chars).map( -> $i {
         my ($hi, $lo) = ($l1, $l2).map({ .substr($i, 1) eq $set ?? 1 !! 0 });
         @char-for[2 * $hi + $lo];
      }).join('').put;
   }
}

sub solve ($inputs is copy) {

   # calculate a "reasonable" jump ahead from scattering and speeds
   my ($min, $max) = limits($inputs».<p>);
   my ($vmin, $vmax) = limits($inputs».<v>);
   my $delta = $max «-» $min;
   my $vdelta = $vmax «-» $vmin;
   my $steps = ($delta «/» $vdelta).min.Int - 3;

   # advance that much in one big jump
   put "jumping ahead $steps steps";
   $inputs = tick($inputs, $steps);

   # now proceed tick by tick to find the most compact arrangement. Size
   # is measured by the half-perimeter of the bounding box
   my $previous-half-perimeter = Nil;
   while 'necessary' {
      my $pts = $inputs».<p>;
      my ($min, $max) = limits($pts);  # calculate bounding box
      my $half-perimeter = ($max «-» $min).sum;
      $previous-half-perimeter //= $half-perimeter + 1; # initialize
      last if $half-perimeter >= $previous-half-perimeter;

      # one more step ahead... save the half-perimeter for comparison
      $previous-half-perimeter = $half-perimeter;
      $inputs = tick($inputs);
      ++$steps;
      put "step ahead once more, $steps";
   }

   put "got past minimum half-perimeter, backtracking a bit...\n";

   # one-before-last was the minimum, go one step before it i.e. -2
   $inputs = tick($inputs, -2);
   $steps -= 2;

   # now print out the three frames, with the lowest one in the middle
   for 1 .. 3 {
      my $pts = $inputs».<p>;
      my @render = render($pts, 1);
      printout-unicode(@render);
      put "\n$steps {'_' x 10}\n\n";
      last if $_ == 3;
      $inputs = tick($inputs);
      ++$steps;
   }
}
