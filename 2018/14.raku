#!/usr/bin/env raku
use v6;

sub MAIN ($input = $?FILE.subst(/\.raku$/, '.sample')) {
   return solve($input);
}

sub solve ($input) {
   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";

   my $target = $input ~~ /^ \d+ $/ ?? $input !! $input.IO.words[0];
   my $start = now;

   my $length = $target.chars;
   my $recipes = '3710101245';
   my $elf1 = 3;
   my $elf2 = 6;
   my $threshold = 100000;
   my ($part1, $part2);
   while (! defined $part1) || (! defined $part2) {
      my $val1 = $recipes.substr($elf1, 1);
      my $val2 = $recipes.substr($elf2, 1);
      $recipes ~= $val1 + $val2;
      my $n = $recipes.chars;
      if ($n >= $threshold) {
         $n.note if %*ENV<DEBUG>;
         $threshold += 100000;
      }
      if (! defined $part1) && ($n >= $target + 10) {
         $part1 = $recipes.substr($target, 10);
         my $elapsed = sprintf('%.3f', now - $start);
         put "part1 ($elapsed) $highlight$part1$reset";
      }
      $elf1 = ($elf1 + $val1 + 1) % $n;
      $elf2 = ($elf2 + $val2 + 1) % $n;
      next if defined $part2;
      my $index = $recipes.substr($n - 1 - $length).index($target) // next;
      $part2 = $recipes.chars - $length - 1 + $index;
      my $elapsed = sprintf('%.3f', now - $start);
      put "part2 ($elapsed) $highlight$part2$reset";
   }

   return 0;
}
