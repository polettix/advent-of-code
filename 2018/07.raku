#!/usr/bin/env raku
use v6;

sub MAIN ($filename = Nil) {
   my $inputs = get-inputs($filename // $?FILE.subst(/\.raku$/, '.tmp'));
   my ($part1, $part2) = solve($inputs);

   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";
   put "part1 $highlight$part1$reset";
   put "part2 $highlight$part2$reset";
}

sub get-inputs ($filename) {
   my (%children-of, %parent-of);
   for $filename.IO.basename.IO.lines -> $line {
      $line ~~ m/^ Step \s+ (\S+) .* step \s+ (\S+)/;
      %children-of{$0}.push($1);
      %parent-of{$1}.push($0);
   }
   return ('children-of' => %children-of, 'parent-of' => %parent-of).hash;
} ## end sub get_inputs ($filename = undef)

sub solve ($inputs) {
   return (part1($inputs), part2($inputs));
}

sub part1 ($inputs) {
   my @free = ($inputs<children-of>.keys (-) $inputs<parent-of>.keys)
      .keys.sort: {$^a cmp $^b};
   my %constrained-by;
   for $inputs<parent-of>.kv -> $child, $parents {
      %constrained-by{$child}{$_} = 1 for $parents.Slip;
   }
   my @schedule;
   while (@free) {
      my $item = @free.shift;
      @schedule.push: $item;
      next unless $inputs<children-of>{$item}:exists;
      for $inputs<children-of>{$item}.Slip -> $child {
         %constrained-by{$child}{$item}:delete;
         if %constrained-by{$child}.elems == 0 {
            %constrained-by{$child}:delete;
            @free.push: $child;
         }
      }
      @free = @free.sort: {$^a cmp $^b};
   }
   return @schedule.join: '';
}

sub part2 ($inputs) {
   my %constrained-by;
   for $inputs<parent-of>.kv -> $child, $parents {
      %constrained-by{$child}{$_} = 1 for $parents.Slip;
   }

   my $task-base = 1 + (%*ENV<BASE> // 0) - ord('A');
   my @backlog = ($inputs<children-of>.keys (-) $inputs<parent-of>.keys)
      .keys.sort({$^a cmp $^b});

   my @workers = (1 .. (%*ENV<WORKERS> // 2)).map: { "Worker $_" };

   my @ongoing;
   my $time = 0;
   while @backlog || @ongoing {
      while @backlog && @workers {
         my $task = @backlog.shift;
         my $duration = $task-base + $task.ord;
         my $end = $time + $duration;
         my $wkr = @workers.shift;
         @ongoing.push: (end => $end, task => $task, worker => $wkr).hash;
      }

      @ongoing = @ongoing.sort: { $^a<end> <=> $^b<end> };
      my $item = @ongoing.shift;
      my $task = $item<task>;
      $time = $item<end>;
      @workers.push: $item<worker>;
      put "$time $task";

      next unless $inputs<children-of>{$task}:exists;
      for $inputs<children-of>{$task}.Slip -> $child {
         %constrained-by{$child}{$task}:delete;
         if %constrained-by{$child}.elems == 0 {
            %constrained-by{$child}:delete;
            @backlog.push: $child;
         }
      }
      @backlog = @backlog.sort: {$^a cmp $^b};
   }
   return $time;
}
