#!/usr/bin/env perl
use 5.024;
use warnings;
use English qw< -no_match_vars >;
use autodie;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use File::Basename qw< basename >;
use Data::Dumper; $Data::Dumper::Indent = 1;
use Storable 'dclone';
use List::Util 'min';
$|++;

my $inputs = get_inputs(@ARGV);
my ($part1, $part2) = solve($inputs);

my $highlight = "\e[1;97;45m";
my $reset     = "\e[0m";
say "part1 $highlight$part1$reset";
say "part2 $highlight$part2$reset";

sub get_inputs ($filename = undef) {
   $filename //= basename(__FILE__) =~ s{\.pl\z}{.tmp}rmxs;
   open my $fh, '<', $filename;
   chomp(my $line = <$fh>);
   return $line;
} ## end sub get_inputs ($filename = undef)

sub solve ($inputs) {
   my $part1 = $ENV{RX} ? \&part1_rx : \&part1;
   return ($part1->($inputs), part2($inputs));
}

sub part1_matcher () {
   my $allpairs = join '|',
      map { (lc($_) . uc($_), uc($_) . lc($_)) } 'a' .. 'z';
   return qr{$allpairs};
}

sub part1_rx ($inputs) {
   state $matcher = part1_matcher();
   1 while $inputs =~ s/$matcher//g;
   return length $inputs;
}

sub part1 ($inputs) {
   my $changed = -1;
   while ($changed && length$inputs) {
      $changed = 0;
      my $current = substr $inputs, 0, 1;
      my $i = 0;
      while ($i < length($inputs) - 1) {
         my $succ = substr $inputs, $i + 1, 1;
         if ($current ne $succ && lc($current) eq lc($succ)) {
            ++$changed;
            substr $inputs, $i, 2, '';
            $current = substr($i, 1) if $i < length $inputs;
         }
         else {
            $current = $succ;
            ++$i;
         }
      }
   }
   return length $inputs;
}

sub part2 ($inputs) {
   return min map { part1_rx($inputs =~ s{$_}{}rigmxs) } 'a' .. 'z';
}
