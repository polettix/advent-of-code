#!/usr/bin/env raku
use v6;
use Test;

sub MAIN ($filename = $?FILE.subst(/\.raku$/, '.sample')) {
   return solve($filename) if $filename.IO.e;

   my @tests = (3, 5, 8, 4),
      (122, 79, 57, -5),
      (217, 196, 39, 0),
      (101, 153, 71, 4);
   for @tests -> ($x, $y, $serial-number, $power-level) {
      is power-level-at($x, $y, $serial-number), $power-level,
         "($x, $y, $serial-number) -> $power-level";
   }

   done-testing();

   my $X = 18;
   my $Y = 32;
   for $Y .. $Y + 4 -> $y {
      ($X .. $X + 4).map( -> $x { super-power-level-at($x, $y, 42, 1) } ).join(' ').put;
   }
   ''.put;
   for $Y .. $Y + 3 -> $y {
      ($X .. $X + 3).map( -> $x { super-power-level-at($x, $y, 42, 2) } ).join(' ').put;
   }
   ''.put;
   for $Y .. $Y + 2 -> $y {
      ($X .. $X + 2).map( -> $x { super-power-level-at($x, $y, 42, 3) } ).join(' ').put;
   }
}

sub solve ($filename) {
   my $highlight = "\e[1;97;45m";
   my $reset     = "\e[0m";

   my $inputs = get-inputs($filename);
   my ($start, $elapsed);

   $start = now;
   my $part1 = part1($inputs);
   $elapsed = now - $start;
   put "part1 ($elapsed) $highlight$part1$reset";

   $start = now;
   my $part2 = part2($inputs);
   $elapsed = now - $start;
   put "part2 ($elapsed) $highlight$part2$reset";

   return 0;
}

sub get-inputs ($filename) { $filename.IO.slurp + 0 }

sub part1 ($serial-number) {
   my (@parts, @sums, $max, $max-x, $max-y);
   for (1 .. 298) X (1 .. 298) -> ($x, $y) {
      my $v = super-power-level-at($x, $y, $serial-number, 3);
      ($max, $max-x, $max-y) = $v, $x, $y
         if (! defined $max) || $max < $v;
   }
   ($max-x, $max-y, $max).note if %*ENV<DEBUG>;
   return "$max-x,$max-y";

   for 1 .. 300 -> $y {
      my $old-part = @parts == 3 ?? @parts.shift !! Nil;
      my (@part, @local, $sum);
      for 1 .. 300 -> $x {
         @local.push: my $pl = power-level-at($x, $y, $serial-number);
         $sum += $pl;
         $sum -= @local.shift while @local > 3;
         if @local == 3 {
            @part[$x] =  $sum;
            @sums[$x] += $sum;
            @sums[$x] -= $old-part[$x] if defined $old-part;
            ($max, $max-x, $max-y) = @sums[$x], $x - 2, $y - 2
               if (! defined $max) || $max < @sums[$x];
         }
      }
      @parts.push: @part;
   }
   ($max-x, $max-y, $max).note if %*ENV<DEBUG>;
   return "$max-x,$max-y";
}

sub part2 ($serial-number) {
   my (@parts, @sums, $max, $max-x, $max-y, $max-size);
   for 1 .. 300 -> $size {
      for (1 .. 301 - $size) X (1 .. 301 - $size) -> ($x, $y) {
         my $v = super-power-level-at($x, $y, $serial-number, $size);
         ($max, $max-x, $max-y, $max-size) = $v, $x, $y, $size
            if (! defined $max) || $max < $v;
      }
   }
   ($max-x, $max-y, $max-size, $max).note if %*ENV<DEBUG>;
   return "$max-x,$max-y,$max-size";
}

sub power-level-at ($x, $y, $serial-number) {
   my $rack-id = $x + 10;
   my $power-level = $rack-id * ($rack-id * $y + $serial-number);
   my $digit = $power-level < 100 ?? 0 !! $power-level.substr(*-3, 1) + 0;
   return $digit - 5;
}

sub super-power-level-at ($x, $y, $serial-number, $size) {
   state %cache;
   state &me = &?BLOCK;
   if ! defined %cache{$serial-number}[$size][$x][$y] {
      my $value;
      if $size == 1 { $value = power-level-at($x, $y, $serial-number) }
      else {
         $value =   &me(    $x,     $y, $serial-number, $size - 1)
                  + &me($x + 1, $y + 1, $serial-number, $size - 1)
                  + &me($x + $size - 1,     $y, $serial-number,         1)
                  + &me(    $x, $y + $size - 1, $serial-number,         1);
         $value -=  &me($x + 1, $y + 1, $serial-number, $size - 2)
            if $size > 2;
      }
      %cache{$serial-number}[$size][$x][$y] = $value;
   }
   return %cache{$serial-number}[$size][$x][$y];
}
