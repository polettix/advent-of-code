#!/bin/sh
me="$(readlink -f "$0")"
md="$(dirname "$me")"

$md/get-inputs.sh "$@" | {
   read day
   cp "$md/template.raku" "$day.raku"
   cat -
}
